#include "imageexplorer.h"

#include <QDesktopServices>
#include <QFileInfo>
#include <QTimer>
#include <QDebug>
#include <QMenu>

/*!
 * \brief ImageExplorer::ImageExplorer
 * \param parent
 */
ImageExplorer::ImageExplorer(QWidget *parent) :
    QLabel(parent), m_rubberBand(NULL)
{
    setContextMenuPolicy(Qt::CustomContextMenu);
    setAlignment(Qt::AlignCenter);
    setFeatures(ImgExp::INVALID);
    m_offsetX = 0;
    m_offsetY = 0;
    m_currentWidth = 640;
    m_currentHeight = 480;

    // Wait for first click
    m_lastX = m_lastY = -1;

    // for alt images
    p_actionGroup = new QActionGroup(this);

    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            this, SLOT(showContextMenu(const QPoint &)));
}

/*!
 * \brief addAlternateImage
 *        These can be swapped to via rightclick->change image
 * \param _img - the image
 * \param _name - a name to be shown in the context menu
 * \param _checked - is it ticked on or off?
 */
void ImageExplorer::addAlternateImage(QString _imgPath, QString _name, bool _checked)
{
    if (_name.isEmpty())
        return;

    QAction *temp = new QAction(_name, this);
    temp->setCheckable(true);
    temp->setChecked(_checked);
    temp->setData(QVariant(_name));  /// TODO:
    m_alternateImgs.push_back(_imgPath);
    m_altActions.push_back(temp);
    p_actionGroup->addAction(temp);
}

/*!
 * \brief ImageExplorer::useAltImage
 *        swap to the new image
 * \param _action - the menu action
 */
void ImageExplorer::useAltImage(QAction* _action)
{
    int imgIndex = m_altActions.indexOf(_action);

    // Load in the image and send the timestamp to OffenceDisplay
    m_fullsizeImg = QImage(m_alternateImgs.at(imgIndex));
    updateDisplay(true);
    emit altImageUsed(m_id, _action->text());
}

/*!
 * \brief ImageExplorer::resetTranslation
 *        Reset the display image with the original and center it
 */
void ImageExplorer::resetTranslation()
{
    m_displayImg = m_fullsizeImg;
    m_centerX = m_fullsizeImg.width() / 2;
    m_centerY = m_fullsizeImg.height() / 2;
    m_offsetX = m_offsetY = 0;
    m_zoomScale = 1.0f;
}

void ImageExplorer::setTranslation(QPoint _center, bool _zoomIn)
{
    m_centerX = _center.x();
    m_centerY = _center.y();

    if (_zoomIn)
    {
        m_zoomScale += m_zoomScale;
        if (m_zoomScale > 20.0) m_zoomScale = 20.0f;
    }
    else
    {
        m_zoomScale -= (m_zoomScale / 2.0);
        if (m_zoomScale < 1.0) m_zoomScale = 1.0f;
    }

    updateDisplay(true);
}

void ImageExplorer::setTranslation(QRect _box)
{
    float maxHeight = static_cast<float>(m_fullsizeImg.height());
    float maxWidth = static_cast<float>(m_fullsizeImg.width());

    QRect paddedRect;
    if (_box.width() > _box.height())
    {
        float scaleHeight = maxHeight / maxWidth;
        int padding = (_box.width() * scaleHeight) - _box.height();
        int paddedY = _box.y() - (padding / 2.0f);
        int paddedHeight = _box.width() * scaleHeight;
        paddedRect = QRect(_box.x(), paddedY, _box.width(), paddedHeight);
        m_zoomScale = (float)m_fullsizeImg.width() / (float)paddedRect.width();
    }
    else
    {
        float scaleWidth =  maxWidth / maxHeight;
        int padding = (_box.height() * scaleWidth) - _box.width();
        int paddedX = _box.x() - (padding / 2.0f);
        int paddedWidth = _box.height() * scaleWidth;
        paddedRect = QRect(paddedX, _box.y(), paddedWidth, _box.height());
        m_zoomScale = (float)m_fullsizeImg.height() / (float)paddedRect.height();
    }

    // center
    m_centerX = paddedRect.x() + (paddedRect.width() / 2);
    m_centerY = paddedRect.y() + (paddedRect.height() / 2);
    updateDisplay(true);
}

/*!
 * \brief ImageExplorer::setImage
 * \param _newImg the image to be displayed
 * \param keepTranslation - this can be set true to use the zoom/offset of the previous image
 */
void ImageExplorer::setImage(QImage _newImg, bool keepTranslation = false)
{
    // Clear previous actions
    m_altActions.clear();
    m_alternateImgs.clear();
    foreach (QAction *act, p_actionGroup->actions())
        p_actionGroup->removeAction(act);

    // Set image
    m_fullsizeImg = _newImg;
    if (_newImg.isNull()) {
        m_fullsizeImg = QImage(1024, 806, QImage::Format_ARGB32);
        m_fullsizeImg.fill(Qt::white);
    }

    if (!keepTranslation)
        resetTranslation();

    updateDisplay(true);
    if (!m_displayImg.isNull()) {
        setPixmap(QPixmap::fromImage(m_displayImg.scaled(m_currentWidth,m_currentHeight,Qt::KeepAspectRatio, m_transformType)));
    }
}

/*!
 * \brief ImageExplorer::updateDisplay
 * \param fromFull
 */
void ImageExplorer::updateDisplay(bool fromFull)
{
    if (pixmap() == NULL) {
        setText("Image unavailable");
        return;
    }

    if (fromFull)
    {
        applyTransformation();
    }

    setPixmap(QPixmap::fromImage(m_displayImg.scaled(m_currentWidth, m_currentHeight, Qt::KeepAspectRatio, m_transformType)));
}

void ImageExplorer::setDisplayImageFromFull(QImage _newFull)
{
    QImage temp = m_fullsizeImg;
    m_fullsizeImg = _newFull;
    updateDisplay(true);
    m_fullsizeImg = temp;
}

void ImageExplorer::resizeEvent(QResizeEvent * event)
{
    QLabel::resizeEvent(event);

    m_currentWidth = event->size().width();
    m_currentHeight = event->size().height();
    if (!m_displayImg.isNull()) {
        setPixmap(QPixmap::fromImage(m_displayImg.scaled(m_currentWidth,m_currentHeight,Qt::KeepAspectRatio, m_transformType)));
    }
}

/*!
 * \brief ImageExplorer::applyTransformation
 *        Create a copy of the full size image cropped and zoomed
 */
void ImageExplorer::applyTransformation()
{
    if(m_zoomScale > 0.999 && m_zoomScale < 1.001) {
        m_displayImg = m_fullsizeImg;
        m_zoomScale = 1;
        m_offsetX = m_offsetY = 0;
        m_centerX = m_fullsizeImg.width() / 2;
        m_centerX = m_fullsizeImg.height() / 2;
    } else {
        int xstart = (m_centerX - m_fullsizeImg.width() / (2 * m_zoomScale));
        int ystart = (m_centerY - m_fullsizeImg.height() / (2 * m_zoomScale));
        int xwidth = m_fullsizeImg.width() / m_zoomScale;
        int yheight = m_fullsizeImg.height() / m_zoomScale;

        m_displayImg = m_fullsizeImg.copy(xstart, ystart, xwidth, yheight);

        // new offset in realsize of image
        m_offsetX = xstart;
        m_offsetY = ystart;
    }
}

void ImageExplorer::copyTransformation(int _centerX, int _centerY, float _zoomScale)
{
    m_centerX = _centerX;
    m_centerY = _centerY;
    m_zoomScale = _zoomScale;
    updateDisplay(true);
}

// Used for scroll zooming in and out of the image
// centering on the mouse position
void ImageExplorer::wheelEvent(QWheelEvent *event)
{
    if (m_features & ImgExp::SCROLLZOOM)
    {
        float factor = (static_cast<float>(event->angleDelta().y()) / 8.0f) / 120.0f;
        m_zoomScale = qBound<float>(1, m_zoomScale + factor, 5.0f);

        applyTransformation();
        updateDisplay();

        emit imageScrolled(m_id, m_zoomScale);
    }
}

// In default mode this is dragging the image (up/down/left/right)
// In Zoom mode this is click zooming or box zooming
void ImageExplorer::mousePressEvent(QMouseEvent* event)
{
    if(event->buttons() == Qt::LeftButton)
    {
        if (m_features & ImgExp::BOXSELECT)
        {
            m_pointOrigin = event->pos();
            if (!m_rubberBand)
                m_rubberBand = new QRubberBand(QRubberBand::Rectangle, this);
            m_rubberBand->setGeometry(QRect(m_pointOrigin, QSize()));
            m_rubberBand->show();
        }

        if (m_features & ImgExp::IMAGECLICK)
        {
            imageClicked(m_id, originalPos(event));
        }
    }
    if(event->button() == Qt::RightButton)
        emit imageRightClicked(m_centerX, m_centerY, m_zoomScale);
}

QPoint ImageExplorer::originalPos(QMouseEvent* _event)
{
    // Calc original pixel position
    // Position of click inside image (not whole label)
    int imageX = _event->pos().x() - ((width() - pixmap()->width()) / 2);
    int imageY = _event->pos().y() - ((height() - pixmap()->height()) / 2);
    double xScale = static_cast<double>(m_displayImg.width()) / static_cast<double>(pixmap()->width());
    double yScale = static_cast<double>(m_displayImg.height()) / static_cast<double>(pixmap()->height());
    int x = m_offsetX + (imageX * xScale);
    int y = m_offsetY + (imageY * yScale);

    return QPoint(x,y);
}

// Ends a drag event for either box zoom or image translation
void ImageExplorer::mouseMoveEvent(QMouseEvent* event)
{
    if(event->buttons() == Qt::LeftButton)
    {
        setTransformType(Qt::FastTransformation);
        if (m_features & ImgExp::DRAG)
        {
            // Get display image relative position to measure drag
            int imageX = event->pos().x() - ((width() - pixmap()->width()) / 2);
            int imageY = event->pos().y() - ((height() - pixmap()->height()) / 2);

            double xScale = static_cast<double>(m_displayImg.width()) / static_cast<double>(pixmap()->width());
            double yScale = static_cast<double>(m_displayImg.height()) / static_cast<double>(pixmap()->height());
            int x = (imageX * xScale);
            int y = (imageY * yScale);

            // First drag
            if (m_lastX == -1 && m_lastY == -1) {
                m_lastX = x;
                m_lastY = y;
                return;
            }

            // drag the center a little
            int dragX = (m_lastX - x);
            int dragY = (m_lastY - y);
            m_centerX += dragX;
            m_centerY += dragY;

            // reset last pos
            m_lastX = x;
            m_lastY = y;

            updateDisplay(true);
        }

        if (m_features & ImgExp::BOXSELECT)
        {
            QPoint boundPoint;
            boundPoint.setX(qBound<int>(0, event->pos().x(), width()));
            boundPoint.setY(qBound<int>(0, event->pos().y(), height()));
            m_rubberBand->setGeometry(QRect(m_pointOrigin, boundPoint).normalized());
        }
    }
}

// Ends a drag event for either box zoom or image translation
void ImageExplorer::mouseReleaseEvent(QMouseEvent* event)
{
    Q_UNUSED(event)

    if (m_features & ImgExp::BOXSELECT)
    {
        m_rubberBand->hide();
        m_lastSelection = m_rubberBand->geometry();

        double xScale = static_cast<double>(m_displayImg.width()) / static_cast<double>(pixmap()->width());
        double yScale = static_cast<double>(m_displayImg.height()) / static_cast<double>(pixmap()->height());

        int full_selection_x = m_offsetX + (m_lastSelection.x() - ((width() - pixmap()->width()) / 2)) * xScale;
        int full_selection_y = m_offsetY + (m_lastSelection.y() - ((height() - pixmap()->height()) / 2)) * yScale;
        int full_selection_width = m_lastSelection.width() * xScale;
        int full_selection_height = m_lastSelection.height() * yScale;

        // Send the signal with rect info in original image co-ords
        QRect fullRect(full_selection_x, full_selection_y, full_selection_width, full_selection_height);
        emit imageBoxZoomed(m_id, fullRect);
    }

    // Reset drag
    m_lastX = m_lastY = -1;

    setTransformType(Qt::SmoothTransformation);
    emit imageReleased(m_id, originalPos(event));
}

void ImageExplorer::mouseDoubleClickEvent(QMouseEvent* event)
{
    if (m_features & ImgExp::IMAGEDOUBLECLICK)
    {
        emit imageDoubleClicked(m_id, originalPos(event));
    }
}

/// SLOTS

void ImageExplorer::showContextMenu(const QPoint &_pos)
{
    QMenu contextMenu(tr("Context menu"), this);
    QAction action1("Open File", this);
    connect(&action1, SIGNAL(triggered()), this, SLOT(openNatively()));
    contextMenu.addAction(&action1);

    QAction action2("Show in Explorer", this);
    connect(&action2, SIGNAL(triggered()), this, SLOT(openLocalFolder()));
    contextMenu.addAction(&action2);

    if (m_altActions.length() > 0)
    {
        contextMenu.addSeparator();
        contextMenu.setStyleSheet("QMenu::item:selected { color: blue; }");
        QMenu *altImgs = contextMenu.addMenu("Change Image");
        for (int i = 0; i < m_altActions.length(); ++i)
            altImgs->addAction(m_altActions.at(i));
        connect(altImgs, SIGNAL(triggered(QAction*)), this, SLOT(useAltImage(QAction*)));
        contextMenu.addSeparator();
    }

    contextMenu.exec(mapToGlobal(_pos));
}

void ImageExplorer::openNatively()
{
    QFileInfo pathInfo(m_imageFileName);
    QString fileUrl = "file:///" + pathInfo.absoluteFilePath();
    if (!QDesktopServices::openUrl(QUrl(fileUrl)))
        qDebug("File failed to open");
}

void ImageExplorer::openLocalFolder()
{
    QFileInfo pathInfo(m_imageFileName);
    QString fileUrl = "file:///" + pathInfo.absolutePath();
    if (!QDesktopServices::openUrl(QUrl(fileUrl)))
        qDebug("File failed to open");
}
