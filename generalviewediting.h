#ifndef GENERALVIEWEDITING_H
#define GENERALVIEWEDITING_H

#include <QGenericMatrix>
#include <QtCore/qmath.h>
#include <QPushButton>
#include <QCheckBox>
#include <QWidget>
#include <QSlider>
#include <QSpinBox>
#include <QGridLayout>
#include <QLabel>

#include "wlpDefs.h"
#include "imageexplorer.h"

//Forward decleration of editor
class GeneralEditor;

//General viewing class
class GeneralView : public ImageExplorer, private WLPBase
{
    friend class GeneralEditor;
    Q_OBJECT

public:
    explicit GeneralView(QWidget *p_Parent = 0);
    GeneralView(QString p_Channel, QImage *p_Img_Ptr, WLPFlag p_EditType, QWidget *p_Parent);
    
signals:
    
public slots:
    void Update();

private:
    //Main objects
    QImage *m_Img_Ptr;
    QImage m_ImgScaled;
    QImage m_ImgDisplay;

    //GUI
    QGridLayout *m_Layout;
    //QLabel *m_Label;
    //ImageExplorer *p_explorer;
};

//General Editor class
class GeneralEditor : public QWidget, private WLPBase
{
    Q_OBJECT
public:
    GeneralEditor(QWidget *p_Parent_Ptr, GeneralView *p_Viewer_Ptr, WLPMode p_Mode, WLPFlag p_EditType, QString _info = "");

    QPushButton * GetApplyButt(){ return m_ApplyButton_Ptr; }
    QPushButton * GetCancelButt(){ return m_CancelButton_Ptr; }

    qreal lowest, highest;

public slots:

    void UpdateControl(WLPFlag _updateState);
    void UpdateControl() { UpdateControl(WLP_PREVIEW_ON); }
    void SaveControl() { UpdateControl(WLP_PREVIEW_OFF); }

    void UpdateBright(WLPFlag _updateState);
    void UpdateBright() { UpdateBright(WLP_PREVIEW_ON); }
    void SaveBright() { UpdateBright(WLP_PREVIEW_OFF); }

    void UpdateInvert(WLPFlag _updateState);
    void UpdateInvert() { UpdateInvert(WLP_PREVIEW_ON); }
    void SaveInvert() { UpdateInvert(WLP_PREVIEW_OFF); }

    void Cancel();


private:
    //Set up functions
    void SetUpControl();
    void SetUpBright(QString _info);
    void SetUpInvert();

    GeneralView *m_Viewer_Ptr;
    WLPFlag m_EditType;
    WLPMode m_Mode;

    QPushButton *m_ApplyButton_Ptr;
    QPushButton *m_CancelButton_Ptr;

    //GUI
    QGridLayout *m_Layout_Ptr;
    QLabel * m_Label_Ptr;

    QCheckBox *p_InvertA;
    QCheckBox *p_InvertB;
    QCheckBox *p_InvertC;
    QCheckBox *p_InvertD;

    QSlider *m_ControlA_Ptr;
    QSlider *m_ControlB_Ptr;
    QSlider *m_ControlC_Ptr;
    QSlider *m_ControlD_Ptr;
    QSlider *p_ControlD; //Key for CMYK
};

#endif // GENERALVIEWEDITING_H
