#-------------------------------------------------
#
# Project created by QtCreator 2013-10-28T01:36:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WavelengthAlpha
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    infrachromemethod.cpp \
    spectralexplorer.cpp \
    dualprocess.cpp \
    rainbowextractor.cpp \
    luminancemapper.cpp \
    generalviewediting.cpp \
    pooleditor.cpp \
    wlpDefs.cpp \
    colourpurity.cpp \
    loadingthread.cpp \
    wlpsettings.cpp \
    imageexplorer.cpp \
    stylesheets.cpp

HEADERS  += mainwindow.h \
    infrachromemethod.h \
    spectralexplorer.h \
    dualprocess.h \
    ColourMapping.h \
    wlpDefs.h \
    rainbowextractor.h \
    luminancemapper.h \
    generalviewediting.h \
    pooleditor.h \
    colourpurity.h \
    loadingthread.h \
    wlpTypes.h \
    wlpsettings.h \
    imageexplorer.h

FORMS    += mainwindow.ui

RESOURCES += \
    Icon.qrc

CONFIG += c++11
CONFIG += -Wall # this works fine for MSVC; QT maps it to the correct equivalent
CONFIG += warn_on
