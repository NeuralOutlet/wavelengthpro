#include "loadingthread.h"
#include "mainwindow.h"
#include "wlpsettings.h"
#include "wlpDefs.h"

#include <QDesktopServices>

#include <QApplication>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <QLayout>
#include <vector>
#include <QLabel>

//Static declerations
int WLPBase::s_ViewWidth;
int WLPBase::s_ViewHeight;
bool WLPBase::lutSetLAB = false;
bool WLPBase::lutSetLCH = false;

//Global decleration of config
//(to be extern'd where needed)
WLPConfig g_Config;

// Main program
int main(int argc, char *argv[])
{
    // Begin WavelengthPro
    QApplication a(argc, argv);

    WLPSettings  s;
    s.SyncConfig();

    MainWindow w;
    w.show();

    // Begin initial background loading in a new thread
    LoadingThread loadThread(&w);
    loadThread.start();

    return a.exec();
}
