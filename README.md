# WavelengthPro #

The idea for this software was to focus on editing multi-spectral photography - images taken in ultraviolet, visible, and infrared light. The main features are:

* Digital IRG emulator (Infrachrome.cpp)
* Multi-Channel mixer (spectralexplorer.cpp)
* Colour-Space mixer (luminancemapper.cpp)
* Colour Purity (colourpurity.cpp)
* Dual Processing (dualprocess.cpp)
* General adjustment (generalviewediting.cpp)

There are hardly any comments anywhere so good luck, you'll need it. Some of the comments you do find might not relate to anything or infact lie to you.


# Building WLP #

### Windows, Linux, and Mac ###

I believe the following steps generally work for all three platforms.

* Download Qt Creator
* Download Git Bash and make a local repo:
```
mkdir /your/WLP/dir/
cd /your/WLP/dir/
git clone https://bitbucket.org/NeuralOutlet/wavelengthpro
```
* Open Qt Creator
* Open Existing Project -> MultispectralApp.pro
* Compile and Run (ctrl + R)