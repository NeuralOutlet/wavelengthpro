#ifndef SPECTRALEXPLORER_H
#define SPECTRALEXPLORER_H

#include <QWidget>
#include <QLabel>
#include <QSpinBox>
#include <vector>
#include <QTabWidget>
#include <QComboBox>
#include <QGridLayout>
#include <QImageWriter>
#include <QMessageBox>
#include <QFileDialog>
#include <QPushButton>

#include "wlpTypes.h"
//#include "wlpDefs.h"

class SpectralExplorer : public QWidget, public WLPBase
{
    Q_OBJECT
public:
    explicit SpectralExplorer(WLPPool *_dropper, QWidget *parent = 0);

    QComboBox* GetPmap(){ return pMaps; }
    QComboBox* GetPgray(){ return pGreys; }
    QSpinBox* GetMaxChan(){ return maxChannels; }

    float lerp(float a, float b, float f);
    QImage GetChannel(QString chan, WLPFlag p_PreviewState);

    qreal GetGrey(QColor _pixel);
    
signals:
    void DropImage(QString);

public slots:
    void ModifyPseudo();
    void ModifyFalse(WLPFlag p_PreviewState);
    void ModifyFalse(){ ModifyFalse( WLP_PREVIEW_ON ); }
    void UpdateGrey(int i);
    void NormGrey(){ UpdateGrey(-1); }
    void CustomGrey(QString str);
    void ChannelMix();
    void UpdateTabs();
    void Render();
    void Drop();

private:
    //The Images
    QImage *p_MasterImgs;
    QImage m_ImgScaled[WLP_POOL_MAX];
    WLPPool *p_Pool;

    QImage mixedImg;
    QImage pseudoImg;
    int imgWidth;
    int imgHeight;

    //Colour mapping:
    QWidget *mixerWidget;
    QLabel *pseudoView;
    bool mixerLoaded;
    bool isJet;
    QTabWidget *tabColMap;
    QComboBox *pGreys;
    QComboBox *pMaps;
    QComboBox chanWave[7];
    QSlider chanScale[7];
    QSpinBox *maxChannels;
    QLabel chanTemplate;

    //qreal m_GreyWeight[2];
    QSpinBox *p_GreyWeight[3];
    QLabel *p_Equation;

    int p_SubCount;
    
};

#endif // SPECTRALEXPLORER_H
