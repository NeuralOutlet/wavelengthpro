#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "wlpsettings.h"

#include <QLabel>
#include <QLayout>
#include <QToolButton>
#include <QPlainTextEdit>
#include <QSignalMapper>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressBar>
#include <QThread>
#include <QDropEvent>
#include <QMimeData>
#include <QUrl>
#include <QMovie>
#include <QMouseEvent>

//Global config data
extern WLPConfig g_Config;

// stylesheet
extern QString g_style;

//Helpful
QString g_ImgTypes[WLP_POOL_MAX] = {
    "FULL (350-1000nm)",
    "UV (350-400nm)",
    "VIS (400-650nm)",
    "H-α (~656.28nm)",
    "IR (720-1000nm)",
    "IR (950-1000nm)",
    "UVIR (non-VIS)",
    "NIR (750-1400nm)",
    "SWIR (1400-3000nm)",
    "MWIR (3000-8000nm)",

    "Multi-Channel",
    "Colour-Space",
    "Emulated IRG",
    "Emulated Ortho",
    "Rainbow Mask",
    "Dual Processed",
    "Colour Pure"
};

int IndexFromString(QString _str)
{
    for(int i = 0; i < WLP_POOL_MAX; ++i)
        if(_str == g_ImgTypes[i])
            return i;

    // Specific type not recognised: Assuming genral
    return -1;
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    p_ImgPool = new WLPPool;
    connect(p_ImgPool, SIGNAL(DropSig(QString)), this, SLOT(CatchDropped(QString)));
    setAcceptDrops(true);

    ui->setupUi(this);

    // create objects for the label and progress bar
    p_StatLabel = new QLabel(this);
    p_StatProgress = new QProgressBar(this);

    // set text for the label
    p_StatLabel->setText("");

    // make progress bar text invisible
    p_StatProgress->setTextVisible(false);
    p_StatProgress->setValue(0);

    // add the two controls to the status bar
    statusBar()->addWidget(p_StatLabel);
    statusBar()->addWidget(p_StatProgress,1);

    //Set up the MDI enviroment
    setWindowIcon(QIcon(":/Icons/WavelengthPro.png"));
    specialArea = new QMdiArea(this);
    specialArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    specialArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setCentralWidget(specialArea);
    connect(specialArea, SIGNAL(subWindowActivated(QMdiSubWindow*)), this, SLOT(Adjustment(QMdiSubWindow*)));
    showMaximized();

    setWindowTitle("WavelengthPro: The Multispectral Image Editor");

    m_IsSpectral = true;

    //Set up the menu bar:
    SetUpMenu();

    //Image pool set up
    QWidget *imgPool;
    QLabel *helloText;
    QGridLayout *poolLayout;
    p_PoolTabs = new QTabWidget;
    p_PoolSpectral = new QListWidget;
    p_PoolSpectral->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(p_PoolSpectral, SIGNAL(customContextMenuRequested(QPoint)),this, SLOT(OpenContextMenu(QPoint)));
    connect(p_PoolSpectral, SIGNAL(itemClicked(QListWidgetItem*)),this, SLOT(PoolListClicked(QListWidgetItem*)));

    p_PoolGeneralTest = new ImageLister;
    p_PoolGeneralTest->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(p_PoolGeneralTest, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(OpenGifViewer(QPoint)));
    connect(p_PoolGeneralTest, SIGNAL(clicked(QModelIndex)),this, SLOT(PoolListClickedGeneral(QModelIndex)));

    p_PoolCreated = new QListWidget;
    connect(p_PoolCreated, SIGNAL(itemClicked(QListWidgetItem*)),this, SLOT(PoolListClicked(QListWidgetItem*)));
    m_CountSpectral = 0;
    m_CountCreated = 0;

    //Spectral
    poolLayout = new QGridLayout;
    helloText = new QLabel;
    helloText->setMaximumWidth(this->width());
    helloText->setWordWrap(true);
    helloText->setText("This is the spectral image pool, here is where wavelength-specific images are stored. Loading these images activates tools that take advantage of actual multispectral information such as: The EIR film emulator, Vision Simulator & Pool Analysis.");
    poolLayout->addWidget(helloText,0,0);
    poolLayout->addWidget(p_PoolSpectral,1,0);
    imgPool = new QWidget(this);
    imgPool->setLayout(poolLayout);
    imgPool->setWindowTitle("Image Pool");
    p_PoolTabs->addTab(imgPool, "Spectral");

    //Created
    poolLayout = new QGridLayout;
    helloText = new QLabel;
    helloText->setMaximumWidth(this->width());
    helloText->setWordWrap(true);
    helloText->setText("This is the created image pool, here is where images made with WLP tools can be stored. Use the 'Drop to Pool' buttons to move them here for further editing.");
    poolLayout->addWidget(helloText,0,0);
    poolLayout->addWidget(p_PoolCreated,1,0);
    imgPool = new QWidget;
    imgPool->setLayout(poolLayout);
    imgPool->setWindowTitle("Image Pool");
    p_PoolTabs->addTab(imgPool, "Created");

    //General
    poolLayout = new QGridLayout;
    helloText = new QLabel;
    helloText->setMaximumWidth(this->width());
    helloText->setWordWrap(true);
    helloText->setText("This is the General image pool, these images hold no spectral data but can still be used in most of the tools.");
    poolLayout->addWidget(helloText,0,0);
    poolLayout->addWidget(p_PoolGeneralTest,1,0);
    imgPool = new QWidget;
    imgPool->setLayout(poolLayout);
    imgPool->setWindowTitle("Image Pool");
    p_PoolTabs->addTab(imgPool, "General");

    p_PoolTabs->setWindowTitle("Image Pools");
    QMdiSubWindow *pools = specialArea->addSubWindow(p_PoolTabs);
    pools->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowMinimizeButtonHint);

    p_PoolTabs->show();

    setStyleSheet(g_style);
}



void MainWindow::PoolListClicked(QListWidgetItem *item)
{
    QString wave = item->text();
    GeneralView *temp = new GeneralView(wave, GetImage(wave), WLP_EDIT_NONE, 0);

    p_PoolSubWnd = new QMdiSubWindow(this);
    p_PoolSubWnd->setWidget(temp);
    p_PoolSubWnd->setAttribute(Qt::WA_DeleteOnClose, true);
    specialArea->addSubWindow(p_PoolSubWnd);
    p_PoolSubWnd->setMinimumSize(50,50);
    p_PoolSubWnd->show();
}

void MainWindow::PoolListClickedGeneral(QModelIndex _index)
{
    QString name = p_PoolGeneralTest->item(_index.row())->text();
    GeneralView *temp = new GeneralView(name, p_ImgPool->GetImgGeneral(_index.row()), WLP_EDIT_NONE, 0);

    p_PoolSubWnd = new QMdiSubWindow(this);
    p_PoolSubWnd->setWidget(temp);
    p_PoolSubWnd->setAttribute(Qt::WA_DeleteOnClose, true);
    specialArea->addSubWindow(p_PoolSubWnd);
    p_PoolSubWnd->setMinimumSize(50,50);
    p_PoolSubWnd->show();
}

void MainWindow::PoolListClickedGeneralGif(QModelIndex _index)
{
    QString name = p_PoolGeneralTest->item(_index.row())->text();
    if(name.contains(".gif"))
    {
        QMovie *tmp = new QMovie( p_ImgPool->m_GeneralGif[_index.row()] );
        QLabel *processLabel = new QLabel;
        processLabel->setMovie(tmp);
        tmp->start();
        p_PoolSubWnd = new QMdiSubWindow(this);
        p_PoolSubWnd->setWindowTitle("Animated GIF Viewer");
        p_PoolSubWnd->setWidget(processLabel);
        p_PoolSubWnd->setAttribute(Qt::WA_DeleteOnClose, true);
        specialArea->addSubWindow(p_PoolSubWnd);
        p_PoolSubWnd->show();
    }
}

QAction* MainWindow::CreateLoadOption(QMenu* const loadMenu, QSignalMapper* const signalMapper, const QString& name, const QString& description)
{
    QAction* action = new QAction(name, this);
    action->setStatusTip(description);
    loadMenu->addAction(action);
    connect(action, SIGNAL(triggered()),signalMapper, SLOT(map()));
    return action;
}

void MainWindow::SetUpMenu()
{
    QSignalMapper* signalMapper = new QSignalMapper (this);

    QMenu *fileMenu = new QMenu(this);

    fileMenu = menuBar()->addMenu(tr("File"));
    QMenu *loadAMenu = fileMenu->addMenu(tr("Load Band"));
    QAction *loadBAct = new QAction(tr("Load..."), this);
    fileMenu->addAction(loadBAct);
    connect(loadBAct, SIGNAL(triggered()),this, SLOT(PhotoLoadGeneral()));

    fileMenu->addSeparator();

    QAction* fullLoad = CreateLoadOption(loadAMenu, signalMapper, tr("FULL (350-1000nm)"), tr("Load a new file (Full spectrum / Wideband)"));
    loadAMenu->addSeparator();
    QAction* ultraLoad  = CreateLoadOption(loadAMenu, signalMapper, tr("UV (350-400nm)"),  tr("Load a new file (Ultraviolet)"));
    QAction* colourLoad = CreateLoadOption(loadAMenu, signalMapper, tr("VIS (400-650nm)"), tr("Load a new file (Visible colour)"));
    QAction* hAlphaLoad = CreateLoadOption(loadAMenu, signalMapper, tr("H-α (~656.28nm)"), tr("Load a new file (Hydrogen Alpha)"));
    QAction* infraLoadW = CreateLoadOption(loadAMenu, signalMapper, tr("IR (720-1000nm)"), tr("Load a new file (Wideband infrared)"));
    QAction* infraLoadS = CreateLoadOption(loadAMenu, signalMapper, tr("IR (950-1000nm)"), tr("Load a new file (Shortband infrared)"));
    loadAMenu->addSeparator();
    QAction* load403 = CreateLoadOption(loadAMenu, signalMapper, tr("UVIR (non-VIS)"), tr("Load a new file (Ultraviolet & infrared)"));

    //------

    p_PoolMenu = new QMenu(this);

    p_PoolMenu = menuBar()->addMenu(tr("Pool"));
    p_AlignAction = p_PoolMenu->addAction(tr("Align"));
    p_AlignAction->setDisabled(true);
    connect(p_AlignAction, SIGNAL(triggered()), this, SLOT(runAlign()));

    p_CloudAction = p_PoolMenu->addAction(tr("Cloud Balance"));
    connect(p_CloudAction, SIGNAL(triggered()), this, SLOT(runCloud()));
    p_CloudAction->setDisabled(true);

    p_ScaleAction = p_PoolMenu->addAction(tr("Scale"));
    connect(p_ScaleAction, SIGNAL(triggered()), this, SLOT(runScale()));
    p_ScaleAction->setDisabled(true);

    p_PoolMenu->setDisabled(true);

    //------

    p_AdjustMenu = new QMenu(this);
    p_ModeGroup = new QActionGroup(this);
    p_AdjustMenu = menuBar()->addMenu(tr("Adjustment"));
    QMenu *modeMenu = p_AdjustMenu->addMenu(tr("Mode"));;
    connect(modeMenu, SIGNAL(hovered(QAction*)), this, SLOT(ChangeMode(QAction*))); //Hover may not work for Mac (doesn't for linux)
    connect(modeMenu, SIGNAL(triggered(QAction*)), this, SLOT(ChangeMode(QAction*)));

    //rgG,RGB,RGBW, HSL,HSV,HSI, CMY,CMYK, YUV,YIQ,YCbCr,
    //xyY,XYZ,Hunter-Lab,UVW,Luv,LCHuv,Lab,LCHab,LMS
    QString modeStr[] = { tr("rgG"),tr("RGB"),tr("RGBW"), tr("HSL"),tr("HSV"),tr("HSI"), tr("CMY"),tr("CMYK"), tr("YUV"),tr("YIQ"),tr("YCbCr"),
                          tr("xyY"),tr("XYZ"),tr("Hunter-Lab"),tr("UVW"),tr("Luv"),tr("LCHuv"),tr("Lab"),tr("LCHab"),tr("LMS")
                        };

    for(int i = 0; i < 20; ++i) //1,3,7,9,17
    {
        p_ModeActions[i] = new QAction(modeStr[i], this); p_ModeActions[i]->setActionGroup(p_ModeGroup);
        modeMenu->addAction(p_ModeActions[i]); p_ModeActions[i]->setCheckable(true);
        if(i != 1 && i != 3 && i != 7 && i != 9 && i != 17)
            p_ModeActions[i]->setVisible(false);

        if(i == 10 || i == 11) //Unsupported
        {
            p_ModeActions[i]->setDisabled(true);
            p_ModeActions[i]->setCheckable(false);
        }
    }
    p_ModeActions[1]->setChecked(true);
    modeMenu->addSeparator();
    p_ShowAll = new QAction("More...", this);
    connect(p_ShowAll, SIGNAL(triggered()), this, SLOT(ShowHideMode()));
    modeMenu->addAction(p_ShowAll);

    p_AdjustMenu->addSeparator();
    p_ControlAction = new QAction(tr("Color-Balance"), this);
    p_AdjustMenu->addAction(p_ControlAction);
    connect(p_ControlAction, SIGNAL(triggered()), this, SLOT(GEditorControl()));

    QAction *invert = new QAction("Invert Channels", this);
    p_AdjustMenu->addAction(invert);
    connect(invert, SIGNAL(triggered()), this, SLOT(GEditorInvert()));

    p_BrightConAction = new QAction(tr("Brightness/Contrast"), this);
    p_AdjustMenu->addAction(p_BrightConAction);
    connect(p_BrightConAction, SIGNAL(triggered()), this, SLOT(GEditorBright()));

    //------

    p_ToolsMenu = new QMenu(this);

    p_ToolsMenu = menuBar()->addMenu(tr("Tools"));

    chanMixAction = new QAction(tr("Multi-Channel Mixer"), this);
    p_ToolsMenu->addAction(chanMixAction); chanMixAction->setDisabled(true);
    connect(chanMixAction, SIGNAL(triggered()), this, SLOT(runChanMix()));

    hslMapAction = new QAction(tr("Colour-Space Mixer"), this);
    p_ToolsMenu->addAction(hslMapAction); hslMapAction->setDisabled(true);
    connect(hslMapAction, SIGNAL(triggered()),this, SLOT(runHSLMap()));

    p_ToolsMenu->addSeparator();

    QMenu *emulateMenu = p_ToolsMenu->addMenu(tr("Film Emulators"));

    eirChrome = new QAction(tr("Ektachrome IR"), this);
    emulateMenu->addAction(eirChrome);
    eirChrome->setDisabled(true);
    connect(eirChrome, SIGNAL(triggered()), this, SLOT(runChrome()));

    p_ToolsMenu->addSeparator();

    rainbowAction = new QAction(tr("Rainbow Extractor"), this);
    p_ToolsMenu->addAction(rainbowAction); rainbowAction->setDisabled(true);
    rainbowAction->setDisabled(true);
    connect(rainbowAction, SIGNAL(triggered()), this, SLOT(runRainbow()));

    dualProaction = new QAction(tr("UV Dual Process"), this);
    p_ToolsMenu->addAction(dualProaction);
    dualProaction->setDisabled(true);
    connect(dualProaction, SIGNAL(triggered()), this, SLOT(runDual()));

    purityAction = new QAction(tr("Colour Purity"), this);
    p_ToolsMenu->addAction(purityAction);
    purityAction->setDisabled(true);
    connect(purityAction, SIGNAL(triggered()), this, SLOT(runPurity()));

    p_ToolsMenu->setDisabled(true);

    //-------------

    QMenu *wlpmenu = new QMenu(this);
    wlpmenu = menuBar()->addMenu(tr("WavelengthPro"));

    QAction *editSet = new QAction(tr("Settings / Preferences"), this);
    wlpmenu->addAction(editSet);
    connect(editSet, SIGNAL(triggered()),this, SLOT(OpenSettings()));

    wlpmenu->addSeparator();

    QAction *exampleWlp = new QAction(tr("Examples..."), this);
    wlpmenu->addAction(exampleWlp);
    connect(exampleWlp, SIGNAL(triggered()),this, SLOT(ExampleLinks()));

    QAction *aboutWlp = new QAction(tr("About WavelengthPro..."), this);
    wlpmenu->addAction(aboutWlp);
    connect(aboutWlp, SIGNAL(triggered()),this, SLOT(AboutWLP()));

    //========================== Signal value mapping for loading various images ==========================//
    signalMapper->setMapping(fullLoad, fullLoad->text());
    signalMapper->setMapping(ultraLoad, ultraLoad->text());
    signalMapper->setMapping(colourLoad, colourLoad->text());
    signalMapper->setMapping(hAlphaLoad, hAlphaLoad->text());
    signalMapper->setMapping(infraLoadW, infraLoadW->text());
    signalMapper->setMapping(infraLoadS, infraLoadS->text());
    signalMapper->setMapping(load403, load403->text());
    connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(PhotoLoadDialog(QString)));
}

QImage* MainWindow::GetImage(QString type)
{
    return p_ImgPool->GetImg(type);
}

WLPFlag MainWindow::SetImage(QImage img, QString type)
{
    return p_ImgPool->SetImage(img, type);
}

void MainWindow::PhotoLoadGeneral()
{
    QFileDialog dialog(this);
    dialog.setDirectory(g_Config.Get(WLPC_DIR_LOAD));
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setNameFilter(QString("Image Files (*.jpg *.jpeg *.tif *.tiff *.png *.gif *.bmp)"));
    QStringList fileNames;
    if (dialog.exec())
        fileNames = dialog.selectedFiles();

    QString path;
    //Call general loader
    foreach(QString fileName, fileNames)
    {
        PhotoLoad(fileName, "General");
        QFileInfo openedFile(fileName);
        path = openedFile.absolutePath();
    }

    //Set path as new default load path
    WLPSettings s; s.UpdateLoadDir(path);
}

void MainWindow::PhotoLoadDialog(QString _imgType)
{
    //Open load dialog
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    QString("Open Image - " + _imgType), g_Config.Get(WLPC_DIR_LOAD), QString("Image Files (*.jpg *.jpeg *.tif *.tiff *.png *.gif *.bmp)"));
    //Set path as new default load path
    QFileInfo openedFile(fileName);
    WLPSettings s; s.UpdateLoadDir(openedFile.absolutePath());

    //Call general loader
    PhotoLoad(fileName, _imgType);
}

void MainWindow::PhotoLoadDrag(QString _url)
{
    QString fileName = _url, imgType;
    bool isAccepted = true;

    QCheckBox *test = new QCheckBox("Load all images as general images?");
    connect(test, SIGNAL(clicked(bool)), this, SLOT(PhotoSetSpectral(bool)));
    test->setChecked(false);

    if(m_IsSpectral)
    {
        //Generate imgtype by prompting the user to choose
        QDialog dialog(this);
        QGridLayout *lay = new QGridLayout;
        lay->addWidget(new QLabel("File: " + fileName), 0, 0,1,2,Qt::AlignCenter);
        QComboBox *waveChoice = new QComboBox;
        waveChoice->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
        for(int i = 0; i < 7; ++i)
        {
            waveChoice->addItem(g_ImgTypes[i]);
        }
        waveChoice->setCurrentIndex(2); //Default to VIS
        lay->addWidget(waveChoice,1,0,1,2,Qt::AlignCenter);
        QPushButton *OkButt = new QPushButton("Load");
        connect(OkButt, SIGNAL(clicked()), &dialog, SLOT(accept()));
        lay->addWidget(OkButt,2,0,1,1,Qt::AlignCenter);
        QPushButton *Cancel = new QPushButton("Cancel");
        connect(Cancel, SIGNAL(clicked()), &dialog, SLOT(reject()));
        lay->addWidget(Cancel,2,1,1,1,Qt::AlignCenter);
        lay->addWidget(test, 3,0,1,2,Qt::AlignCenter);
        dialog.setLayout(lay);

        //Run the dialog and catch chosen wavelength
        isAccepted = dialog.exec();
        imgType = waveChoice->currentText();
    }

    //Catch it
    if(!m_IsSpectral)
    {
        fileName = _url;
        imgType = "General";
    }

    //Call general loader if accepted
    if(isAccepted)
        PhotoLoad(fileName, imgType);
}

void MainWindow::PhotoLoad(QString fileName, QString imgType)
{
    //Create image from file
    QImage image(fileName);

    if(image.isNull())
    {
      QMessageBox msgBox;
      msgBox.setText("Error");
      msgBox.exec();
      return;
    }

    if(imgType == "IR (720-1000nm)" && g_Config.Get(WLPC_FLIP_IR))
    {
        for(int i = 0; i < image.width(); ++i)
        {
            for(int j = 0; j < image.height(); ++j)
            {
                QColor temp = image.pixel(i,j);
                QColor col(temp.blue(), temp.green(), temp.red());
                image.setPixel(i,j,col.rgb());
            }
        }
    }

    QIcon icon(QPixmap::fromImage(image.scaled(200,200,Qt::KeepAspectRatio)));
    WLPFlag loadState = SetImage(image, imgType);
    if( loadState == WLP_LOAD_FIRST )
    {
        //Add image to pool
        if(imgType == "General")
        {
            qDebug() << imgType << " in if";
            p_ImgPool->m_GeneralGif.push_back(fileName);

            QFileInfo info(fileName);
            p_ImgPool->m_GenTexts.push_back(info.fileName());

            QListWidgetItem *tmp = new QListWidgetItem(icon, info.fileName(), 0, 0);
            p_PoolGeneralTest->addItem( tmp );
        }
        else
        {
            ++m_CountSpectral;
            p_PoolSpectral->addItem( new QListWidgetItem(icon, imgType, 0, 0) );
            //add to channel mixers
            p_ImgPool->m_Texts.push_back(imgType);

            //Only for Spectral Pool
            p_CloudAction->setEnabled(true);

        }
    }
    else if( loadState == WLP_LOAD_OVERWRITE )
    {
        for(int i = 0; i < m_CountSpectral; ++i)
        {
            if( p_PoolSpectral->item(i)->text() == imgType )
                p_PoolSpectral->item(i)->setIcon(icon);
        }
    }
    else { qDebug() << "Load Error :("; }

     //Actions that are enabled after any image has been loaded:
     hslMapAction->setEnabled(true);
     chanMixAction->setEnabled(true);
     p_ScaleAction->setEnabled(true);
     p_ToolsMenu->setEnabled(true);
     p_PoolMenu->setEnabled(true);

     if(p_ImgPool->m_IsLoaded[0])
     {
         eirChrome->setEnabled(true);
     }

     if(p_ImgPool->m_IsLoaded[1])
     {
        dualProaction->setEnabled(true);
     }

     if(p_ImgPool->m_IsLoaded[2])
     {
         rainbowAction->setEnabled(true);
         purityAction->setEnabled(true);
     }
}

MainWindow::~MainWindow()
{
    delete ui;
}

//======================================

//Generalised method for opening a tool (e.g., Channel mixer, Dual Process, etc)
template<class Tool>
bool WLPTool<Tool>::Run(QMdiArea *_specialArea, Tool *_tool, int _width, int _height, QIcon _icon)
{
    if(!objBool)
    {
        objBool = true;
        tool = _tool;
        tool->setAttribute(Qt::WA_DeleteOnClose, true);

        wnd= new QMdiSubWindow;
        wnd->setAttribute(Qt::WA_DeleteOnClose, true);
        _specialArea->addSubWindow(wnd);

        wnd->setWidget(tool);
        wnd->setMinimumSize(_width,_height);
        wnd->setWindowIcon(_icon);
        wnd->show();

        QObject::connect(tool, SIGNAL(destroyed()), &objBool, SLOT(SetFalse()));
        return true;
    }
    wnd->show();
    return false;
}

void MainWindow::runChrome()
{
    m_Infrachrome.Run(specialArea, new InfrachromeMethod(p_ImgPool), g_Config.Get(WLPC_WIDTH) + 250,500, this->windowIcon());
}

void MainWindow::runDual()
{
    m_DualProcess.Run(specialArea, new DualProcess(p_ImgPool), g_Config.Get(WLPC_WIDTH) + 400,g_Config.Get(WLPC_HEIGHT), this->windowIcon());
}

void MainWindow::runChanMix()
{
    m_ChannelMix.Run(specialArea, new SpectralExplorer(p_ImgPool), g_Config.Get(WLPC_WIDTH) + 300,g_Config.Get(WLPC_HEIGHT), this->windowIcon());
}

void MainWindow::runRainbow()
{
    m_HueExtract.Run(specialArea, new RainbowExtractor(p_ImgPool), g_Config.Get(WLPC_WIDTH) * 2,g_Config.Get(WLPC_HEIGHT)+50, this->windowIcon());
}

void MainWindow::runHSLMap()
{
    m_SpaceMix.Run(specialArea, new LuminanceMapper(p_ImgPool), g_Config.Get(WLPC_WIDTH),g_Config.Get(WLPC_HEIGHT)+100, this->windowIcon());
}

void MainWindow::runPurity()
{
    m_Purity.Run(specialArea, new ColourPurity(p_ImgPool, 0), g_Config.Get(WLPC_WIDTH),g_Config.Get(WLPC_HEIGHT)+50, this->windowIcon());
}

void MainWindow::runPool(WLPFlag _poolType)
{
    m_PoolEdit.Run(specialArea, new PoolEditor(p_ImgPool, _poolType), g_Config.Get(WLPC_WIDTH),g_Config.Get(WLPC_HEIGHT)+100, this->windowIcon());
}

//===================

void MainWindow::ExampleLinks()
{
    QLabel *label[4];
    QDialog diag(this);
    diag.setWindowTitle("Examples of Using WavelengthPro");
    QGridLayout *lay = new QGridLayout;

    label[0] = new QLabel("<a href=\"https://neuraloutlet.wordpress.com/2014/11/16/editing-ultraviolet-photography/\">Editing with Ultraviolet Images</a>");
    label[1] = new QLabel("<a href=\"https://neuraloutlet.wordpress.com/2014/02/27/full-spectrum-photography-mapping/\">Using the MultiChannel Mixer</a>");
    label[2] = new QLabel("<a href=\"https://www.youtube.com/watch?v=CuAkgZmj9DY\">Using the Colour-Space Mixer (Artistic)</a>");
    label[3] = new QLabel("<a href=\"https://neuraloutlet.wordpress.com/2014/03/17/luminance-mapping-uv-and-thermal/\">Using the Colour-Space Mixer (Technically)</a>");

    for(int i = 0; i < 4; ++i)
    {
        label[i]->setOpenExternalLinks(true);
        lay->addWidget(label[i]);
    }

    diag.setLayout(lay);
    diag.exec();
}

void MainWindow::AboutWLP()
{
    QMessageBox msg(this);
    msg.setWindowTitle("WavelengthPro Version 2.0 (Alpha)");
    msg.setIconPixmap(QPixmap(":/Icons/WavelengthPro.png").scaled(100,100, Qt::KeepAspectRatio));
    QString textbody = "WLP is a photo editing suite with enphasis on multi-spectral images.\n\n";

    textbody += "\tLoading Full - Activates IRG Film Emulator tool\n";
    textbody += "\tLoading UV - Activates Dual Process tool\n";
    textbody += "\tLoading VIS - Activates Rainbow Extractor tool\n";
    textbody += "\tLoading VIS - Activates Colour Purity tool\n";

    msg.setText(textbody);

    //Call it
    msg.exec();
}

void MainWindow::OpenSettings()
{
    WLPSettings set(this);
    set.exec();

    //Call twice to leave mode menu in
    //same state but with new defaults
    ShowHideMode();
    ShowHideMode();
}

//=====================================

void MainWindow::GEditorBright(QString _type)
{
    //Get active window
    QMdiSubWindow *tmp = specialArea->activeSubWindow();
    QString title = ( tmp->windowTitle() );
    if(title.contains("Image Viewer"))
    {
        for(int i = 0; i < 20; ++i)
        {
            if(p_ModeActions[i]->isChecked())
            {
                WLPMode mode = static_cast<WLPMode>(i);
                GeneralEditor* editor = new GeneralEditor(0, static_cast<GeneralView*>(tmp->widget()), mode, WLP_EDIT_BRIGHT, _type);

                if(!editor || editor == nullptr)
                {
                    QString error = "editor pointer cannot be null!";
                    throw std::runtime_error(error.toStdString());
                }

                QMdiSubWindow *poolEdit = new QMdiSubWindow;
                poolEdit->setWidget(editor);
                poolEdit->setAttribute(Qt::WA_DeleteOnClose, true);
                specialArea->addSubWindow(poolEdit);
                poolEdit->show();

                connect(editor->GetApplyButt(), SIGNAL(clicked()), poolEdit, SLOT(close()));
                connect(editor->GetCancelButt(), SIGNAL(clicked()), poolEdit, SLOT(close()));
            }
        }
    }
}

void MainWindow::GEditorControl()
{
    //Get active window
    QMdiSubWindow *tmp = specialArea->activeSubWindow();
    QString title = ( tmp->windowTitle() );

    if(title.contains("Image Viewer"))
    {
        GeneralEditor *editor = NULL;
        for(int i = 0; i < 20; ++i)
        {
            if(p_ModeActions[i]->isChecked())
            {
                WLPMode mode = static_cast<WLPMode>(i);
                editor = new GeneralEditor(0, static_cast<GeneralView*>(tmp->widget()), mode, WLP_EDIT_CONTROL);
            }
        }


        QMdiSubWindow *poolEdit = new QMdiSubWindow;
        poolEdit->setWidget(editor);
        poolEdit->setAttribute(Qt::WA_DeleteOnClose, true);
        specialArea->addSubWindow(poolEdit);
        poolEdit->show();

        connect(editor->GetApplyButt(), SIGNAL(clicked()), poolEdit, SLOT(close()));
        connect(editor->GetCancelButt(), SIGNAL(clicked()), poolEdit, SLOT(close()));
    }

}

void MainWindow::GEditorInvert()
{
    //Get active window
    QMdiSubWindow *tmp = specialArea->activeSubWindow();
    QString title = ( tmp->windowTitle() );

    if(title.contains("Image Viewer"))
    {
        GeneralEditor *editor = NULL;
        for(int i = 0; i < 20; ++i)
        {
            if(p_ModeActions[i]->isChecked())
            {
                WLPMode mode = static_cast<WLPMode>(i);
                editor = new GeneralEditor(0, static_cast<GeneralView*>(tmp->widget()), mode, WLP_EDIT_INVERT);
            }
        }


        QMdiSubWindow *poolEdit = new QMdiSubWindow;
        poolEdit->setWidget(editor);
        poolEdit->setAttribute(Qt::WA_DeleteOnClose, true);
        specialArea->addSubWindow(poolEdit);
        poolEdit->show();

        connect(editor->GetApplyButt(), SIGNAL(clicked()), poolEdit, SLOT(close()));
        connect(editor->GetCancelButt(), SIGNAL(clicked()), poolEdit, SLOT(close()));
    }

}

void MainWindow::dropEvent(QDropEvent *ev)
{
    QList<QUrl> urls = ev->mimeData()->urls();
    foreach(QUrl url, urls)
    {
        QString filename = url.toLocalFile();
        QString suffix = QFileInfo(filename).suffix().toUpper();
        if(suffix=="PNG" || suffix=="JPG" || suffix=="JPEG" || suffix=="TIF" || suffix=="GIF")
        {
            PhotoLoadDrag( filename );
            continue;
        }
    }

    //Reset back to normal
    m_IsSpectral = true;
}

void MainWindow::dragEnterEvent(QDragEnterEvent *ev)
{
    ev->accept();
}

void MainWindow::CatchDropped(QString _type)
{
    QImage _img = *GetImage(_type);
    QIcon icon(QPixmap::fromImage(_img.scaled(200,200,Qt::KeepAspectRatio)));
    WLPFlag loadState = SetImage(_img, _type);
    if( loadState == WLP_LOAD_FIRST )
    {
        //Add image to pool
        ++m_CountCreated;
        p_PoolCreated->addItem( new QListWidgetItem(icon, _type, 0, 0) );
        //add to channel mixers
        p_ImgPool->m_Texts.push_back(_type);
    }
    else if( loadState == WLP_LOAD_OVERWRITE )
    {
        for(int i = 0; i < m_CountCreated; ++i)
        {
            if( p_PoolCreated->item(i)->text() == _type )
                p_PoolCreated->item(i)->setIcon(icon);
        }
    }
    else { qDebug() << "Load Error :("; }
}

void MainWindow::OpenGifViewer(QPoint _point)
{
    QListWidgetItem* item = p_PoolGeneralTest->itemAt(_point);
    if(item)
    {
        PoolListClickedGeneralGif(p_PoolGeneralTest->GetIndex(item));
    }
}

void MainWindow::OpenContextMenu(QPoint _point)
{
    QMenu menu;
    QPoint openPoint = p_PoolSpectral->mapTo(this, _point);
    QListWidgetItem* item = p_PoolSpectral->itemAt(_point);
    if(item)
    {
        menu.addAction("Add filter (*)");
        menu.exec(openPoint);
    }

}

void MainWindow::ShowHideMode()
{
    bool showmore = false;
    if(p_ShowAll->text() == "More...")
    {
        showmore = true;
        p_ShowAll->setText("Less...");
    }
    else
        p_ShowAll->setText("More...");

    int modeA = g_Config.Get(WLPC_TOPMODE_1);
    int modeB = g_Config.Get(WLPC_TOPMODE_2);
    int modeC = g_Config.Get(WLPC_TOPMODE_3);
    int modeD = g_Config.Get(WLPC_TOPMODE_4);
    int modeE = g_Config.Get(WLPC_TOPMODE_5);

    for(int i = 0; i < 20; ++i)
    {
        p_ModeActions[i]->setVisible(true);
        if(i != modeA && i != modeB && i != modeC && i != modeD && i != modeE) //these are always shown
            p_ModeActions[i]->setVisible(showmore);
    }
}

void MainWindow::ChangeMode(QAction * _act)
{
    //Set it as checked
    _act->setChecked(true);

    //rgG,RGB,RGBW, HSL,HSV,HSI, CMY,CMYK, YUV,YIQ,YCbCr,
    //xyY,XYZ,Hunter-Lab,UVW,Luv,LCHuv,Lab,LCHab,LMS

    //Run through list to find it is checked and rename the labels
    QAction *control = p_AdjustMenu->actions().at(2);
    QString controlTypes[20] = { tr("Chromaticty"),tr("Colour-Balance"),tr("Colour-Balance"),
                                 tr("Hue/Saturation"), tr("Hue/Saturation"),tr("Hue/Saturation"),
                                 tr("Colour-Balance"),tr("Colour-Balance"),
                                 tr("Luma/Chroma"), tr("Luma/Chroma"), tr("Luma/Chroma"),
                                 tr("Chromaticity"),tr("XYZ Origin"), tr("Lightness/Chroma"),tr("Lightness/Chroma"),tr("Lightness/Chroma"), tr("Chromacity/Hue"),
                                 tr("Colour Opponancy"), tr("Chromacity/Hue"), tr("Cone Response")
                               };
    QAction *contrast = p_AdjustMenu->actions().at(4);
    QString contrastTypes[20] = { tr("Contrast (Green)"),tr("Brightnes/Contrast"),tr("Whiteness/Contrast"),
                                 tr("Saturation Contrast"), tr("Saturation Contrast"),tr("Saturation Contrast"),
                                 tr("Brightnes/Contrast"),tr("Blackness/Contrast"),
                                 tr("Luma/Contrast"), tr("Luma/Contrast"), tr("False"),
                                 tr("False"),tr("Brightness/Contrast"), tr("False"),tr("False"),tr("False"), tr("False"),
                                 tr("False"), tr("False"), tr("Brightness/Contrast")
                               };

    for(int i = 0; i < 20; ++i)
    {
        if(p_ModeActions[i]->isChecked())
        {
            control->setText(controlTypes[i]);
            contrast->setText(contrastTypes[i]);
            m_ContrastType = contrastTypes[i];
            contrast->setEnabled(true);

            if(contrastTypes[i] == "False")
            {
                contrast->setText("Brightness/Contrast");
                contrast->setDisabled(true);
            }
        }
    }


}

void MainWindow::Adjustment(QMdiSubWindow* _subWnd)
{
    if(_subWnd)
    {
        QString title = ( _subWnd->windowTitle() );
        if(title.contains("Image Viewer"))
        {
            p_AdjustMenu->setEnabled(true);
        }
        else
        {
            p_AdjustMenu->setDisabled(true);
        }
    }
}
