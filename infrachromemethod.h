#ifndef INFRACHROMEMETHOD_H
#define INFRACHROMEMETHOD_H

#include <QGroupBox>
#include <QGridLayout>
#include <QSpinBox>
#include <QWidget>
#include <QLayout>
#include <QLabel>
#include <QSlider>

#include "wlpTypes.h"

class InfrachromeMethod : public QWidget, public WLPBase
{
    Q_OBJECT

public:
    InfrachromeMethod(WLPPool *_pool, QWidget *p_Parent_Ptr = 0);

public slots:
    void Update() { Update(WLP_PREVIEW_ON); }
    void Update(WLPFlag p_PreviewState);
    void Render();
    void Drop();

    void SaveToFile();
    void LoadSettings();
    void RestoreSettings();
    void SaveSettings(bool msgOff);

    void SetRGB(){ m_ChanState = WLP_PREVIEW_RGB; }
    void SetRed(){ m_ChanState = WLP_PREVIEW_RED; }
    void SetGreen(){ m_ChanState = WLP_PREVIEW_GREEN; }
    void SetBlue(){ m_ChanState = WLP_PREVIEW_BLUE; }

private:
    WLPPool *p_Pool;
    QLabel *m_MainLabel_Ptr;
    QImage *m_Img_Ptr;
    QImage m_SmallImg;
    QImage m_OutImg;
    QImage m_DisplayImg;

    WLPFlag m_ChanState;
    QSlider m_AllSlides[5][2];
    QSpinBox m_AllSpins[5][2];
};

#endif // INFRACHROMEMETHOD_H
