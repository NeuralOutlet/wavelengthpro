#ifndef WLPDEFS_H
#define WLPDEFS_H

#include <QFileInfo>
#include <QFileDialog>
#include <QGenericMatrix>
#include <exception>
#include <stdexcept>
#include <QString>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <qmath.h>

//Define WLP_NULL
#ifndef nullptr
    #define nullptr 0
#endif
#define WLP_NULL nullptr



//General flags used by WavelengthPro
enum WLPFlag{
    //Pool image types (Spectral)
    WLP_POOL_FULL        = 0x0,
    WLP_POOL_UV          = 0x1,
    WLP_POOL_VIS         = 0x2,
    WLP_POOL_H_ALPHA     = 0x3,
    WLP_POOL_IR_WIDE     = 0x4,
    WLP_POOL_IR_SHORT    = 0x5,
    WLP_POOL_UVIR        = 0x6,
    WLP_POOL_IR_NIR      = 0x7, // Near IR (750-1400nm)
    WLP_POOL_IR_SWIR     = 0x8, // Short wave (1400-3000nm)
    WLP_POOL_IR_MWIR     = 0x9, // Mid wave (3000-8000nm)

    //Pool image types (Created)
    WLP_POOL_MIX_CHAN    = 0xa, // Channel Mix
    WLP_POOL_MIX_COL     = 0xb, // Colour-space Mix
    WLP_POOL_EIR         = 0xc, // IRG Image
    WLP_POOL_ORTHO       = 0xd, // Orthochromatic Image
    WLP_POOL_MASK        = 0xe, // Rainbow Extracted Image
    WLP_POOL_DUALP       = 0xf, // Dual Processed Image
    WLP_POOL_PURE        = 0x10, // Colour Pure Image

    WLP_POOL_MAX, // Array size

    //Render as preview flags
    WLP_PREVIEW_ON       = 0x20,
    WLP_PREVIEW_OFF      = 0x21,

    //RGB Channel preview
    WLP_PREVIEW_RGB      = 0x30,
    WLP_PREVIEW_RED      = 0x31,
    WLP_PREVIEW_GREEN    = 0x32,
    WLP_PREVIEW_BLUE     = 0x33,

    //General editing flags
    WLP_EDIT_NONE        = 0x50,
    WLP_EDIT_CONTROL     = 0x51,
    WLP_EDIT_INVERT      = 0x52,
    WLP_EDIT_BRIGHT      = 0x53,
    WLP_EDIT_WHITE       = 0x54,
    WLP_EDIT_LEVELS      = 0x55,
    WLP_EDIT_CURVES      = 0x56,

    WLP_EDIT_ALIGN       = 0x57,
    WLP_EDIT_CLOUD       = 0x58,
    WLP_EDIT_SCALE       = 0x59,

    //Colour-Space Update states
    WLP_UPDATE_BASE      = 0x60,
    WLP_UPDATE_A         = 0x61,
    WLP_UPDATE_B         = 0x62,
    WLP_UPDATE_C         = 0x63,
    WLP_UPDATE_ALL       = 0x64,
    WLP_UPDATE_PRESET    = 0x65,

    //Working space gammas:
    WLP_GAMMA_SRGB       = 0x70,
    WLP_GAMMA_L          = 0x71,
    WLP_GAMMA_VALUE      = 0x72,

    //RGB Working Spaces:
    WLP_WORKSPACE_OFFSET = 0x80,
    WLP_WORKSPACE_SRGB   = 0x80,
    WLP_WORKSPACE_ADOBE  = 0x81,
    WLP_WORKSPACE_APPLE  = 0x82,
    WLP_WORKSPACE_CIE    = 0x83,
    WLP_WORKSPACE_PRO    = 0x84,
    WLP_WORKSPACE_ECI    = 0x85,
    WLP_WORKSPACE_NTSC   = 0x86,
    WLP_WORKSPACE_PAL    = 0x87,
    WLP_WORKSPACE_WIDE   = 0x88,
    WLP_WORKSPACE_MATCH  = 0x89,
    WLP_WORKSPACE_BRUCE  = 0x8a,



    //N-Channel Formats:
    WLP_FORMAT_ERROR     = 0x100,
    WLP_FORMAT_1CHANNEL  = 0x101,
    WLP_FORMAT_2CHANNEL  = 0x102,
    WLP_FORMAT_3CHANNEL  = 0x103,
    WLP_FORMAT_4CHANNEL  = 0x104,
    WLP_FORMAT_5CHANNEL  = 0x105,

    //Loading flags
    WLP_LOAD_FIRST       = 0x1000,
    WLP_LOAD_OVERWRITE   = 0x1001
};

struct Tristimulus{
    double X;
    double Y;
    double Z;
    Tristimulus(){X=100; Y=100; Z=100;} //Standard E illuminant
    Tristimulus(double x, double y, double z){
        X=x; Y=y; Z=z;
    }
};

enum WLPMode
{
    WLP_MODE_RGC      = 0x00,
    WLP_MODE_RGB      = 0x01,
    WLP_MODE_RGBW     = 0x02,

    WLP_MODE_HSL      = 0x03,
    WLP_MODE_HSV      = 0x04,
    WLP_MODE_HSI      = 0x05,

    WLP_MODE_CMY      = 0x06,
    WLP_MODE_CMYK     = 0x07,

    WLP_MODE_YUV      = 0x08,
    WLP_MODE_YIQ      = 0x09,
    WLP_MODE_YCC      = 0x0a,

    WLP_MODE_XYY      = 0x0b,
    WLP_MODE_XYZ      = 0x0c, //1931
    WLP_MODE_HUNTER   = 0x0d, //1948
    WLP_MODE_UVW      = 0x0e, //1964
    WLP_MODE_LUV      = 0x0f, //1976
    WLP_MODE_LCH_UV   = 0x10,
    WLP_MODE_LAB      = 0x11, //1976
    WLP_MODE_LCH_AB   = 0x12,
    WLP_MODE_LMS      = 0x13  //CIECAM02 (2002)
};

//The base class for all tools that share attributes
class WLPBase
{
public:
    WLPBase(){ Initialise(); }
    void Initialise();

    static int s_ViewWidth;
    static int s_ViewHeight; 

    //All glory to the Static LUTs
    static QColor lutRGBtoLab[256][256][256];
    static QColor lutLabtoRGB[256][256][256];
    static bool lutSetLAB;
    static QColor lutRGBtoLCH[256][256][256];
    static QColor lutLCHtoRGB[256][256][256];
    static bool lutSetLCH;

    //Space conversions
    QColor RGBtoXYZ(QColor p_Col);
    QColor XYZtoRGB(QColor p_Col);
    QColor XYZtoLab(QColor p_Col);
    QColor LabtoLCH(QColor p_Col);
    QColor LCHtoLab(QColor p_Col);
    QColor LabtoXYZ(QColor p_Col);
    QColor RGBtoYIQ(QColor p_Col);
    QColor YIQtoRGB(QColor p_Col);
    QColor RGBtoYUV(QColor p_Col);
    QColor YUVtoRGB(QColor p_Col);
    QColor RGBtoHSL(QColor p_Col);
    QColor RGBtoHSV(QColor p_Col);
    QColor RGBtoRGC(QColor p_Col); //rg Chromacity
    QColor XYZtoLMS(QColor p_Col);
    QColor LMStoXYZ(QColor p_Col);
    QColor XYZtoLuv(QColor p_Col);
    QColor LuvtoXYZ(QColor p_Col);
    QColor XYZtoUVW(QColor p_Col);
    QColor UVWtoXYZ(QColor p_Col);
    QColor RGBtoCMYK(QColor p_Col);
    QColor RGBAtoHSI(QColor p_Col);
    QColor HSItoRGBA(QColor p_Col);
    QColor RGBtoHSI(QColor p_Col);
    QColor HSItoRGB(QColor p_Col);
    QColor CMYKtoCMY(QColor p_Col);
    QColor CMYtoCMYK(QColor p_Col);
    QColor RGBtoCMY(QColor p_Col);
    QColor RGBtoHLAB(QColor p_Col);
    QColor HLABtoXYZ(QColor p_Col);
    QColor XYZtoHLAB(QColor p_Col);
    QColor RGCtoRGB(QColor p_Col);

    //The convinience functions:
    QColor ConvertToRGB(QColor p_Col, WLPMode _mode);
    QColor ConvertTo(QColor _col, WLPMode _newMode, WLPMode _oldMode = WLP_MODE_RGB);

    //White Point
    Tristimulus m_Illuminant;
    void ChangeWhite(int _index);

    //Gamma
    WLPFlag m_GammaFlag;
    qreal m_Gamma;

    //RGB Working-Space
    void ChangeRGB(WLPFlag _workingSpace);

    //All the linear matricies for colour-space conversion
    QGenericMatrix<3,3,qreal> rgb2yiqMatrix;
    QGenericMatrix<3,3,qreal> yiq2rgbMatrix;

    QGenericMatrix<3,3,qreal> rgb2yuvMatrix;
    QGenericMatrix<3,3,qreal> yuv2rgbMatrix;

    QGenericMatrix<3,3,qreal> rgb2xyzMatrix;
    QGenericMatrix<3,3,qreal> xyz2rgbMatrix;

    QGenericMatrix<3,3,qreal> xyz2lmsMatrix;
    QGenericMatrix<3,3,qreal> lms2xyzMatrix;

    QGenericMatrix<3,1,qreal> rgbVector;
    QGenericMatrix<3,1,qreal> outSpace;
    QGenericMatrix<3,1,qreal> rgbOut;
};


// Thread flags used by WavelengthPro
enum WLPThreadFlag{
   WLPT_NONE         = 0x00,
   WLPT_LUTS_WRITE   = 0x01,
   WLPT_LUTS_READ    = 0x02
};

//WLPC = WavelengtPro Configurations
enum WLPC_IntFlag {
    WLPC_WELCOME_MSG = 0,

    WLPC_WIDTH,
    WLPC_HEIGHT,

    WLPC_BASE_IRG,
    WLPC_FRAC_RX,
    WLPC_LEVEL_R,
    WLPC_FRAC_GX,
    WLPC_LEVEL_G,
    WLPC_GAMMA_R_VIS,
    WLPC_GAMMA_R_IRCORR,
    WLPC_GAMMA_G_VIS,
    WLPC_GAMMA_G_IRCORR,
    WLPC_MASTER_LEVEL,
    WLPC_MASTER_GAMMA,

    WLPC_DROP_MAX,
    WLPC_FLIP_IR,

    WLPC_WORKSPACE,
    WLPC_ILLUMINANT,
    WLPC_SCREEN_GAMMA,

    WLPC_TOPMODE_1,
    WLPC_TOPMODE_2,
    WLPC_TOPMODE_3,
    WLPC_TOPMODE_4,
    WLPC_TOPMODE_5,

    WLPC_NOTE_MODULE,
    WLPC_NOTE_VARIABLE,
    WLPC_NOTE_NUMBER,
    WLPC_NOTE_STRING,
    WLPC_NOTE_KEYWORD,
    WLPC_NOTE_DEFAULT,
    WLPC_NOTE_COMMENT,

    WLPC_INT_SIZE //size of config array
};

enum WLPC_StrFlag {
    WLPC_DIR_SAVE = 0,
    WLPC_DIR_LOAD,

    WLPC_STR_SIZE //size of config array
};

//The actual confiuration struct
struct WLPConfig {
    QString strArray[WLPC_STR_SIZE];
    int numArray[WLPC_INT_SIZE];

    //Get function to return config string
    QString Get(WLPC_StrFlag flag){ return strArray[flag]; }
    //Get function to return config int
    int Get(WLPC_IntFlag flag){ return numArray[flag]; }

    //Set function for config string
    void Set(WLPC_StrFlag flag, QString str){ strArray[static_cast<int>(flag)] = str; }
    //Set function for config int
    void Set(WLPC_IntFlag flag, int val){ numArray[static_cast<int>(flag)] = val; }
};




#endif // WLPDEFS_H
