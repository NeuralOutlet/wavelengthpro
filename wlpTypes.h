#ifndef WLPTYPES_H
#define WLPTYPES_H

#include "wlpDefs.h"

#include <QMdiSubWindow>
#include <QMouseEvent>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMovie>
#include <QIcon>



extern int IndexFromString(QString _str);

class WLPPool : public QObject
 {
   Q_OBJECT

 public:
    WLPPool( QObject *parent=0 ) : QObject( parent )
   {
        for(int i = 0; i < WLP_POOL_MAX; ++i)
            m_IsLoaded[i] = false;
        m_GenCount = 0;
   }


    QImage * GetImg(QString _type)
    {
        int index = IndexFromString(_type);
        if(index == -1)
            return WLP_NULL;

        return &m_Specific[index];
    }
    QImage * GetImgGeneral(int _index)
    {
        return &m_General[_index];
    }

    WLPFlag SetImage(QImage _img, QString _type)
    {
        //Record file size if largest in pool
        if(m_MaxSize.area < (_img.width() * _img.height()))
        {
            m_MaxSize.width = _img.width();
            m_MaxSize.height = _img.height();
            m_MaxSize.CalcArea();
        }

        WLPFlag state = WLP_LOAD_FIRST;
        if(!(_type == "General"))
        {
            int i = IndexFromString(_type);
            m_Specific[i] = _img;
            //Saftey check
            if(m_IsLoaded[i])
                state = WLP_LOAD_OVERWRITE;
            m_IsLoaded[i] = true;
        }
        else //General Images
        {
            ++m_GenCount;
            m_General.push_back(_img);
        }

        return state;
    }

    QImage * GetPool(){
        return m_Specific;
    }

    void Drop(QString _str){ emit DropSig(_str); }

    std::vector<QString> m_Texts;
    std::vector<QString> m_GenTexts;
    bool m_IsLoaded[WLP_POOL_MAX];

    //The dimensions of the largest image
    struct{
        int width; int height; int area;
        void CalcArea(){ area = width * height; }
    }m_MaxSize;
    int m_GenCount;

    std::vector<QImage> m_General;
    std::vector<QString> m_GeneralGif;

 signals:
   void DropSig(QString);

 public slots:
   void Nothing(){}

private:
   QImage m_Specific[WLP_POOL_MAX];
};

class QObjectBool : public QObject
 {
   Q_OBJECT

 public:
    QObjectBool( QObject *parent=0 ) : QObject( parent ), m_Active(0)
   {
   }

   int m_Active;

   QObjectBool& operator=(bool val){ m_Active = val; return *this; }
   operator bool() const { return (m_Active != 0); }

 public slots:
   void SetTrue(){ m_Active = true; }
   void SetFalse(){ m_Active = false; }
   void SetInt(int val){ m_Active = val; }
 };


template<class Tool>
class WLPTool
{
public:
    WLPTool() : isActive(false) { }
    Tool *tool;
    QMdiSubWindow *wnd;
    bool isActive;
    QObjectBool objBool;

    bool Run(QMdiArea *_specialArea, Tool *_tool, int _width, int _height, QIcon _icon = QIcon());
};

class ImageLister : public QListWidget
{
    Q_OBJECT

public:
    ImageLister( QListWidget *parent=0 ) : QListWidget( parent ){}

    QModelIndex GetIndex(QListWidgetItem *_item){ return indexFromItem(_item); }

signals:
    void RightClicked(QModelIndex _index);
    void LeftClicked(QModelIndex _index);

public slots:

};

#endif //WLPTYPES_H
