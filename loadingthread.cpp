#include "loadingthread.h"



//Constructor to set up non-threaded loadbar (GUI side)
LoadingThread::LoadingThread(MainWindow *_wnd)
{
        connect(this, SIGNAL(LoadText(QString)), _wnd, SLOT(LoadText(QString)));
        connect(this, SIGNAL(LoadUpdate(int)), _wnd, SLOT(LoadUpdate(int)));
        connect(this, SIGNAL(LoadComplete()), _wnd, SLOT(LoadComplete()));
}

//Function for getting the from and to of a 3DLUT:
void LoadingThread::GetLUTs(int _a, int _b, int _c, QColor &_toSpace, QColor &_fromSpace, QString colSpace)
{
    //Add more later
    if(colSpace == "LAB")
    {
        _toSpace = XYZtoLab(RGBtoXYZ(QColor::fromRgb(_a,_b,_c)));
        _fromSpace = XYZtoRGB(LabtoXYZ(QColor::fromRgb(_a,_b,_c)));
    }
    else if(colSpace == "YIQ")
    {
        _toSpace = RGBtoYIQ(QColor::fromRgb(_a,_b,_c));
        _fromSpace = YIQtoRGB(QColor::fromRgb(_a,_b,_c));
    }
    else if(colSpace == "LCHab")
    {
        _toSpace = LabtoLCH(XYZtoLab(RGBtoXYZ(QColor::fromRgb(_a,_b,_c))));
        _fromSpace = XYZtoRGB(LabtoXYZ(LCHtoLab(QColor::fromRgb(_a,_b,_c))));
    }
    else
    {
        qDebug() << "LUT not recognised.";
        _toSpace = _fromSpace = QColor(255,0,0); //Red for error
    }
}

//The thread, to update the loading bar a signal needs
//to be emitted and picked up a slot in the main GUI thread
void LoadingThread::run()
{
    QColor toSpace, fromSpace;
    qreal outLUT[3];
    qreal inLUT[3];

    //In a general solution spaceName will change through all LUTs
    QString luts[2] = { "LAB", "LCHab" };
    for(int i = 0; i < 2; ++i)
    {
        QString spaceName = luts[i];

        std::string fileName = spaceName.toStdString() + ".lut";

        //Use ifstream fail to see if file exists or not
        std::ifstream file(fileName.c_str());
        if(!file.fail())
        {
          file.close();
          std::ifstream in(fileName.c_str(),std::ios_base::binary);
          if(in.good())
          {
              QString loadText = "Reading in 3DLUT: " + spaceName;
              emit LoadText(loadText);

              for(int r = 0; r < 256; ++r)
              {
                  for(int g = 0; g < 256; ++g)
                  {
                      for(int b = 0; b < 256; ++b)
                      {
                          in.read((char *)&inLUT,sizeof(qreal)*3);
                          if(i == 0) lutRGBtoLab[r][g][b] = QColor::fromRgbF(inLUT[0], inLUT[1], inLUT[2]);
                          if(i == 1) lutRGBtoLCH[r][g][b] = QColor::fromRgbF(inLUT[0], inLUT[1], inLUT[2]);
                          in.read((char *)&inLUT,sizeof(qreal)*3);
                          if(i == 0) lutLabtoRGB[r][g][b] = QColor::fromRgbF(inLUT[0], inLUT[1], inLUT[2]);
                          if(i == 1) lutLCHtoRGB[r][g][b] = QColor::fromRgbF(inLUT[0], inLUT[1], inLUT[2]);
                      }
                  }

                  //Update loading bar from between 0-100%
                  qreal progress = static_cast<qreal>(r) / 255.0;
                  emit LoadUpdate(progress * 100);
              }
              in.close();
              qDebug() << "Finished reading LUT: " + spaceName;
          }
        }
        else
        {
            std::ofstream out(fileName.c_str(), std::ios_base::binary);
            if(out.good())
            {
                QString loadText = "Generating 3DLUT: " + spaceName;
                emit LoadText(loadText);

                for(int r = 0; r < 256; ++r)
                {
                    for(int g = 0; g < 256; ++g)
                    {
                        for(int b = 0; b < 256; ++b)
                        {
                            GetLUTs(r,g,b, toSpace, fromSpace, spaceName);

                            //To Space
                            outLUT[0] = toSpace.redF();
                            outLUT[1] = toSpace.greenF();
                            outLUT[2] = toSpace.blueF();
                            out.write((char *)&outLUT,sizeof(qreal)*3);
                            if(i == 0) lutRGBtoLab[r][g][b] = QColor::fromRgbF(outLUT[0], outLUT[1], outLUT[2]);
                            if(i == 1) lutRGBtoLCH[r][g][b] = QColor::fromRgbF(outLUT[0], outLUT[1], outLUT[2]);

                            //From Space
                            outLUT[0] = fromSpace.redF();
                            outLUT[1] = fromSpace.greenF();
                            outLUT[2] = fromSpace.blueF();
                            out.write((char *)&outLUT,sizeof(qreal)*3);
                            if(i == 0) lutLabtoRGB[r][g][b] = QColor::fromRgbF(outLUT[0], outLUT[1], outLUT[2]);
                            if(i == 1) lutLCHtoRGB[r][g][b] = QColor::fromRgbF(outLUT[0], outLUT[1], outLUT[2]);
                        }
                    }

                    //Update loading bar from between 0-100%
                    qreal progress = static_cast<qreal>(r) / 255.0;
                    emit LoadUpdate(progress * 100);
                }
                out.close();
                qDebug() << "Finished generating LUT: " + spaceName;
            }else { qDebug() << "Horrific LUT Failure"; }
        }

        //Clear the loading bar widget
        emit LoadComplete();

        //Activate use for the LUT by tools
        if(i == 0) lutSetLAB = true;
        if(i == 1) lutSetLCH = true;
    }
}
