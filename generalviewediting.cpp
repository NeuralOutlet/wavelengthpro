#include "generalviewediting.h"

#include <QCheckBox>

GeneralView::GeneralView(QWidget *p_Parent) :
    ImageExplorer(p_Parent), WLPBase() //QWidget(p_Parent)
{

}

GeneralView::GeneralView(QString p_Channel, QImage *p_Img_Ptr, WLPFlag p_EditType = WLP_EDIT_NONE, QWidget *p_Parent = 0) :
    ImageExplorer(p_Parent)
{
    m_Img_Ptr = p_Img_Ptr;
    m_ImgScaled = m_Img_Ptr->scaled(s_ViewWidth,s_ViewHeight, Qt::KeepAspectRatio);
    m_ImgDisplay = m_ImgScaled;

    setImage(m_ImgDisplay, false);

    //m_Label = new QLabel;
    //m_Label->setPixmap( QPixmap::fromImage( m_ImgDisplay ) );

    //m_Layout = new QGridLayout;
    //m_Layout->addWidget(m_Label);
    //m_Layout->addWidget(p_explorer);

    //setLayout(m_Layout);
    setWindowTitle("Image Viewer: " + p_Channel);

    Q_UNUSED(p_EditType);
}

void GeneralView::Update()
{
    setImage(m_ImgDisplay, true);
}

//=========================================================

GeneralEditor::GeneralEditor(QWidget *p_Parent_Ptr, GeneralView *p_Viewer_Ptr, WLPMode p_Mode, WLPFlag p_EditType, QString _info) :
    QWidget(p_Parent_Ptr)
{
    //Colourspace debugging:
    lowest = highest = 0;
    setWindowIcon( p_Parent_Ptr->windowIcon() );

    m_Viewer_Ptr = p_Viewer_Ptr;
    m_EditType = p_EditType;
    m_Mode = p_Mode;

    m_Layout_Ptr = new QGridLayout;
    m_Layout_Ptr->addWidget(new QLabel("Image from pool:"), 0,0);
    m_Layout_Ptr->addWidget(new QLabel(p_Viewer_Ptr->windowTitle()), 0,1);

    //Close on Viewer destroyed
    connect(p_Viewer_Ptr, SIGNAL(destroyed()), this, SLOT(close()));
    //Focus the active image viewer on destroy
    connect(this, SIGNAL(destroyed()), p_Viewer_Ptr, SLOT(setFocus()));

    if(m_EditType == WLP_EDIT_CONTROL)
    {
        SetUpControl();
    }
    else if(m_EditType == WLP_EDIT_BRIGHT)
    {
        SetUpBright(_info);
    }
    else if(m_EditType == WLP_EDIT_INVERT)
    {
        SetUpInvert();
    }
    else{}
}

void GeneralEditor::SetUpControl()
{
    //Set particulars for each mode
    QLabel *labelA, *labelB, *labelC;

    //rgG,RGB,RGBW, HSL,HSV,HSI, CMY,CMYK, YUV,YIQ,YCbCr,
    //xyY,XYZ,Hunter-Lab,UVW,Luv,LCHuv,Lab,LCHab,LMS

    QString elementA[] = { tr("Red Chromaticity"),tr("Red"),tr("Red %"), tr("Hue"),tr("Hue"),tr("Hue"), tr("Cyan"),tr("Cyan"), tr("Luma (Y)"),tr("Luma (Y)"),tr("Luma (Y)"),
                          tr("X Chromaticity"),tr("X"),tr("Lumnance (L)"),tr("Chroma (U)"),tr("Luminance (L)"),tr("Luminance (L)"),tr("Luminance (L)"),tr("Luminance (L)"),tr("Long")
                        };
    QString elementB[] = { tr("Green Chromaticity"),tr("Green"),tr("Green %"), tr("Saturation"),tr("Saturation"),tr("Saturation"), tr("Magenta"),tr("Magenta"), tr("Chroma (U)"),tr("In-Phase"),tr("YCbCr"),
                          tr("Y Chromaticity"),tr("Y"),tr("a"),tr("Chroma (V)"),tr("Chroma (u)"),tr("Chromanance (C)"),tr("Chroma (a)"),tr("Chromanance (C)"),tr("Medium")
                        };
    QString elementC[] = { tr("Green"),tr("Blue"),tr("Blue %"), tr("Lightness"),tr("Value"),tr("Intensity"), tr("Yellow"),tr("Yellow"), tr("Chroma (V)"),tr("Quadrature"),tr("YCbCr"),
                          tr("Y"),tr("Z"),tr("b"),tr("Luminance (W)"),tr("Chroma (v)"),tr("Hue"),tr("Chroma (b)"),tr("Hue"),tr("Short")
                        };
    QString modeStr[] = { tr("rgG"),tr("RGB"),tr("RGBW"), tr("HSL"),tr("HSV"),tr("HSI"), tr("CMY"),tr("CMYK"), tr("YUV"),tr("YIQ"),tr("YCbCr"),
                          tr("xyY"),tr("XYZ"),tr("Hunter-Lab"),tr("UVW"),tr("Luv"),tr("LCHuv"),tr("Lab"),tr("LCHab"),tr("LMS")
                        };


    int index = static_cast<int>(m_Mode);
    int rangeA, rangeB, rangeC, rangeD;

    labelA = new QLabel(elementA[index]);
    labelB = new QLabel(elementB[index]);
    labelC = new QLabel(elementC[index]);
    QString forth = m_Mode == WLP_MODE_CMYK ? "Key (Black)" : "White %";
    QLabel *labelD = new QLabel(forth);
    setWindowTitle(modeStr[index]);

    rangeA = rangeB = rangeC = rangeD = 255;
    if( index == 16 && index == 18) //Not Hue:  //LCH (ab/uv)
    {
        rangeC = 180;
    }

    if(index >= 3 && index <= 5)  //Not Hue:  //HS (L/V/I)
    {
        rangeA = 180;
    }

    m_ControlA_Ptr = new QSlider(Qt::Horizontal);
    m_ControlA_Ptr->setRange(-rangeA, rangeA);
    m_ControlA_Ptr->setValue(0);
    QSpinBox *controlBoxA = new QSpinBox;
    controlBoxA->setRange(-rangeA, rangeA);
    controlBoxA->setValue(0);
    connect(m_ControlA_Ptr, SIGNAL(valueChanged(int)), controlBoxA, SLOT(setValue(int)));
    connect(controlBoxA, SIGNAL(valueChanged(int)), m_ControlA_Ptr, SLOT(setValue(int)));
    connect(m_ControlA_Ptr, SIGNAL(valueChanged(int)), this, SLOT(UpdateControl()));

    m_ControlB_Ptr = new QSlider(Qt::Horizontal);
    m_ControlB_Ptr->setRange(-rangeB, rangeB);
    m_ControlB_Ptr->setValue(0);
    QSpinBox *controlBoxB = new QSpinBox;
    controlBoxB->setRange(-rangeB, rangeB);
    controlBoxB->setValue(0);
    connect(m_ControlB_Ptr, SIGNAL(valueChanged(int)), controlBoxB, SLOT(setValue(int)));
    connect(controlBoxB, SIGNAL(valueChanged(int)), m_ControlB_Ptr, SLOT(setValue(int)));
    connect(m_ControlB_Ptr, SIGNAL(valueChanged(int)), this, SLOT(UpdateControl()));

    m_ControlC_Ptr = new QSlider(Qt::Horizontal);
    m_ControlC_Ptr->setRange(-rangeC, rangeC);
    m_ControlC_Ptr->setValue(0);
    QSpinBox *controlBoxC = new QSpinBox;
    controlBoxC->setRange(-rangeC, rangeC);
    controlBoxC->setValue(0);
    connect(m_ControlC_Ptr, SIGNAL(valueChanged(int)), controlBoxC, SLOT(setValue(int)));
    connect(controlBoxC, SIGNAL(valueChanged(int)), m_ControlC_Ptr, SLOT(setValue(int)));
    connect(m_ControlC_Ptr, SIGNAL(valueChanged(int)), this, SLOT(UpdateControl()));

    m_ApplyButton_Ptr = new QPushButton("Apply Changes");
    connect(m_ApplyButton_Ptr, SIGNAL(clicked()), this, SLOT(SaveControl()));
    connect(m_ApplyButton_Ptr, SIGNAL(clicked()), this, SLOT(close()));
    m_CancelButton_Ptr = new QPushButton("Cancel");
    connect(m_CancelButton_Ptr, SIGNAL(clicked()), this, SLOT(Cancel()));
    connect(m_CancelButton_Ptr, SIGNAL(clicked()), this, SLOT(close()));

    m_Layout_Ptr->addWidget(labelA, 1,0);
    m_Layout_Ptr->addWidget(m_ControlA_Ptr, 1,1);
    m_Layout_Ptr->addWidget(controlBoxA, 1,2);
    m_Layout_Ptr->addWidget(labelB, 2,0);
    m_Layout_Ptr->addWidget(m_ControlB_Ptr, 2,1);
    m_Layout_Ptr->addWidget(controlBoxB, 2,2);
    m_Layout_Ptr->addWidget(labelC, 3,0);
    m_Layout_Ptr->addWidget(m_ControlC_Ptr, 3,1);
    m_Layout_Ptr->addWidget(controlBoxC, 3,2);
    m_Layout_Ptr->addWidget(m_ApplyButton_Ptr, 5,1);
    m_Layout_Ptr->addWidget(m_CancelButton_Ptr, 5,2);

    //In the case of a fourth element
    if(m_Mode == WLP_MODE_CMYK || m_Mode == WLP_MODE_RGBW)
    {
        p_ControlD = new QSlider(Qt::Horizontal);
        p_ControlD->setRange(-rangeC, rangeC);
        p_ControlD->setValue(0);
        QSpinBox *controlBoxD = new QSpinBox;
        controlBoxD->setRange(-rangeD, rangeD);
        controlBoxD->setValue(0);
        connect(p_ControlD, SIGNAL(valueChanged(int)), controlBoxD, SLOT(setValue(int)));
        connect(controlBoxD, SIGNAL(valueChanged(int)), p_ControlD, SLOT(setValue(int)));
        connect(p_ControlD, SIGNAL(valueChanged(int)), this, SLOT(UpdateControl()));
        m_Layout_Ptr->addWidget(labelD, 4,0);
        m_Layout_Ptr->addWidget(p_ControlD, 4,1);
        m_Layout_Ptr->addWidget(controlBoxD, 4,2);
    }

    setLayout(m_Layout_Ptr);
}


void GeneralEditor::SetUpBright(QString _info)
{
    QString componentA[20] = { tr("G"),tr("RGB"),tr("W"),
                                 tr("L"), tr("V"),tr("I"),
                                 tr("CMY"),tr("K"),
                                 tr("Y"), tr("Y"), tr("Err"),
                                 tr("Err"),tr("XYZ"), tr("Err"),tr("Err"),tr("Err"), tr("Err"),
                                 tr("Err"), tr("Err"), tr("LMS")
                               };
    QString componentB[20] = { tr("G"),tr("RGB"),tr("W"),
                               tr("L"), tr("V"),tr("I"),
                               tr("CMY"),tr("K"),
                               tr("Y"), tr("Y"), tr("Err"),
                               tr("Err"),tr("XYZ"), tr("Err"),tr("Err"),tr("Err"), tr("Err"),
                               tr("Err"), tr("Err"), tr("LMS")
                             };
    //Set particulars for each mode
    QLabel *labelA = new QLabel("Brightness");
    QLabel *labelB = new QLabel("Contrast");
    QLabel *labelC, *labelD;
    int rangeA = 0, rangeB = 0, rangeC = 0, rangeD = 0;

    labelA->setText("Brightness (" + componentA[m_Mode] + "):");
    labelB->setText("Contrast (" + componentB[m_Mode] + "):");
    if(m_Mode == WLP_MODE_HSL || m_Mode == WLP_MODE_HSI || m_Mode == WLP_MODE_HSV)
    {
        labelC = new QLabel("Saturation (S):");
        labelD = new QLabel("Contrast (S):");
    }
    rangeA = 255;
    rangeB = 255;
    rangeC = 255;
    rangeD = 255;

    setWindowTitle(_info);

    m_ControlA_Ptr = new QSlider(Qt::Horizontal);
    m_ControlA_Ptr->setRange(-rangeA, rangeA);
    m_ControlA_Ptr->setValue(0);
    QSpinBox *controlBoxA = new QSpinBox;
    controlBoxA->setRange(-rangeA, rangeA);
    controlBoxA->setValue(0);
    connect(m_ControlA_Ptr, SIGNAL(valueChanged(int)), controlBoxA, SLOT(setValue(int)));
    connect(controlBoxA, SIGNAL(valueChanged(int)), m_ControlA_Ptr, SLOT(setValue(int)));
    connect(m_ControlA_Ptr, SIGNAL(valueChanged(int)), this, SLOT(UpdateBright()));

    m_ControlB_Ptr = new QSlider(Qt::Horizontal);
    m_ControlB_Ptr->setRange(-rangeB, rangeB);
    m_ControlB_Ptr->setValue(0);
    QSpinBox *controlBoxB = new QSpinBox;
    controlBoxB->setRange(-rangeB, rangeB);
    controlBoxB->setValue(0);
    connect(m_ControlB_Ptr, SIGNAL(valueChanged(int)), controlBoxB, SLOT(setValue(int)));
    connect(controlBoxB, SIGNAL(valueChanged(int)), m_ControlB_Ptr, SLOT(setValue(int)));
    connect(m_ControlB_Ptr, SIGNAL(valueChanged(int)), this, SLOT(UpdateBright()));

    m_ControlC_Ptr = new QSlider(Qt::Horizontal);
    m_ControlC_Ptr->setRange(-rangeC, rangeC);
    m_ControlC_Ptr->setValue(0);
    QSpinBox *controlBoxC = new QSpinBox;
    controlBoxC->setRange(-rangeC, rangeC);
    controlBoxC->setValue(0);
    connect(m_ControlC_Ptr, SIGNAL(valueChanged(int)), controlBoxC, SLOT(setValue(int)));
    connect(controlBoxC, SIGNAL(valueChanged(int)), m_ControlC_Ptr, SLOT(setValue(int)));
    connect(m_ControlC_Ptr, SIGNAL(valueChanged(int)), this, SLOT(UpdateBright()));

    m_ControlD_Ptr = new QSlider(Qt::Horizontal);
    m_ControlD_Ptr->setRange(-rangeD, rangeD);
    m_ControlD_Ptr->setValue(0);
    QSpinBox *controlBoxD = new QSpinBox;
    controlBoxD->setRange(-rangeD, rangeD);
    controlBoxD->setValue(0);
    connect(m_ControlD_Ptr, SIGNAL(valueChanged(int)), controlBoxD, SLOT(setValue(int)));
    connect(controlBoxD, SIGNAL(valueChanged(int)), m_ControlD_Ptr, SLOT(setValue(int)));
    connect(m_ControlD_Ptr, SIGNAL(valueChanged(int)), this, SLOT(UpdateBright()));

    m_ApplyButton_Ptr = new QPushButton("Apply Changes");
    connect(m_ApplyButton_Ptr, SIGNAL(clicked()), this, SLOT(SaveBright()));
    connect(m_ApplyButton_Ptr, SIGNAL(clicked()), this, SLOT(close()));
    m_CancelButton_Ptr = new QPushButton("Cancel");
    connect(m_CancelButton_Ptr, SIGNAL(clicked()), this, SLOT(Cancel()));
    connect(m_CancelButton_Ptr, SIGNAL(clicked()), this, SLOT(close()));

    m_Layout_Ptr->addWidget(labelA, 1,0);
    m_Layout_Ptr->addWidget(m_ControlA_Ptr, 1,1);
    m_Layout_Ptr->addWidget(controlBoxA, 1,2);
    m_Layout_Ptr->addWidget(labelB, 2,0);
    m_Layout_Ptr->addWidget(m_ControlB_Ptr, 2,1);
    m_Layout_Ptr->addWidget(controlBoxB, 2,2);
    if(m_Mode == WLP_MODE_HSL || m_Mode == WLP_MODE_HSV)
    {
        m_Layout_Ptr->addWidget(labelC, 3,0);
        m_Layout_Ptr->addWidget(m_ControlC_Ptr, 3,1);
        m_Layout_Ptr->addWidget(controlBoxC, 3,2);

        m_Layout_Ptr->addWidget(labelD, 4,0);
        m_Layout_Ptr->addWidget(m_ControlD_Ptr, 4,1);
        m_Layout_Ptr->addWidget(controlBoxD, 4,2);
    }
    m_Layout_Ptr->addWidget(m_ApplyButton_Ptr, 5,1);
    m_Layout_Ptr->addWidget(m_CancelButton_Ptr, 5,2);
    setLayout(m_Layout_Ptr);
}

void GeneralEditor::SetUpInvert()
{
    //Set particulars for each mode
    QLabel *labelA, *labelB, *labelC;

    //rgG,RGB,RGBW, HSL,HSV,HSI, CMY,CMYK, YUV,YIQ,YCbCr,
    //xyY,XYZ,Hunter-Lab,UVW,Luv,LCHuv,Lab,LCHab,LMS

    QString elementA[] = { tr("Red Chromaticity"),tr("Red"),tr("Red %"), tr("Hue"),tr("Hue"),tr("Hue"), tr("Cyan"),tr("Cyan"), tr("Luma (Y)"),tr("Luma (Y)"),tr("Luma (Y)"),
                          tr("X Chromaticity"),tr("X"),tr("Lumnance (L)"),tr("Chroma (U)"),tr("Luminance (L)"),tr("Luminance (L)"),tr("Luminance (L)"),tr("Luminance (L)"),tr("Long")
                        };
    QString elementB[] = { tr("Green Chromaticity"),tr("Green"),tr("Green %"), tr("Saturation"),tr("Saturation"),tr("Saturation"), tr("Magenta"),tr("Magenta"), tr("Chroma (U)"),tr("In-Phase"),tr("YCbCr"),
                          tr("Y Chromaticity"),tr("Y"),tr("a"),tr("Chroma (V)"),tr("Chroma (u)"),tr("Chromanance (C)"),tr("Chroma (a)"),tr("Chromanance (C)"),tr("Medium")
                        };
    QString elementC[] = { tr("Green"),tr("Blue"),tr("Blue %"), tr("Lightness"),tr("Value"),tr("Intensity"), tr("Yellow"),tr("Yellow"), tr("Chroma (V)"),tr("Quadrature"),tr("YCbCr"),
                          tr("Y"),tr("Z"),tr("b"),tr("Luminance (W)"),tr("Chroma (v)"),tr("Hue"),tr("Chroma (b)"),tr("Hue"),tr("Short")
                        };
    QString modeStr[] = { tr("rgG"),tr("RGB"),tr("RGBW"), tr("HSL"),tr("HSV"),tr("HSI"), tr("CMY"),tr("CMYK"), tr("YUV"),tr("YIQ"),tr("YCbCr"),
                          tr("xyY"),tr("XYZ"),tr("Hunter-Lab"),tr("UVW"),tr("Luv"),tr("LCHuv"),tr("Lab"),tr("LCHab"),tr("LMS")
                        };


    int index = static_cast<int>(m_Mode);
    labelA = new QLabel(elementA[index]);
    labelB = new QLabel(elementB[index]);
    labelC = new QLabel(elementC[index]);
    QString forth = m_Mode == WLP_MODE_CMYK ? "Key (Black)" : "White %";
    m_Mode == WLP_MODE_LAB ? forth = "Flip on Axis" : forth = forth;
    QLabel *labelD = new QLabel(forth);
    setWindowTitle("Invert Channels (" + modeStr[index] + ")");

    p_InvertA = new QCheckBox;
    p_InvertB = new QCheckBox;
    p_InvertC = new QCheckBox;
    connect(p_InvertA, SIGNAL(clicked()), this, SLOT(UpdateInvert()));
    connect(p_InvertB, SIGNAL(clicked()), this, SLOT(UpdateInvert()));
    connect(p_InvertC, SIGNAL(clicked()), this, SLOT(UpdateInvert()));


    m_ApplyButton_Ptr = new QPushButton("Apply Changes");
    connect(m_ApplyButton_Ptr, SIGNAL(clicked()), this, SLOT(SaveInvert()));
    connect(m_ApplyButton_Ptr, SIGNAL(clicked()), this, SLOT(close()));
    m_CancelButton_Ptr = new QPushButton("Cancel");
    connect(m_CancelButton_Ptr, SIGNAL(clicked()), this, SLOT(Cancel()));
    connect(m_CancelButton_Ptr, SIGNAL(clicked()), this, SLOT(close()));

    m_Layout_Ptr->addWidget(labelA, 1,0);
    m_Layout_Ptr->addWidget(p_InvertA, 1,1);
    m_Layout_Ptr->addWidget(labelB, 2,0);
    m_Layout_Ptr->addWidget(p_InvertB, 2,1);
    m_Layout_Ptr->addWidget(labelC, 3,0);
    m_Layout_Ptr->addWidget(p_InvertC, 3,1);
    m_Layout_Ptr->addWidget(m_ApplyButton_Ptr, 5,0);
    m_Layout_Ptr->addWidget(m_CancelButton_Ptr, 5,1);

    //In the case of a fourth element
    if(m_Mode == WLP_MODE_CMYK || m_Mode == WLP_MODE_RGBW)
    {
        p_InvertD = new QCheckBox;
        connect(p_InvertD, SIGNAL(clicked()), this, SLOT(UpdateInvert()));
        m_Layout_Ptr->addWidget(labelD, 4,0);
        m_Layout_Ptr->addWidget(p_InvertD, 4,1);
    }

    setLayout(m_Layout_Ptr);
}

void GeneralEditor::UpdateControl(WLPFlag _updateState)
{
    int aDelta = m_ControlA_Ptr->value();
    int bDelta = m_ControlB_Ptr->value();
    int cDelta = m_ControlC_Ptr->value();
    int dDelta = 0;

    if(m_Mode == WLP_MODE_CMYK || m_Mode == WLP_MODE_RGBW)
        dDelta = p_ControlD->value();

    //temp
    QColor temp;
    QRgb col;

    QImage tmp;
    if(_updateState == WLP_PREVIEW_ON)
        tmp = m_Viewer_Ptr->m_ImgScaled;
    else
        tmp = *(m_Viewer_Ptr->m_Img_Ptr);

    for (int x = 0; x < tmp.width(); ++x)
    {
        for (int y = 0; y < tmp.height(); ++y)
        {
             QColor vis(tmp.pixel(x,y));
             temp = vis;

             if( m_Mode != WLP_MODE_HSL && m_Mode != WLP_MODE_HSV && m_Mode != WLP_MODE_RGBW
                     && m_Mode != WLP_MODE_CMYK)
             {
                 //VConvert to space, offset channels, then convert back
                 temp = ConvertTo(temp, m_Mode);
                 if(m_Mode == WLP_MODE_RGC) temp.setBlueF( vis.greenF() );
                 qreal r = qBound<qreal>(0, temp.redF() + (aDelta/255.0), 1);
                 qreal g = qBound<qreal>(0, temp.greenF() + (bDelta/255.0), 1);
                 qreal b = qBound<qreal>(0, temp.blueF() + (cDelta/255.0), 1);
                 temp = QColor::fromRgbF(r,g,b);
                 temp = ConvertToRGB(temp, m_Mode);
                 col = temp.rgb();
             }
             else if(m_Mode == WLP_MODE_CMYK || m_Mode == WLP_MODE_RGBW)
             {
                 temp = ConvertTo(temp, m_Mode);
                 qreal c = qBound<qreal>(0, (temp.cyanF()) + (aDelta/255.0), 1);
                 qreal m = qBound<qreal>(0, (temp.magentaF()) + (bDelta/255.0), 1);
                 qreal y = qBound<qreal>(0, (temp.yellowF()) + (cDelta/255.0), 1);
                 qreal k = qBound<qreal>(0, (temp.blackF()) + (dDelta/255.0), 1);
                 temp = QColor::fromCmykF(c, m, y, k).rgb();
                 temp = ConvertToRGB(temp, m_Mode);
                 col = temp.rgb();
             }
             else //Non-rgb-constructions are hsl, hsv & cmyk
             {
                 int hDelta = 0, sDelta = 0, lDelta = 0, vDelta = 0;
                 if(m_Mode == WLP_MODE_HSL)
                 {

                     QColor hslVis = vis.toHsl();
                     hDelta = hslVis.hslHue() + aDelta;
                     sDelta = hslVis.hslSaturation() + (bDelta);
                     lDelta = hslVis.lightness() + (cDelta);
                 }
                 else if(m_Mode == WLP_MODE_HSV)
                 {
                     QColor hsvVis = vis.toHsv();
                     hDelta = hsvVis.hsvHue() + aDelta;
                     sDelta = hsvVis.hsvSaturation() + (bDelta);
                     vDelta = hsvVis.value() + (cDelta);
                 }

                 if(hDelta < 0){hDelta += 360;}
                 else if(hDelta > 360){hDelta -= 360;}

                 int h = qBound(0, hDelta, 359);
                 int s = qBound(0, sDelta, 255);
                 int l = qBound(0, lDelta, 255);
                 int v = qBound(0, vDelta, 255);

                 if(m_Mode == WLP_MODE_HSL)
                    col = QColor::fromHsl(h,s,l).rgb();
                 if(m_Mode == WLP_MODE_HSV)
                    col = QColor::fromHsl(h,s,v).rgb();
                 else{}
             }

           tmp.setPixel(x, y, col);
         }
       }

    //Update display image, scale if necessary
    m_Viewer_Ptr->m_ImgDisplay = tmp.scaled(s_ViewWidth,s_ViewHeight, Qt::KeepAspectRatio);
    m_Viewer_Ptr->Update();
    if(_updateState == WLP_PREVIEW_OFF)
    {
        *(m_Viewer_Ptr->m_Img_Ptr) = tmp;
        m_Viewer_Ptr->m_ImgScaled = tmp.scaled(s_ViewWidth,s_ViewHeight, Qt::KeepAspectRatio);
    }
}

void GeneralEditor::UpdateBright(WLPFlag _updateState)
{
    int brightness = m_ControlA_Ptr->value();
    double contrast = m_ControlB_Ptr->value();
    double conDelta = (259.0 * (contrast + 255.0)) / (255.0 * (259.0 - contrast));

    int saturation; double satContrast; double satDelta = 0;
    if(m_Mode == WLP_MODE_HSL || m_Mode == WLP_MODE_HSV)
    {
        saturation = m_ControlC_Ptr->value();
        satContrast = m_ControlD_Ptr->value();
        satDelta = (259.0 * (satContrast + 255.0)) / (255.0 * (259.0 - satContrast));
    }

    QImage tmp;
    if(_updateState == WLP_PREVIEW_ON)
        tmp = m_Viewer_Ptr->m_ImgScaled;
    else
        tmp = *(m_Viewer_Ptr->m_Img_Ptr);

    for (int x = 0; x < tmp.width(); ++x) {
        for (int y = 0; y < tmp.height(); ++y) {

             QColor vis(tmp.pixel(x,y));
             QColor temp = ConvertTo(vis, m_Mode);
             QRgb col = 0;

             if(m_Mode == WLP_MODE_RGB || m_Mode == WLP_MODE_XYZ || m_Mode == WLP_MODE_LMS || m_Mode == WLP_MODE_CMY)
             {
                 int r = qBound<int>(0, (conDelta * (vis.red() - 128)) + 128 + brightness, 255);
                 int g = qBound<int>(0, (conDelta * (vis.green() - 128)) + 128 + brightness, 255);
                 int b = qBound<int>(0, (conDelta * (vis.blue() - 128)) + 128 + brightness, 255);
                 temp = QColor::fromRgb(r,g,b);
                 temp = ConvertToRGB(temp, m_Mode);
                 col = temp.rgb();
             }
             else if(m_Mode == WLP_MODE_HSL || m_Mode == WLP_MODE_HSV || m_Mode == WLP_MODE_HSI)
             {
                 int lvi = 127;
                 if(m_Mode == WLP_MODE_HSV) lvi = vis.value();
                 if(m_Mode == WLP_MODE_HSL) lvi = vis.lightness();
                 int lDelta = qBound<int>(0, (conDelta * (lvi - 128)) + 128 + brightness, 255);
                 int sDelta = qBound<int>(0, (satDelta * (vis.hslSaturation() - 128)) + 128 + saturation, 255);
                 col = QColor::fromHsv(vis.hsvHue(),sDelta,lDelta).rgb();
             }
             else if(m_Mode == WLP_MODE_CMYK || m_Mode == WLP_MODE_RGBW)
             {
                 QColor temp = vis;
                 if(m_Mode == WLP_MODE_RGBW)
                 {
                     temp = RGBtoHSI(vis);
                     temp = HSItoRGBA(temp);
                 }

                 qreal c = qBound<qreal>(0, (conDelta * (temp.cyan() - 128)) + 128 + brightness, 255);
                 qreal m = qBound<qreal>(0, (conDelta * (temp.magenta() - 128)) + 128 + brightness, 255);
                 qreal y = qBound<qreal>(0, (conDelta * (temp.yellow() - 128)) + 128 + brightness, 255);
                 qreal k = qBound<qreal>(0, (conDelta * (temp.black() - 128)) + 128 + brightness, 255);
                 temp = QColor::fromCmyk(c, m, y, k).rgb();

                 if(m_Mode == WLP_MODE_RGBW)
                 {
                     temp = RGBAtoHSI(temp);
                     temp = HSItoRGB(temp);
                     c = qBound<qreal>(0, temp.cyanF(), 1);
                     m = qBound<qreal>(0, temp.magentaF(), 1);
                     y = qBound<qreal>(0, temp.yellowF(), 1);
                     k = qBound<qreal>(0, temp.blackF(), 1);
                     temp = QColor::fromCmykF(c, m, y, k).rgb();
                 }

                 col = temp.rgb();
             }
             else if(m_Mode == WLP_MODE_YUV || m_Mode == WLP_MODE_YIQ)
             {
                 QColor c = (m_Mode == WLP_MODE_YUV) ? RGBtoYUV(vis) : RGBtoYIQ(vis);
                 int luma = qBound<int>(0, (conDelta * (vis.red() - 128)) + 128 + brightness, 255);
                 c = QColor::fromRgb(luma, c.green(), c.blue()).rgb();
                 c = (m_Mode == WLP_MODE_YUV) ? YUVtoRGB(c) : YIQtoRGB(c);
                 col = c.rgb();
             }
             else if(m_Mode == WLP_MODE_RGC)
             {
                 QColor c = RGBtoRGC(vis);
                 int green = qBound<int>(0, (conDelta * (vis.green() - 128)) + 128 + brightness, 255);
                 c = QColor::fromRgb(c.red(), c.green(), green).rgb();
                 qreal x = c.redF();
                 qreal y = c.greenF();
                 qreal Y = c.blueF();

                 qreal X = qBound<qreal>(0, (x * Y) / y, 1);
                 qreal Z = qBound<qreal>(0, ((1 - x - y) * Y)/y, 1);

                 col = QColor::fromRgbF(X,Y,Z).rgb();
             }

           tmp.setPixel(x, y, col);
         }
       }

    //Update display image, scale if necessary
    m_Viewer_Ptr->m_ImgDisplay = tmp.scaled(s_ViewWidth,s_ViewHeight, Qt::KeepAspectRatio);
    m_Viewer_Ptr->Update();
    if(_updateState == WLP_PREVIEW_OFF)
    {
        *(m_Viewer_Ptr->m_Img_Ptr) = tmp;
        m_Viewer_Ptr->m_ImgScaled = tmp.scaled(s_ViewWidth,s_ViewHeight, Qt::KeepAspectRatio);
    }
}

void GeneralEditor::UpdateInvert(WLPFlag _updateState)
{
    bool invertA = p_InvertA->isChecked();
    bool invertB = p_InvertB->isChecked();
    bool invertC = p_InvertC->isChecked();
    bool invertD = false;

    if(m_Mode == WLP_MODE_CMYK || m_Mode == WLP_MODE_RGBW)
        invertD = p_InvertD->isChecked();

    //temp
    QColor temp;

    QImage tmp;
    if(_updateState == WLP_PREVIEW_ON)
        tmp = m_Viewer_Ptr->m_ImgScaled;
    else
        tmp = *(m_Viewer_Ptr->m_Img_Ptr);

    for (int x = 0; x < tmp.width(); ++x)
    {
        for (int y = 0; y < tmp.height(); ++y)
        {
             QColor vis(tmp.pixel(x,y));
             temp = vis;

             //YUV, LAB, YIQ, HLAB
             if(m_Mode != WLP_MODE_CMYK && m_Mode != WLP_MODE_RGBW && m_Mode != WLP_MODE_HSL && m_Mode != WLP_MODE_HSV)
             {
                 temp = ConvertTo(vis, m_Mode);
                 if(m_Mode == WLP_MODE_RGC) temp.setBlueF( vis.greenF() );
                 qreal r,g,b;
                 r = invertA ? qBound<qreal>(0, 1 - temp.redF(), 1) : temp.redF();
                 g = invertB ? qBound<qreal>(0, 1 - temp.greenF(), 1) : temp.greenF();
                 b = invertC ? qBound<qreal>(0, 1 - temp.blueF(), 1) : temp.blueF();
                 temp = QColor::fromRgbF(r,g,b);
                 temp = ConvertToRGB(temp, m_Mode);
             }
             else if(m_Mode == WLP_MODE_CMYK || m_Mode == WLP_MODE_RGBW)
             {
                 temp = ConvertTo(vis, m_Mode);
                 qreal c = invertA ? qBound<qreal>(0, 1 - temp.cyanF(), 1) : temp.cyanF();
                 qreal m = invertB ? qBound<qreal>(0, 1 - temp.magentaF(), 1) : temp.magentaF();
                 qreal y = invertC ? qBound<qreal>(0, 1 - temp.yellowF(), 1) : temp.yellowF();
                 qreal k = invertD ? qBound<qreal>(0, 1 - temp.blackF(), 1) : temp.blackF();
                 temp = QColor::fromCmykF(c,m,y,k);
                 temp = ConvertToRGB(temp, m_Mode);
             }
             else if(m_Mode == WLP_MODE_HSL)
             {
                 qreal r = invertA ? qBound<qreal>(0, 1 - vis.hslHueF(), 1) : vis.hslHueF();
                 qreal g = invertB ? qBound<qreal>(0, 1 - vis.hslSaturationF(), 1) : vis.hslSaturationF();
                 qreal b = invertC ? qBound<qreal>(0, 1 - vis.lightnessF(), 1) : vis.lightnessF();
                 temp = QColor::fromHslF(r,g,b);
             }
             else if(m_Mode == WLP_MODE_HSV)
             {
                 qreal r = invertA ? qBound<qreal>(0, 1 - vis.hsvHueF(), 1) : vis.hsvHueF();
                 qreal g = invertB ? qBound<qreal>(0, 1 - vis.hsvSaturationF(), 1) : vis.hsvSaturationF();
                 qreal b = invertC ? qBound<qreal>(0, 1 - vis.valueF(), 1) : vis.valueF();
                 temp = QColor::fromHsvF(r,g,b);
             }
             else{ qDebug() << "Something terrible has happened."; }

             tmp.setPixel(x,y, temp.rgb());
        }
    }

    //Update display image, scale if necessary
    m_Viewer_Ptr->m_ImgDisplay = tmp.scaled(s_ViewWidth,s_ViewHeight, Qt::KeepAspectRatio);
    m_Viewer_Ptr->Update();
    if(_updateState == WLP_PREVIEW_OFF)
    {
        *(m_Viewer_Ptr->m_Img_Ptr) = tmp;
        m_Viewer_Ptr->m_ImgScaled = tmp.scaled(s_ViewWidth,s_ViewHeight, Qt::KeepAspectRatio);
    }
}

void GeneralEditor::Cancel()
{
    m_Viewer_Ptr->m_ImgDisplay = m_Viewer_Ptr->m_ImgScaled;
    m_Viewer_Ptr->Update();
}
