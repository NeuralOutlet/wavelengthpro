#include "ColourMapping.h"
#include "spectralexplorer.h"
#include "wlpsettings.h"

#include <iostream>

extern WLPConfig g_Config;

SpectralExplorer::SpectralExplorer(WLPPool *_pool, QWidget *parent) :
    QWidget(parent)
{
    p_Pool = _pool;
    setWindowTitle("Multi-Channel Mixer");
    p_MasterImgs = p_Pool->GetPool(); //allImgs; //for final render
    //Add the active channels:
    std::vector<QString>::iterator iter = p_Pool->m_Texts.begin();
    int counter = 0;
    for(; iter != p_Pool->m_Texts.end(); ++iter, ++counter)
    {
        QString imgType = (*iter);
        //add to channel mixers
        for(int i = 0; i < 7; ++i)
        {
            if(iter != p_Pool->m_Texts.begin())
                chanWave[i].addItem("--------------------");
            chanWave[i].addItem("Av of " + imgType);
            chanWave[i].addItem("R  of " + imgType);
            chanWave[i].addItem("G  of " + imgType);
            chanWave[i].addItem("B  of " + imgType);
        }
    }

    iter = p_Pool->m_GenTexts.begin();
    for(; iter != p_Pool->m_GenTexts.end(); ++iter)
    {
        QString imgType = (*iter);
        //add to channel mixers
        for(int i = 0; i < 7; ++i)
        {
            if(iter != p_Pool->m_GenTexts.begin() || counter > 0)
                chanWave[i].addItem("--------------------");
            chanWave[i].addItem("Av of " + imgType);
            chanWave[i].addItem("R  of " + imgType);
            chanWave[i].addItem("G  of " + imgType);
            chanWave[i].addItem("B  of " + imgType);
        }
    }

    //Add all images:
    QImage temp;
    int activeImg = -1;
    for(int i = 0; i < WLP_POOL_MAX; ++i)
    {
        if(p_Pool->m_IsLoaded[i])
        {
            m_ImgScaled[i] = p_MasterImgs[i].scaled(s_ViewWidth,s_ViewHeight,Qt::KeepAspectRatio);
            activeImg = i;
        }
        else
        {
            m_ImgScaled[i] = temp;
        }
    }

    // Account for only general images
    if(activeImg != -1)
        mixedImg = p_MasterImgs[activeImg].scaled(s_ViewWidth,s_ViewHeight,Qt::KeepAspectRatio);
    else
        mixedImg = p_Pool->GetImgGeneral(0)->scaled(s_ViewWidth,s_ViewHeight,Qt::KeepAspectRatio);

    // Set the preview to be one of the image 3to3 RGB Map
    imgWidth = mixedImg.width();
    imgHeight = mixedImg.height();
    for(int i = 0; i < 3; ++i)
    {
        chanWave[i].setCurrentIndex(i+1);
        p_GreyWeight[i] = new QSpinBox;
        p_GreyWeight[i]->setMaximum(300);
        p_GreyWeight[i]->setValue(33);
        connect(p_GreyWeight[i], SIGNAL(valueChanged(int)), this, SLOT(UpdateGrey(int)));
    }

    // Pseudo colour:
    p_Equation = new QLabel;
    pseudoView = new QLabel;
    tabColMap = new QTabWidget;
    isJet = false;
    mixerLoaded = false;
    pMaps = new QComboBox;
    pMaps->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    pMaps->addItem("None");
    pMaps->addItem("greyscale");
    pMaps->addItem("Bone");
    pMaps->addItem("Sepia");
    pMaps->addItem("Jet (blue to red)");
    pMaps->addItem("Jet (red to blue)");

    pGreys = new QComboBox;
    pGreys->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    pGreys->addItem("RGB Grey  (colourimetric)");  //0.2126R + 0.7152G + 0.0722B
    pGreys->addItem("RGB Grey  (average)");        //0.3333R + 0.3333G + 0.3333B
    pGreys->addItem("RGB Grey  (luma)");           //0.2990R + 0.5870G + 0.1140B
    pGreys->addItem("YUV Grey  (luma)");
    pGreys->addItem("XYZ Grey  (average)");
    pGreys->addItem("HSL Grey  (lightness)");
    pGreys->addItem("HSV Grey  (value)");
    pGreys->addItem("Lab Grey  (lightness)");
    pGreys->addItem("UVW Grey  (lightness)");
    pGreys->addItem("CMYK Grey (Blackscale)");
    pGreys->addItem("Custom...");
    connect(pGreys, SIGNAL(currentTextChanged(QString)), this, SLOT(CustomGrey(QString)));

    maxChannels = new QSpinBox;
    maxChannels->setRange(1,6);
    maxChannels->setValue(6);
    mixerLoaded = false;

    QObject::connect(pMaps, SIGNAL(currentIndexChanged(int)), this, SLOT(ModifyPseudo()) );
    QObject::connect(pGreys, SIGNAL(currentIndexChanged(int)), this, SLOT(ModifyPseudo()) );
    QObject::connect(maxChannels, SIGNAL(valueChanged(int)), this, SLOT(UpdateTabs()) );
    QObject::connect(maxChannels, SIGNAL(valueChanged(int)), this, SLOT(ModifyFalse()) );

    for(int i = 0; i < maxChannels->value(); ++i)
        QObject::connect(&chanWave[i], SIGNAL(currentIndexChanged(int)), this, SLOT(ModifyFalse()));

    UpdateTabs();
    ChannelMix();
}

void SpectralExplorer::CustomGrey(QString str)
{
    if(str != "Custom...")
        return;

    //Edit widget
    QWidget *temp = new QWidget(0);
    QPushButton *norm = new QPushButton("Normalise");
    connect(norm, SIGNAL(clicked()), this, SLOT(NormGrey()));
    QPushButton *save = new QPushButton("Use Custom Grey");
    QPushButton *cancel = new QPushButton("Cancel");
    connect(save, SIGNAL(clicked()), temp, SLOT(close()));
    connect(cancel, SIGNAL(clicked()), temp, SLOT(close()));

    QGridLayout *lay = new QGridLayout;
    lay->addWidget(new QLabel("% of R"),0,0);
    lay->addWidget(p_GreyWeight[0], 1,0);
    lay->addWidget(new QLabel("% of G"),0,1);
    lay->addWidget(p_GreyWeight[1], 1,1);
    lay->addWidget(new QLabel("% of B"),0,2);
    lay->addWidget(p_GreyWeight[2], 1,2);

    qreal r = (qreal)p_GreyWeight[0]->value() / 100.0;
    qreal g = (qreal)p_GreyWeight[1]->value() / 100.0;
    qreal b = (qreal)p_GreyWeight[2]->value() / 100.0;
    p_Equation->setText("R" + QString::number(r) + " + G" + QString::number(g) + " + B" + QString::number(b)
            + " = " + QString::number((r+g+b)*100) + "%");

    lay->addWidget(p_Equation,2,0,1,3,Qt::AlignCenter);
    lay->addWidget(save, 3,0);
    lay->addWidget(norm,3,1);
    lay->addWidget(cancel,3,2);
    temp->setLayout(lay);
    temp->show();
}

void SpectralExplorer::UpdateGrey(int norm)
{
    qreal r,g,b;
    if(norm == -1) //if we are normalising the values
    {
        qreal R = (qreal)p_GreyWeight[0]->value();
        qreal G = (qreal)p_GreyWeight[1]->value();
        qreal B = (qreal)p_GreyWeight[2]->value();
        qreal scale = 100.0 / (R+G+B);

        r = (R * scale);
        g = (G * scale);
        b = (B * scale);
        p_GreyWeight[0]->setValue(r);
        p_GreyWeight[1]->setValue(g);
        p_GreyWeight[2]->setValue(b);
        r /= 100.0;
        g /= 100.0;
        b /= 100.0;
    }
    else
    {
        r = (qreal)p_GreyWeight[0]->value() / 100.0;
        g = (qreal)p_GreyWeight[1]->value() / 100.0;
        b = (qreal)p_GreyWeight[2]->value() / 100.0;
    }

    QString num;
    QString text = "R";
    num.sprintf("%.2f", static_cast<double>(r));
    text += num + " + G";
    num.sprintf("%.2f", static_cast<double>(g));
    text += num + " + B";
    num.sprintf("%.2f", static_cast<double>(b));
    text += num + " = ";
    num.sprintf("%.2f", static_cast<double>((r+g+b)*100));
    text += num + "%";

    p_Equation->setText(text);
    ModifyPseudo();
}

void SpectralExplorer::UpdateTabs()
{
    for(int i = 0; i <= 5; ++i)
        if(i < maxChannels->value())
            chanWave[i].setDisabled(false);
        else
            chanWave[i].setDisabled(true);

    if(maxChannels->value() == 1)
        chanTemplate.setText("Red:\ta \nGreen:\ta \nBlue:\ta");
    else if(maxChannels->value() == 2)
        chanTemplate.setText("Red:\ta \nGreen:\t(a+b)/2 \nBlue:\tb");
    else if(maxChannels->value() == 3)
        chanTemplate.setText("Red:\ta \nGreen:\tb \nBlue:\tc");
    else if(maxChannels->value() == 4)
        chanTemplate.setText("Red:\t(a+(b*0.33))*0.75 \nGreen:\t((b*0.66)+(c*0.66))*0.75 \nBlue:\t((c*0.33)+d)*0.75");
    else if(maxChannels->value() == 5)
        chanTemplate.setText("Red:\t(a+(b*0.66))*0.60 \nGreen:\t((b*0.33)+c+(d*0.33))*0.60 \nBlue:\t((d*0.66)+e)*0.60");
    else{ chanTemplate.setText("Red:\t(a+b+c)/3\nGreen:\t(d+e+f)/3\nBlue:\t("); }
}

void SpectralExplorer::ChannelMix()
{
    if(mixerLoaded)
    {
        //mixerWidget->
        show();
        return;
    }

    //View Screen
    pseudoView->setPixmap( QPixmap::fromImage( mixedImg ));
    //pseudoView->setAlignment(Qt::AlignCenter);

    //False Colour tab
    QWidget *tabFalse = new QWidget;
    QGridLayout *layFalse = new QGridLayout;
    QLabel *text = new QLabel;
    text->setWordWrap(true);
    text->setText("False colour is the mapping of colours to wavelengths.");
    text->setAlignment(Qt::AlignTop);
    layFalse->addWidget(text,0,0,1,2);
    text = new QLabel; text->setText("Active channels");
    layFalse->addWidget(text, 2,0);
    QLabel *cTemp = &chanTemplate;
    layFalse->addWidget(cTemp, 1,0,1,2);
    layFalse->addWidget(maxChannels,2,1);

    std::string letter = "";
    for(int i = 0; i < maxChannels->value(); ++i)
    {
        letter = ""; letter += (char)(49+i);
        (49+i==49)?letter+= "st " : (49+i==50)?letter+= "nd " : letter+= "th ";
        letter += "channel (";
        letter += (char)(97+i); letter += ")";
        layFalse->addWidget(new QLabel(QString(letter.c_str())),3+i,0);
        layFalse->addWidget(&chanWave[i],3+i,1);
    }
    maxChannels->setValue(3);

    tabFalse->setLayout(layFalse);
    tabColMap->addTab(tabFalse, "False-colour");


    //Psuedo Colour
    QWidget *tabPseudo = new QWidget;
    QGridLayout *layPseudo = new QGridLayout;
    QLabel *text2 = new QLabel;
    text2->setWordWrap(true);
    text2->setText("Pseudo colours are mapped to the intensity of the pixels (0-255). An example of this is thermal imaging.");
    text2->setAlignment(Qt::AlignTop);
    //text2->setFixedWidth(100);
    layPseudo->addWidget(text2,0,0, 1,2);
    QLabel *text3 = new QLabel;
    text3->setText("Select intensity style: ");
    layPseudo->addWidget(text3,1,0);
    layPseudo->addWidget(pGreys,1,1);
    text3 = new QLabel;
    text3->setText("Select colour-map: ");
    layPseudo->addWidget(text3,2,0);
    layPseudo->addWidget(pMaps, 2,1);
    tabPseudo->setLayout(layPseudo);
    tabColMap->addTab(tabPseudo, "Pseudo-colour");

    QPushButton *saveImg = new QPushButton(tr("Save As"));
    connect(saveImg, SIGNAL(clicked()), this, SLOT(Render()));

    QPushButton *dropImg = new QPushButton(tr("Drop to Pool"));
    connect(dropImg, SIGNAL(clicked()), this, SLOT(Drop()));

    //Setting it all up:
    QGridLayout *gridLay = new QGridLayout;
    gridLay->addWidget(pseudoView,0,0);
    gridLay->addWidget(tabColMap,0,1);
    gridLay->addWidget(saveImg, 1,0,Qt::AlignCenter);
    gridLay->addWidget(dropImg, 1,1,Qt::AlignCenter);

    setMaximumHeight(700);
    setLayout(gridLay);

    mixerLoaded = true;
}


float SpectralExplorer::lerp(float a, float b, float f)
{
    f /= 255;
    float out = a + f * (b - a);
    if (out < a) out = a;
    if (out > b) out = b;

    return out;
}

QImage SpectralExplorer::GetChannel(QString chan, WLPFlag p_PreviewState = WLP_PREVIEW_ON)
{
    QImage temp;// = QImage(imgWidth, imgHeight, mixedImg.format());

    //Split the text to type and channel (prefix)
    char prefix = chan[0].toLatin1();

    //If it is a non-image, return grey
    if(prefix == '-')
    {
        temp = QImage(imgWidth, imgHeight, mixedImg.format());
        QRgb col = QColor::fromRgb( 127,127,127 ).rgb();
        for (int x = 0; x < imgWidth; ++x) {
            for (int y = 0; y < imgHeight; ++y)
            {
                temp.setPixel(x, y, col);
            }
        }
        return temp;
    }

    QString type;
    for(int i = 6; i < chan.length(); ++i)
        type += chan[i];

    int index = IndexFromString(type);
    if(index == -1)
    {
        int genIndex = 0;
        std::vector<QString>::iterator iter = p_Pool->m_GenTexts.begin();
        for(; iter != p_Pool->m_GenTexts.end() && type != p_Pool->m_GenTexts[genIndex]; ++iter)
            ++genIndex;

        if(p_PreviewState==WLP_PREVIEW_ON)
            temp = p_Pool->GetImgGeneral(genIndex)->scaled(s_ViewWidth, s_ViewHeight, Qt::KeepAspectRatio);
        else
            temp = *p_Pool->GetImgGeneral(genIndex);
    }
    else
    {
        //Set the image to the type it is:
        if(p_PreviewState==WLP_PREVIEW_ON)
            temp = m_ImgScaled[index];
        else
            temp = p_MasterImgs[index];
    }

    //Select the correct channels
    QImage output = temp;
    for (int x = 0; x < temp.width(); ++x)
    {
        for (int y = 0; y < temp.height(); ++y)
        {
            QColor tempA(temp.pixel(x,y));
            QRgb col;
            if(prefix == 'B')
                col = QColor::fromRgb( tempA.blue(), tempA.blue(), tempA.blue() ).rgb();
            else if(prefix == 'G')
                col = QColor::fromRgb( tempA.green(), tempA.green(), tempA.green() ).rgb();
            else if(prefix == 'R')
                col = QColor::fromRgb( tempA.red(), tempA.red(), tempA.red() ).rgb();
            else{
                return temp; //prefix is Av
            }

          output.setPixel(x, y, col);
        }
    }

    imgWidth = output.width();
    imgHeight = output.height();
    mixedImg = QImage(imgWidth, imgHeight, mixedImg.format());

    return output;
}

void SpectralExplorer::ModifyFalse(WLPFlag p_PreviewState)
{
    int MAXCHAN = maxChannels->value();
    QImage chanImg[7];
    for(int i = 0; i <= MAXCHAN; ++i)
        chanImg[i] = GetChannel(chanWave[i].currentText(), p_PreviewState);

    for (int x = 0; x < imgWidth; ++x) {
        for (int y = 0; y < imgHeight; ++y)
        {
            QRgb col;
            switch(MAXCHAN)
            {
            case 1:
            {
                QColor A = chanImg[0].pixel(x,y);
                float a = (A.red() + A.green() + A.blue())/3;

                col = QColor::fromRgb( a,a,a ).rgb();
                break;
            }
            case 2:
            {
                QColor A = chanImg[0].pixel(x,y);
                QColor B = chanImg[1].pixel(x,y);
                float a = (A.red() + A.green() + A.blue())/3;
                float b = (B.red() + B.green() + B.blue())/3;
                col = QColor::fromRgb( a,(a+b)/2, b ).rgb();
                break;
            }
            case 3:
            {
                QColor A = chanImg[0].pixel(x,y);
                QColor B = chanImg[1].pixel(x,y);
                QColor C = chanImg[2].pixel(x,y);
                float a = (A.red() + A.green() + A.blue())/3;
                float b = (B.red() + B.green() + B.blue())/3;
                float c = (C.red() + C.green() + C.blue())/3;
                col = QColor::fromRgb( a,b,c ).rgb();
                break;
            }
            case 4:
            {
                QColor A = chanImg[0].pixel(x,y);
                QColor B = chanImg[1].pixel(x,y);
                QColor C = chanImg[2].pixel(x,y);
                QColor D = chanImg[3].pixel(x,y);
                float a = (A.red() + A.green() + A.blue())/3;
                float b = (B.red() + B.green() + B.blue())/3;
                float c = (C.red() + C.green() + C.blue())/3;
                float d = (D.red() + D.green() + D.blue())/3;
                //col = QColor::fromRgb( a,(b+c)/2, d ).rgb();

                //Equal distribution:
                float aSpread = (a + (b * 0.33)) * 0.75;
                float bSpread = ((b * 0.66) + (c * 0.66)) * 0.75;
                float cSpread = ((c * 0.33) + d) * 0.75;
                col = QColor::fromRgb(aSpread, bSpread, cSpread ).rgb();
                break;
            }
            case 5:
            {
                QColor A = chanImg[0].pixel(x,y);
                QColor B = chanImg[1].pixel(x,y);
                QColor C = chanImg[2].pixel(x,y);
                QColor D = chanImg[3].pixel(x,y);
                QColor E = chanImg[4].pixel(x,y);
                float a = (A.red() + A.green() + A.blue())/3;
                float b = (B.red() + B.green() + B.blue())/3;
                float c = (C.red() + C.green() + C.blue())/3;
                float d = (D.red() + D.green() + D.blue())/3;
                float e = (E.red() + E.green() + E.blue())/3;

                //Equal distribution:
                float aSpread = (a + (b*0.66)) * 0.60;
                float bSpread = ((b*0.33) + c + (d*0.33)) * 0.60;
                float cSpread = ((d*0.66) + e) * 0.60;
                col = QColor::fromRgb(aSpread, bSpread, cSpread ).rgb();

                break;
            }
            case 6:
            {
                QColor A = chanImg[0].pixel(x,y);
                QColor B = chanImg[1].pixel(x,y);
                QColor C = chanImg[2].pixel(x,y);
                QColor D = chanImg[3].pixel(x,y);
                QColor E = chanImg[4].pixel(x,y);
                QColor F = chanImg[5].pixel(x,y);
                float a = (A.red() + B.red() + A.blue())/3;
                float b = (B.red() + B.green() + B.blue())/3;
                float c = (C.red() + C.green() + C.blue())/3;
                float d = (D.red() + D.green() + D.blue())/3;
                float e = (E.red() + E.green() + E.blue())/3;
                float ef = (F.red() + F.green() + F.blue())/3;

                //Equal distribution:
                float aSpread = (a + b)/2;
                float bSpread = (c + d)/2;
                float cSpread = (e + ef)/2;
                col = QColor::fromRgb(aSpread, bSpread, cSpread ).rgb();

                break;
            }
          }
          mixedImg.setPixel(x, y, col);
        }
      }

    ModifyPseudo();
}

void SpectralExplorer::ModifyPseudo()
{
    if(pMaps->currentIndex() == 0) //none
    {
        pseudoView->setPixmap(QPixmap::fromImage(mixedImg));
        pseudoImg = mixedImg;
        return;
    }

    QRgb col;
    QImage temp = mixedImg;

    for (int x = 0; x < imgWidth; ++x) {
        for (int y = 0; y < imgHeight; ++y)
        {
            QColor fromCol = mixedImg.pixel(x,y);

            //Set up intensity:
            float grey = 144;
            grey = GetGrey(fromCol);
            //Set up colour map:
            if(pMaps->currentIndex() == 1)
            {
                col = QColor::fromRgb(grey,grey,grey).rgb();
            }
            else if(pMaps->currentIndex() == 2)
            {
                int R = lerp(38,244,grey);
                int G = lerp(48,248,grey);
                int B = lerp(51,248,grey);

                col = QColor::fromRgb(R,G,B).rgb();
            }
            else if(pMaps->currentIndex() == 3)
            {
                int R = lerp(112,255,grey);
                int G = lerp(66,250,grey);
                int B = lerp(20,224,grey);

                col = QColor::fromRgb(R,G,B).rgb();
            }
            else if(pMaps->currentIndex() == 4)
            {
                COLOUR jet = GetColour(grey,0,255);
                col = QColor::fromRgb(jet.b, jet.g,jet.r).rgb();
            }
            else if(pMaps->currentIndex() == 5)
            {
                COLOUR jet = GetColour(grey,0,255);
                col = QColor::fromRgb(jet.r, jet.g,jet.b).rgb();
            }

            else{ /* Sepia (112, 66, 20) */}

          temp.setPixel(x, y, col);
        }
      }
    pseudoImg = temp;
    pseudoView->setPixmap(QPixmap::fromImage(pseudoImg));
}

qreal SpectralExplorer::GetGrey(QColor _pixel)
{
    qreal grey;
    if(pGreys->currentIndex() == 0){ grey = (0.2126*_pixel.red())+ (0.7152*_pixel.green()) + (0.0722*_pixel.blue()); }
    else if(pGreys->currentIndex() == 1){ grey = (0.3333*_pixel.red())+ (0.3333*_pixel.green()) + (0.3333*_pixel.blue()); }
    else if(pGreys->currentIndex() == 2){ grey = (0.2990*_pixel.red())+ (0.5870*_pixel.green()) + (0.1140*_pixel.blue()); }
    else if(pGreys->currentIndex() == 3)
    {
        QColor yuvPixel = RGBtoYUV(_pixel);
        qreal temp = yuvPixel.redF();
        grey = qBound<qreal>(0, temp*255.0, 255);
    }
    else if(pGreys->currentIndex() == 4)
    {
        QColor xyzPixel = RGBtoXYZ(_pixel);
        qreal temp = (0.3333*xyzPixel.redF())+ (0.3333*xyzPixel.greenF()) + (0.3333*xyzPixel.blueF());
        grey = qBound<qreal>(0, temp*255.0, 255);
    }
    else if(pGreys->currentIndex() == 5)
    {
        qreal temp = _pixel.lightnessF();
        grey = qBound<qreal>(0, temp*255.0, 255);
    }
    else if(pGreys->currentIndex() == 6)
    {
        qreal temp = _pixel.valueF();
        grey = qBound<qreal>(0, temp*255.0, 255);
    }
    else if(pGreys->currentIndex() == 7)
    {
        QColor labPixel = RGBtoXYZ(_pixel);
        labPixel = XYZtoLab(labPixel);
        qreal temp = labPixel.redF();
        grey = qBound<qreal>(0, temp*255.0, 255);
    }
    else if(pGreys->currentIndex() == 8)
    {
        QColor uvwPixel = RGBtoXYZ(_pixel);
        uvwPixel = XYZtoUVW(uvwPixel);
        qreal temp = uvwPixel.blueF();
        grey = qBound<qreal>(0, temp*255.0, 255);
    }
    else if(pGreys->currentIndex() == 9)
    {
        QColor cmykPixel = _pixel.toCmyk();
        qreal temp = cmykPixel.blackF();
        grey = qBound<qreal>(0, temp*255.0, 255);
    }
    else if(pGreys->currentIndex() == 10)
    {
        qreal R = _pixel.red() * ((qreal)p_GreyWeight[0]->value()/100.0);
        qreal G = _pixel.green() * ((qreal)p_GreyWeight[1]->value()/100.0);
        qreal B = _pixel.blue() * ((qreal)p_GreyWeight[2]->value()/100.0);
        grey = qBound<qreal>(0, R+G+B, 255);
    }
    else{ grey = 144; }

    return grey;
}

void SpectralExplorer::Render()
{
    ModifyFalse(WLP_PREVIEW_OFF);

    //Open Save As dialog
    QString newName = QFileDialog::getSaveFileName(this, tr("Save image"),
                                                   g_Config.Get(WLPC_DIR_SAVE), tr("JPG (*.jpg) ;; PNG (*.png) ;; TIF (*.tif)") );
    //Save to new path/name
    pseudoImg.save(newName,NULL,100);

    //Set that path as new save path
    QFileInfo bloo(newName);
    WLPSettings s; s.UpdateSaveDir(bloo.absolutePath());
}

void SpectralExplorer::Drop()
{
    ModifyFalse(WLP_PREVIEW_OFF);
    p_MasterImgs[WLP_POOL_MIX_CHAN] = pseudoImg;
    p_Pool->Drop("Multi-Channel");
}
