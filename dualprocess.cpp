#include "dualprocess.h"
#include <QPushButton>
#include <QGridLayout>
#include <QMessageBox>
#include <QGroupBox>
#include <QSpinBox>
#include <QSlider>

#include <QFileDialog>
#include <QDir>

#include "wlpsettings.h"

#include <iostream>

extern WLPConfig g_Config;

DualProcess::DualProcess(WLPPool *_pool, QWidget *p_Parent_Ptr) :
    QWidget(p_Parent_Ptr)
{
    setWindowTitle("Dual Processor");

    p_Pool = _pool;

    //etc
    m_MainLabel_Ptr = new QLabel;
    m_ImgUV_Ptr = p_Pool->GetImg("UV (350-400nm)"); //p_ImgUV_Ptr;
    m_LargeImg = *m_ImgUV_Ptr;
    m_SmallImg = m_ImgUV_Ptr->scaled(s_ViewWidth, s_ViewHeight ,Qt::KeepAspectRatio);
    m_MainLabel_Ptr->setPixmap(QPixmap::fromImage(m_SmallImg));


    m_Balance_Ptr = new QSlider(Qt::Horizontal);
    m_Balance_Ptr->setRange(0,300);
    m_Balance_Ptr->setFixedWidth(300);
    connect(m_Balance_Ptr,SIGNAL(valueChanged(int)),this,SLOT(Update()));

    QSpinBox *proSpin = new QSpinBox;
    proSpin->setRange(0,300);
    connect(proSpin, SIGNAL(valueChanged(int)),this, SLOT(Update()));

    connect(m_Balance_Ptr, SIGNAL(valueChanged(int)), proSpin,SLOT(setValue(int)));
    connect(proSpin, SIGNAL(valueChanged(int)), m_Balance_Ptr,SLOT(setValue(int)));

    m_GreenInt_Ptr = new QSlider(Qt::Horizontal); m_GreenInt_Ptr->setRange(0, 300);
    QSpinBox *gIspin = new QSpinBox; gIspin->setRange(0,300);
    m_GreenDom_Ptr = new QSlider(Qt::Horizontal); m_GreenDom_Ptr->setRange(0, 100);
    QSpinBox *gDspin = new QSpinBox; gDspin->setRange(0,100);

    connect(m_GreenDom_Ptr, SIGNAL(valueChanged(int)), gDspin,SLOT(setValue(int)));
    connect(gDspin, SIGNAL(valueChanged(int)), m_GreenDom_Ptr,SLOT(setValue(int)));

    connect(m_GreenInt_Ptr, SIGNAL(valueChanged(int)), gIspin,SLOT(setValue(int)));
    connect(gIspin, SIGNAL(valueChanged(int)), m_GreenInt_Ptr,SLOT(setValue(int)));

    connect(m_GreenDom_Ptr, SIGNAL(valueChanged(int)),this, SLOT(Update()));
    connect(m_GreenInt_Ptr, SIGNAL(valueChanged(int)),this, SLOT(Update()));

    QPushButton *renderButton = new QPushButton;
    renderButton->setText("Save Image");
    connect(renderButton, SIGNAL(clicked()),this,SLOT(Render()));

    QPushButton *dropButton = new QPushButton;
    dropButton->setText("Drop to Pool");
    connect(dropButton, SIGNAL(clicked()),this,SLOT(Drop()));

    RGBbar *visualBar = new RGBbar(this);

    QGroupBox *rgbBox = new QGroupBox("RGB Red/Blue Balance");
    QGridLayout *balanceLay = new QGridLayout;
    balanceLay->addWidget(new QLabel("Mapping:"),0,0);
    balanceLay->addWidget(visualBar, 0,1);
    balanceLay->addWidget(new QLabel("Balance:"),1,0);
    balanceLay->addWidget(m_Balance_Ptr,1,1);
    balanceLay->addWidget(proSpin,1,2);
    rgbBox->setLayout(balanceLay);

    QGroupBox *greenBox = new QGroupBox("Re-introduce Green Channel");
    QGridLayout *greenLay = new QGridLayout;
    greenLay->addWidget(new QLabel("Add Green:"),0,0);
    greenLay->addWidget(m_GreenDom_Ptr,0,1);
    greenLay->addWidget(gDspin, 0,2);
    greenLay->addWidget(new QLabel("Intensity:"),1,0);
    greenLay->addWidget(m_GreenInt_Ptr,1,1);
    greenLay->addWidget(gIspin, 1,2);
    greenBox->setLayout(greenLay);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(m_MainLabel_Ptr,0,0,3,1);
    layout->addWidget(rgbBox,0,1, 1,3);
    layout->addWidget(greenBox,1,1 ,1,3);
    layout->addWidget(renderButton,2,1);
    layout->addWidget(dropButton, 2,3);
    setLayout(layout);
}

RGBbar::RGBbar(DualProcess *p_Control_Ptr, QWidget *p_Parent_Ptr) :
    QWidget(p_Parent_Ptr)
{
    m_Control_Ptr = p_Control_Ptr;
}

void RGBbar::paintEvent(QPaintEvent *)
{
    float magic = m_Control_Ptr->GetBalance();

    int Rred = (magic)<=100?(magic)*2.55 : 255;
    int Rblue = (magic)<=100?(100-magic)*2.55 : 0;

    int Bred = (magic)>=200?(magic-200)*2.55 : 0;
    int Bblue = (magic)>=200?(300-magic)*2.55 : 255;


    Rred = qBound(0,Rred,255);
    Rblue = qBound(0,Rblue,255);
    Bblue = qBound(0,Bblue,255);
    Bred = qBound(0,Bred,255);

    int Gred = 0; int Gblue = 0;
    if(magic<=100)
    {
        Gred = 0;
        Gblue = 255;
    }
    else if(magic>100 && magic<=200)
    {
        Gred = (magic - 100) * 2.5;
        Gblue = (100 - (magic - 100)) * 2.5;
    }
    else if(magic>200)
    {
        Gred = 255;
        Gblue = 0;
    }
    float gFrac = (m_Control_Ptr->GetGreenFrac()/100);
    float gInt = 255 * (m_Control_Ptr->GetGreenInt()/100);
    gInt = qBound(0,(int)gInt,255);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    painter.setPen(Qt::black);
    painter.setBrush(QColor( 255,0,0 ));
    painter.drawRect(1, 0,100,2);
    painter.setBrush(QColor( Rred, 0, Rblue ));
    painter.drawRect(1, 2,100,20);
    painter.setBrush(QColor( 255, 0, 0 ));
    painter.drawRect(1, 22,100,2);

    painter.setPen(Qt::black);
    painter.setBrush(QColor( 0,255,0 ));
    painter.drawRect(101, 0,100,2);
    painter.setBrush(QColor( Gred * (1-gFrac), gInt*gFrac, Gblue * (1-gFrac) ));
    painter.drawRect(101, 2, 100, 20);
    painter.setBrush(QColor( 0,255,0 ));
    painter.drawRect(101, 22,100,2);

    painter.setPen(Qt::black);
    painter.setBrush(QColor( 0,0,255 ));
    painter.drawRect(201, 0,100,2);
    painter.setBrush(QColor( Bred, 0, Bblue ));
    painter.drawRect(201, 2, 100, 20);
    painter.setBrush(QColor( 0,0,255 ));
    painter.drawRect(201, 22,100,2);
}

void DualProcess::Update(WLPFlag _previewState)
{
    update(); //For OpenGL draw updating
    m_Balance = m_Balance_Ptr->value();

    QImage img;
    if(_previewState == WLP_PREVIEW_ON)
        img = m_SmallImg;
    else
        img = m_LargeImg;

    for (int x = 0; x < img.width(); ++x)
    {
         for (int y = 0; y < img.height(); ++y)
         {
             float r,g,b;
             QColor twoChan(img.pixel(x,y));
             int bias = m_Balance;
             if(m_Balance > 200)
             {
                 bias -= 200;
                 r = g = twoChan.red();
                 b = (twoChan.red() * (bias/100.0)) + (twoChan.blue() * (1.0 - (bias/100.0)));
             }
             else if(m_Balance > 100)
             {
                 bias -= 100;
                 r = twoChan.red();
                 g = (twoChan.red() * (bias/100.0)) + (twoChan.blue() * (1.0 - (bias/100.0)));
                 b = twoChan.blue();
             }
             else
             {
                 r = (twoChan.red() * (bias/100.0)) + (twoChan.blue() * (1.0 - (bias/100.0)));
                 g = b = twoChan.blue();
             }

             float percentGreen = m_GreenDom_Ptr->value()/100.0;
             //Mix new green channel (from red and blue) with original green channel
             g = (g * (1-percentGreen)) + ((twoChan.green() * (m_GreenInt_Ptr->value()/30)) * percentGreen);

             //Keep inside colour max
             g = qBound(0,(int)g,255);

             QRgb col = QColor::fromRgb( r,g,b ).rgb();

             img.setPixel(x, y, col);
         }
    }

    m_MainLabel_Ptr->setPixmap(QPixmap::fromImage(img));

    if(_previewState == WLP_PREVIEW_OFF)
        m_LargeImg = img;
}

void DualProcess::Render()
{
    Update(WLP_PREVIEW_OFF);

    //Open Save As dialog
    QString newName = QFileDialog::getSaveFileName(this, tr("Save image"),
                                                   g_Config.Get(WLPC_DIR_SAVE), tr("JPG (*.jpg) ;; PNG (*.png) ;; TIF (*.tif)") );
    //Save to new path/name
    m_LargeImg.save(newName,NULL,100);

    //Set that path as new save path
    QFileInfo bloo(newName);
    WLPSettings s; s.UpdateSaveDir(bloo.absolutePath());
}

void DualProcess::Drop()
{
    Update(WLP_PREVIEW_OFF);
    p_Pool->GetPool()[WLP_POOL_DUALP] = m_LargeImg;
    p_Pool->Drop("Dual Processed");
}
