#ifndef LOADINGTHREAD_H
#define LOADINGTHREAD_H

#include "mainwindow.h"
#include "wlpDefs.h"
#include <iostream>
#include <QThread>
#include <QLabel>

class LoadingThread : public QThread, public WLPBase
{
    Q_OBJECT

public:
    LoadingThread(MainWindow *_wnd);

public slots:

signals:
    void LoadText(QString);
    void LoadUpdate(int);
    void LoadComplete();

private:
    void GetLUTs(int _a, int _b, int _c, QColor &_toSpace, QColor &_fromSpace, QString colSpace);
    void run();
};

#endif // LOADINGTHREAD_H
