#ifndef LUMINANCEMAPPER_H
#define LUMINANCEMAPPER_H

#include <QGenericMatrix>
#include <QProgressBar>
#include <QMatrix4x4>
#include <QMatrix>
#include <QMatrix3x3>
#include <QtCore/qmath.h>
#include <QVector3D>
#include <QVector>
#include <QGroupBox>
#include <QComboBox>
#include <QGridLayout>
#include <QComboBox>
#include <QWidget>
#include <QLabel>

#include "wlpTypes.h"

struct CustomSet{
    int base;
    int A,B,C;
    CustomSet(int _base, int _A, int _B, int _C){
        base=_base; A=_A; B=_B; C=_C;
    }
};

class LuminanceMapper : public QWidget, private WLPBase
{
    Q_OBJECT
public:
    explicit LuminanceMapper(WLPPool *_pool, QWidget *p_Parent_Ptr = 0);
    
    QImage GetChannel(QString p_Channel, WLPFlag p_PreviewState);

signals:
    void LoadUpdate(int);
    void LoadComplete();
    void LoadStart();
    
public slots:
    //Loadbar
    void BarStart() { p_LoadBar->setVisible(true); }
    void BarUpdate(int _progress){ p_LoadBar->setValue(_progress); }
    void BarComplete(){ p_LoadBar->setVisible(false); }

    //Colour-Space Settings
    void EditSettings();
    void SaveSettings();

    //Presets
    void AddPreset();
    void CreatePreset();
    void SetPreset(int);
    void SetDropMax(int count);

    //The ominous update method
    void Update(WLPFlag p_UpdateState, WLPFlag p_PreviewState);

    //Various ways of invoking update
    void UpdateElementA(QString p_Str) { Q_UNUSED(p_Str); Update(WLP_UPDATE_A, WLP_PREVIEW_ON); }
    void UpdateElementB(QString p_Str) { Q_UNUSED(p_Str); Update(WLP_UPDATE_B, WLP_PREVIEW_ON); }
    void UpdateElementC(QString p_Str) { Q_UNUSED(p_Str); Update(WLP_UPDATE_C, WLP_PREVIEW_ON); }
    void UpdateForPreset(QString _str) { Q_UNUSED(_str); Update(WLP_UPDATE_PRESET, WLP_PREVIEW_ON); }
    void UpdateBase(QString p_Str);
    void Render();
    void Drop();

private:
    //Conversion Functions
    qreal ConvertPixel(QColor p_Col, QString p_Mode);
    qreal GetElement(QColor p_Col, QString p_Space);
    QColor GetRGBFromBase(QColor p_Col, QString p_Base); //Final step

    //RGB Working-Space Info
    QComboBox *p_RgbSpace; //sRGB
    QComboBox *p_RgbWhite; //D65
    QComboBox *p_RgbGamma; //sRGB

    //Preset Info
    QComboBox *p_Presets;
    bool m_ActivePreset;
    std::vector<CustomSet> m_CustomSets;
    QLineEdit *p_PreName;
    QComboBox *p_PreBase;
    QComboBox *p_PreChanA;
    QComboBox *p_PreChanB;
    QComboBox *p_PreChanC;

    //Image stores
    WLPPool *p_Pool;
    QImage *m_MasterImgs_Ptr;
    QImage m_ImgScaled[WLP_POOL_MAX];

    //Output image
    QImage m_MixedImg;
    QImage m_SaveImg;
    int m_Width;
    int m_Height;

    //Widget
    QGridLayout *m_Layout_Ptr;
    QProgressBar *p_LoadBar;
    QComboBox *m_Base_Ptr;
    QGroupBox *m_InBox_Ptr;
    QLabel *m_LabelA_Ptr;
    QLabel *m_LabelB_Ptr;
    QLabel *m_LabelC_Ptr;
    int m_DropDownMax;

    QComboBox *m_BoxSpaceA_Ptr; //R / H / H / L / X / ...
    QComboBox *m_BoxSpaceB_Ptr; //G / S / S / M / Y / ...
    QComboBox *m_BoxSpaceC_Ptr; //B / L / V / S / Z / ...
    QComboBox *m_BoxImgA_Ptr;
    QComboBox *m_BoxImgB_Ptr;
    QComboBox *m_BoxImgC_Ptr;

    QLabel *m_MainLabel_Ptr;   
};




#endif // LUMINANCEMAPPER_H
