#ifndef POOLEDITOR_H
#define POOLEDITOR_H

#include <QGenericMatrix>
#include <QMouseEvent>
#include <QComboBox>
#include <QWidget>
#include <QSlider>
#include <QLabel>

#include "wlpTypes.h"

class ClickLabel;

//===========================================

class PoolEditor : public QWidget, public WLPBase
{
    Q_OBJECT
public:
    explicit PoolEditor(WLPPool *_pool, WLPFlag poolType, QWidget *parent = 0);
    void CloudInit();
    void AlignInit();
    void ScaleInit();
    void SetImagePoint(int _x, int _y){ m_X=_x; m_Y=_y; CloudUpdate(WLP_PREVIEW_ON); }
signals:
    
public slots:
    void AlignUpdate(int val);
    void CloudUpdate(int val){ Q_UNUSED(val); CloudUpdate(WLP_PREVIEW_ON); }
    void CloudUpdate(WLPFlag _previewState);
    void RenderActive();
    void Render();
    void Reset();

private:
    //Internal functions:
    //void CloudUpdate();
    QImage DetectEdges(QImage img, double threshold);

    //Scaling
    QComboBox *p_ScaleType;

    //Cloud Balance
    ClickLabel *p_ClickLabel;
    QSlider *p_Lightness;
    QComboBox *p_ImgBox;

    std::vector<QString>  m_ChanNames;
    std::vector<QImage> m_ImgVector;

    int m_X,m_Y;
    //The Images
    WLPPool *p_Pool;
    QImage *m_MasterImgs;
    QImage m_ImgScaled[WLP_POOL_MAX];
    QImage m_ImgPreview[WLP_POOL_MAX];
    bool *p_ActiveImg;
    QLabel *p_Label[7];

    int imgHeight;
    int imgWidth;

    QSlider *p_EdgeDelta;

    QGenericMatrix<9,9,qreal> m_CloudMatrix;
    QGenericMatrix<1,9,qreal> m_CloudVector;
};

//===========================

class ClickLabel : public QLabel
{
public:
    ClickLabel(PoolEditor *parent){
        p_Parent = parent;
        setStyleSheet("border: 2px solid");
    }

private:
    //ClickLabel(){}

    void mousePressEvent(QMouseEvent *event){
        double x = event->pos().x();
        double y = event->pos().y();
        p_Parent->SetImagePoint(x,y);
    }

    PoolEditor *p_Parent;
};

#endif // POOLEDITOR_H
