#include "pooleditor.h"
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QLabel>

extern WLPConfig g_Config;

PoolEditor::PoolEditor(WLPPool *_pool, WLPFlag poolType, QWidget *parent) :
    QWidget(parent)
{
    //General image loading
    p_Pool = _pool;
    m_MasterImgs = _pool->GetPool(); //allImgs; //for final renders
    p_ActiveImg = _pool->m_IsLoaded; //allLoads;
    m_ChanNames = _pool->m_Texts; //allChans;
    m_X = m_Y = 0;

    //Add all images:
    QImage temp;
    for(int i = 0; i < WLP_POOL_MAX; ++i)
        p_ActiveImg[i] ? m_ImgScaled[i] = m_MasterImgs[i].scaled(s_ViewWidth,s_ViewHeight,Qt::KeepAspectRatio) : temp;

    switch(poolType)
    {
    case WLP_EDIT_ALIGN:
        setWindowTitle("Developing Auto Align Feature");
        AlignInit();
        break;
    case WLP_EDIT_CLOUD:
        setWindowTitle("Cloud Balance (Universal White-point Selection)");
        CloudInit();
        break;
    case WLP_EDIT_SCALE:
        setWindowTitle("Scale pool to uniform size");
        ScaleInit();
        break;
    default:
        qDebug() << "PANIC!!!!";
        break;
    }
}

void PoolEditor::AlignInit()
{
    p_EdgeDelta = new QSlider(Qt::Horizontal,this);
    p_EdgeDelta->setRange(1,200);
    p_EdgeDelta->setValue(100);
    connect(p_EdgeDelta, SIGNAL(valueChanged(int)), this, SLOT(AlignUpdate(int)));

    QGridLayout *layout = new QGridLayout;
    double delta;

    for(int i = 0; i < 7; ++i)
    {
        if(p_ActiveImg[i])
        {
            p_Label[i] = new QLabel;
            QImage edgeImg = m_ImgScaled[i];

            delta = (double)p_EdgeDelta->value()/1000.0;
            p_Label[i]->setPixmap(QPixmap::fromImage(DetectEdges(edgeImg, delta)));
            layout->addWidget(p_Label[i], 0,i);
        }
    }
    layout->addWidget(p_EdgeDelta, 1,0,1,7, Qt::AlignCenter);
    setLayout(layout);
}

QImage PoolEditor::DetectEdges(QImage inImg, double threshold)
{
    QColor inCol, outCol;
    QColor pixLeft;
    QColor pixDown;
    QImage outImg(inImg.width(), inImg.height(), inImg.format());
    for(int x = 0; x < inImg.width(); ++x)
        for(int y = 0; y < inImg.height(); ++y)
        {
            inCol = inImg.pixel(x,y);
            outCol = QColor::fromRgbF(1,1,1);
            if( x > 1 && y > 1 )
            {
                pixLeft = inImg.pixel(x-1,y);
                pixDown = inImg.pixel(x,y-1);
                if( (std::abs(pixLeft.lightnessF() - inCol.lightnessF()) > threshold ||
                        std::abs(pixDown.lightnessF() - inCol.lightnessF()) > threshold) )
                {
                    outCol = QColor::fromRgbF(0,0,0);
                }

            }
            outImg.setPixel(x,y, outCol.rgb());
        }

    return outImg;
}

void PoolEditor::AlignUpdate(int val)
{
    double delta;
    for(int i = 0; i < 7; ++i)
    {
        if(p_ActiveImg[i])
        {
            QImage edgeImg = m_ImgScaled[i];

            delta = (double)val/1000.0;
            p_Label[i]->setPixmap(QPixmap::fromImage(DetectEdges(edgeImg, delta)));
        }
    }
}


void PoolEditor::CloudInit()
{
    p_ImgBox = new QComboBox;
    p_ImgBox->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );

    std::vector<QString>::iterator iter = m_ChanNames.begin();
    for(int i = 0; iter != m_ChanNames.end(); ++iter, ++i)
    {
        p_ImgBox->addItem( m_ChanNames[i] );
        p_ImgBox->setCurrentIndex(i);
    }

    QImage refImg;
    for(int i = 0; i < WLP_POOL_MAX; ++i)
    {
        if(p_ActiveImg[i])
        {
            refImg = m_ImgScaled[i];
            m_ImgVector.push_back(refImg);
        }
    }
    p_Lightness = new QSlider(Qt::Vertical);
    p_Lightness->setRange(1, 255);
    p_Lightness->setValue(255);
    connect(p_Lightness, SIGNAL(valueChanged(int)), this, SLOT(CloudUpdate(int)));
    p_ClickLabel = new ClickLabel(this);
    p_ClickLabel->setFixedWidth(refImg.width());
    p_ClickLabel->setFixedHeight(refImg.height());
    p_ClickLabel->setPixmap(QPixmap::fromImage(refImg));
    QGridLayout *temp = new QGridLayout;
    temp->addWidget(p_ClickLabel, 0, 0, 1, 3, Qt::AlignCenter);
    temp->addWidget(p_ImgBox, 1, 0, 1,2);
    temp->addWidget(p_Lightness, 0, 3);
    QPushButton *reset = new QPushButton("Reset");
    temp->addWidget(reset, 1,2,1,1);
    QPushButton *apply = new QPushButton("Apply (To Pool)");
    temp->addWidget(apply, 2,0);
    QPushButton *applyOnly = new QPushButton("Apply (This Only)");
    temp->addWidget(applyOnly, 2,1);
    QPushButton *cancel = new QPushButton("Cancel");
    temp->addWidget(cancel, 2,2,1,1);
    setLayout(temp);

    //================
    connect(p_ImgBox, SIGNAL(currentIndexChanged(int)), this, SLOT(CloudUpdate(int)));
    connect(applyOnly, SIGNAL(clicked()), this, SLOT(RenderActive()));
    connect(apply, SIGNAL(clicked()), this, SLOT(Render()));
    connect(cancel, SIGNAL(clicked()), this, SLOT(close()));
    connect(reset, SIGNAL(clicked()), this, SLOT(Reset()));
}



void PoolEditor::CloudUpdate(WLPFlag _previewState)
{
    for(int i = 0; i < WLP_POOL_MAX; ++i)
    {
        if(p_ActiveImg[i])
        {
            if(_previewState == WLP_PREVIEW_OFF)
                m_ImgPreview[i] = m_MasterImgs[i];
            else
                m_ImgPreview[i] = m_ImgScaled[i];

            QColor refClickCol;
            qreal refWhite = static_cast<qreal>(p_Lightness->value()) / 255.0;
            refClickCol = m_ImgScaled[i].pixel(m_X,m_Y);

             int values[3];
             for(int x = 0; x < m_ImgPreview[i].width(); ++ x)
             {
                 for(int y = 0; y < m_ImgPreview[i].height(); ++y)
                 {
                     QColor eachPixel = m_ImgPreview[i].pixel(x,y);

                     values[2] = (refWhite/refClickCol.redF()) * eachPixel.red();
                     values[1] = (refWhite/refClickCol.greenF()) * eachPixel.green();
                     values[0] = (refWhite/refClickCol.blueF()) * eachPixel.blue();

                     int r = qBound<int>(0, values[2], 255);
                     int g = qBound<int>(0, values[1], 255);
                     int b = qBound<int>(0, values[0], 255);
                     QRgb col = QColor::fromRgb(r, g, b).rgb();
                     m_ImgPreview[i].setPixel(x,y,col);
                 }
             }
        }
    }
    p_ClickLabel->setPixmap(QPixmap::fromImage( m_ImgPreview[IndexFromString(p_ImgBox->currentText())] ));
}

void PoolEditor::ScaleInit()
{
    p_ScaleType = new QComboBox;
    p_ScaleType->addItem("Stretch");
    p_ScaleType->addItem("Fill");
    p_ScaleType->addItem("Fit");
    //connect()

    bool err = false;
    QString errStr = "";
    QImage tmp;
    for(int i = 0; i < p_Pool->m_GenCount; ++i)
    {
        tmp = p_Pool->m_General[i].scaled(p_Pool->m_MaxSize.width, p_Pool->m_MaxSize.height);
        if(tmp == QImage())
        {
            err = true;
            errStr += "\t" + p_Pool->m_GenTexts[i] + "\n";
        }
        else
        {
            p_Pool->m_General[i] = tmp;
        }
    }

    //Show user messege if any failed
    if(err)
    {
        QMessageBox msgBox;
        msgBox.setText("The following images failed to scale due to memory issues:\n\n"
                       + errStr + "\nThis may be due to the scale size being too high.");
        msgBox.exec();
    }
}

void PoolEditor::Render()
{
    CloudUpdate(WLP_PREVIEW_OFF);
    for(int i = 0; i < WLP_POOL_MAX; ++i)
        m_MasterImgs[i] = m_ImgPreview[i];
}

void PoolEditor::RenderActive()
{
    CloudUpdate(WLP_PREVIEW_OFF);
    int index = IndexFromString( p_ImgBox->currentText() );
    m_MasterImgs[index] = m_ImgPreview[index];
}

void PoolEditor::Reset()
{
    p_ClickLabel->setPixmap(QPixmap::fromImage( m_ImgScaled[IndexFromString(p_ImgBox->currentText())] ));
}
