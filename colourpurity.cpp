#include "colourpurity.h"
#include <QPushButton>
#include <QGroupBox>

#include "wlpsettings.h"

extern WLPConfig g_Config;

ColourPurity::ColourPurity(WLPPool *_pool, QWidget *p_Parent = 0) :
    QWidget(p_Parent)
{
    p_Pool = _pool;
    m_ImgFull = *p_Pool->GetImg("VIS (400-650nm)");
    m_ImgScaled = m_ImgFull.scaled(s_ViewWidth,s_ViewHeight, Qt::KeepAspectRatio);
    m_ImgDisplay = m_ImgScaled;

    p_SearchSpace = new QComboBox;
    p_SearchSpace->addItem("RGB");
    p_SearchSpace->addItem("Lab");
    p_SearchSpace->addItem("HSL");
    connect(p_SearchSpace, SIGNAL(currentIndexChanged(int)), this, SLOT(Update(int)));

    QImage bloo(20,20,QImage::Format_ARGB32);
    for(int i = 0; i < 20; ++i)
    {
        for(int j = 0; j < 20; ++j)
        {
            QColor c;
            if(i+j >= 20)
                c = QColor::fromHslF(0,0,0.2);
            else
                c = QColor::fromHslF(0,0,0.8);

            bloo.setPixel(i,j,c.rgb());
        }
    }
    m_NoColour = bloo;

    //Set up basic colour-palettes
    //Natural Colour System (Scandinavian Colour Institute)
    NCS[0] = QColor(255,255,255);   //White
    NCS[1] = QColor(196,   2,  51); //Red
    NCS[2] = QColor(255, 211,   0); //Yellow
    NCS[3] = QColor(0, 159, 107);   //Green
    NCS[4] = QColor( 0, 135, 189);  //Blue
    NCS[5] = QColor(0,0,0);
    //HSL Rainbow
    SIX[0] = QColor::fromHsl(0, 255, 127); // Red
    SIX[1] = QColor::fromHsl(60, 255, 127); // Yellow
    SIX[2] = QColor::fromHsl(120, 255, 127); // Green
    SIX[3] = QColor::fromHsl(180, 255, 127); // Cyan
    SIX[4] = QColor::fromHsl(240, 255, 127); // Blue
    SIX[5] = QColor::fromHsl(300, 255, 127); // Magenta

    p_SwatchDrop = new QComboBox;
    p_SwatchDrop->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    p_SwatchDrop->addItem(" - Select a colour palette - ");
    p_SwatchDrop->addItem("Natural Color System");
    p_SwatchDrop->addItem(tr("60\u00B0 Hue Rainbow"));
    p_SwatchDrop->addItem("Custom...");
    p_ColType[0] = new QRadioButton("Hue");
    connect(p_ColType[0], SIGNAL(clicked()), this, SLOT(Update()));
    p_ColType[1] = new QRadioButton("Chromaticity");
    connect(p_ColType[1], SIGNAL(clicked()), this, SLOT(Update()));
    p_ColType[2] = new QRadioButton("Colour");
    connect(p_ColType[2], SIGNAL(clicked()), this, SLOT(Update()));

    p_ColType[0]->setChecked(true);
    connect(p_SwatchDrop, SIGNAL(currentIndexChanged(QString)), this, SLOT(CreateSwatch(QString)));

    p_Purity = new QSlider(Qt::Horizontal);
    p_Purity->setRange(0,100);
    p_Purity->setValue(0);
    p_Label = new QLabel;
    p_Label->setPixmap( QPixmap::fromImage( m_ImgDisplay ) );
    p_Layout = new QGridLayout;
    p_Layout->addWidget(p_Label, 0,0,1,10, Qt::AlignHCenter);
    p_Layout->addWidget(p_ColType[0], 1,0, 1, 2);
    p_Layout->addWidget(p_ColType[1], 1,2, 1, 2);
    p_Layout->addWidget(p_ColType[2], 1,4, 1, 2);
    p_Layout->addWidget(p_SwatchDrop, 1,6, 1, 4);

    QGroupBox *pal = new QGroupBox;
    QGridLayout *tmp = new QGridLayout;
    for(int i = 0; i < 10; ++i)
    {
        p_ColImgs[i] = new QImage(bloo);
        p_Colours[i] = new QLabel;
        p_Colours[i]->setPixmap( QPixmap::fromImage( *p_ColImgs[i] ) );
        tmp->addWidget(p_Colours[i],0,i,1,1);
    }
    pal->setLayout(tmp);
    p_Layout->addWidget(pal,2,0,1,10);
    p_Layout->addWidget(p_Purity,3,0,1,10);
    p_Layout->addWidget(new QLabel("Measure by:"), 4,0,1,2);
    p_Layout->addWidget(p_SearchSpace,4,2,1,8);
    connect(p_Purity, SIGNAL(sliderReleased()), this, SLOT(Update()));

    QPushButton *save = new QPushButton("Save");
    connect(save, SIGNAL(clicked()), this, SLOT(Render()));
    p_Layout->addWidget(save, 5, 0, 1,3);

    QPushButton *drop = new QPushButton("Drop to Pool");
    connect(drop, SIGNAL(clicked()), this, SLOT(Drop()));
    p_Layout->addWidget(drop, 5, 7, 1,3);

    setLayout(p_Layout);
    setWindowTitle("Colour Purity");


}

WLPMode ToEnum(QString str)
{
    if(str == "RGB")
        return WLP_MODE_RGB;

    if(str == "Lab")
        return WLP_MODE_LAB;

    if(str == "HSL")
        return WLP_MODE_HSL;

    QString error("Could not convert " + str + " to enum");
    throw std::runtime_error(error.toStdString());

}

void ColourPurity::CreateSwatch(QString type)
{
    ColourSwatch *test = nullptr;
    if(type == " - Select a colour palette - ")
    {
        return;
    }

    if(type == "Natural Color System")
    {
        m_Swatch.clear();
        for(int i = 0; i < 6; ++i)
            AddColour(NCS[i]);
    }

    if(type == tr("60\u00B0 Hue Rainbow"))
    {
        m_Swatch.clear();
        for(int i = 0; i < 6; ++i)
            AddColour(SIX[i]);
    }

    if(type == "Custom...")
    {
        m_Swatch.clear();

        WLPMode searchSpace = ToEnum(p_SearchSpace->currentText());

        //Custom palette
        if(p_ColType[0]->isChecked())
        {
            test = new ColourSwatch(this, searchSpace, this);
        }
        else if(p_ColType[1]->isChecked())
        {
            test = new ColourSwatch(this, searchSpace, this);
        }
        else if(p_ColType[2]->isChecked())
        {
            test = new ColourSwatch(this, searchSpace, this);
        }
        else{}

        if (test != nullptr)
            test->show();
    }
}


qreal lerp(qreal t, qreal a, qreal b){
      return (1-t)*a + t*b;
  }

QColor ColourPurity::ClosestCol(QColor pix, QList<QColor> swatch, int type) //0:hue, 1:chromo, 2:colour
{
    int index = swatch.size()-1;
    qreal distance = 99999;
    for(int i = 0; i < swatch.size(); ++i)
    {
        QColor other =  swatch.at(i);
        qreal deltaE;
        if(type == 0)
        {
            deltaE = MeasureHue(pix, other);
        }
        else if(type == 1) //Lab chromo distance
        {
            deltaE = MeasureChroma(pix, other);
        }
        else //RGB colour distance
        {
            deltaE = MeasureColour(pix, other);
        }

        if( deltaE < distance)
        {
            index = i;
            distance = deltaE;
        }
    }
    return swatch.at(index);
}

void ColourPurity::Update(WLPFlag _previewState)
{
    if(m_Swatch.size() == 0)
        return;

    //Update swatch
    for(int i = 0; i < m_Swatch.size() && i < 10; ++i)
    {
        QColor col = m_Swatch.at(i);
        p_ColImgs[i]->fill(col.rgb());
        p_Colours[i]->setPixmap( QPixmap::fromImage( *p_ColImgs[i] ) );
    }

    for(int i = m_Swatch.size(); i < 10; ++i)
    {
        p_Colours[i]->setPixmap( QPixmap::fromImage( m_NoColour ) );
    }

    //Update image
    QImage refImg;
    if(_previewState == WLP_PREVIEW_ON)
        refImg = m_ImgScaled;
    else
        refImg = m_ImgFull;

    qreal val = static_cast<qreal>(p_Purity->value())/100.0;
    for(int i = 0; i < refImg.width(); ++i)
    {
        for(int j = 0; j < refImg.height(); ++j)
        {
            QColor colA = refImg.pixel(i,j);
            QColor colB;
            QColor c;
            if(p_ColType[0]->isChecked())
            {
                colB = ClosestCol(colA, m_Swatch, 0);
                qreal h = qBound<qreal>(0, lerp(val, colA.hslHueF(), colB.hslHueF()), 1);
                c = QColor::fromHslF( h, colA.hslSaturationF(), colA.lightnessF() );
            }
            else if(p_ColType[1]->isChecked())
            {
                colB = ClosestCol(colA, m_Swatch, 1);
                QColor labA = lutRGBtoLab[colA.red()][colA.green()][colA.blue()];
                QColor labB = lutRGBtoLab[colB.red()][colB.green()][colB.blue()];
                int L = 255 * labA.redF();
                int a = 255 * qBound<qreal>(0, lerp(val, labA.greenF(), labB.greenF()), 1);
                int b = 255 * qBound<qreal>(0, lerp(val, labA.blueF(), labB.blueF()), 1);
                c = lutLabtoRGB[L][a][b];
            }
            else if(p_ColType[2]->isChecked())
            {
                colB = ClosestCol(colA, m_Swatch, 2);
                qreal r = qBound<qreal>(0, lerp(val, colA.redF(), colB.redF()), 1);
                qreal g = qBound<qreal>(0, lerp(val, colA.greenF(), colB.greenF()), 1);
                qreal b = qBound<qreal>(0, lerp(val, colA.blueF(), colB.blueF()), 1);
                c = QColor::fromRgbF( r,g,b );
            }else{}

            refImg.setPixel(i,j,c.rgb());
        }
    }

    m_ImgDisplay = refImg;
    p_Label->setPixmap( QPixmap::fromImage( m_ImgDisplay ) );
}

void ColourPurity::Render()
{
    Update(WLP_PREVIEW_OFF);

    //Open Save As dialog
    QString newName = QFileDialog::getSaveFileName(this, tr("Save image"),
                                                   g_Config.Get(WLPC_DIR_SAVE), tr("JPG (*.jpg) ;; PNG (*.png) ;; TIF (*.tif)") );
    //Save to new path/name
    m_ImgDisplay.save(newName,NULL,100);

    //Set that path as new save path
    QFileInfo bloo(newName);
    WLPSettings s; s.UpdateSaveDir(bloo.absolutePath());
}

void ColourPurity::Drop()
{
    Update(WLP_PREVIEW_OFF);
    p_Pool->GetPool()[WLP_POOL_PURE] = m_ImgDisplay;
    p_Pool->Drop("Colour Pure");
}

ColourSwatch::ColourSwatch(ColourPurity *_main, WLPMode _mode, QWidget *_parent)
{
    Q_UNUSED(_mode);
    Q_UNUSED(_parent);

    p_Main = _main;
    m_X = m_Y = 0;
    p_LightSlide = new QSlider(Qt::Horizontal);
    connect(p_LightSlide, SIGNAL(sliderMoved(int)), this, SLOT(Update(int)));
    p_LightSlide->setRange(0,360);
    p_LightSlide->setValue(180);
    QImage refImg(360,360,QImage::Format_ARGB32);
    for(int i = 0; i < 360; ++i)
    {
        for(int j = 0; j < 360; ++j)
        {
            qreal h = static_cast<qreal>(i)/360.0;
            qreal s = static_cast<qreal>(j)/360.0;
            qreal l = static_cast<qreal>(p_LightSlide->value())/360.0;
            QColor c = QColor::fromHslF(h,s,l);
            refImg.setPixel(i,j,c.rgb());
        }
    }
    p_ClickLabel = new ClickHue(this);
    p_ClickLabel->setFixedWidth(refImg.width());
    p_ClickLabel->setFixedHeight(refImg.height());
    p_ClickLabel->setPixmap(QPixmap::fromImage(refImg));

    QPushButton *addButton = new QPushButton("Add");
    connect(addButton, SIGNAL(clicked()), this, SLOT(AddColour()));

    QImage clickImg(40,20,QImage::Format_ARGB32);
    QColor col = QColor::fromHslF(m_X, m_Y, 0);
    clickImg.fill(col.rgb());
    p_ColPreview = new QLabel;
    p_ColPreview->setPixmap(QPixmap::fromImage(clickImg));
    QGridLayout *temp = new QGridLayout;
    temp->addWidget(p_ClickLabel, 0,0);
    temp->addWidget(p_LightSlide, 1,0);
    temp->addWidget(p_ColPreview, 0,1);
    temp->addWidget(addButton, 0,2);
    setLayout(temp);
}

void ColourSwatch::AddColour()
{
    p_Main->AddColour(m_NextCol);
    p_Main->Update();
}

void ColourSwatch::Update()
{
    qreal slideVal = static_cast<qreal>(p_LightSlide->value())/360.0;
    //Draw Colour Plane showing all chromaticities
    QImage refImg(360,360,QImage::Format_ARGB32);
    for(int i = 0; i < 360; ++i)
    {
        for(int j = 0; j < 360; ++j)
        {
            qreal h = static_cast<qreal>(i)/360.0;
            qreal s = static_cast<qreal>(j)/360.0;
            qreal l = 0.5;
            QColor c = QColor::fromHslF(h,s,l);
            refImg.setPixel(i,j,c.rgb());
        }
    }
    p_ClickLabel->setPixmap(QPixmap::fromImage(refImg));

    QImage clickImg(40,20,QImage::Format_ARGB32);
    QColor col = QColor::fromHslF(m_X, m_Y, slideVal);
    clickImg.fill(col.rgb());
    p_ColPreview->setPixmap(QPixmap::fromImage(clickImg));
    m_NextCol = col;
}

void ColourSwatch::SetXY(qreal _x, qreal _y)
{
    m_X = _x/360.0;
    m_Y = _y/360.0;
    Update();
}

void ClickHue::mousePressEvent(QMouseEvent *event){
    double x = event->pos().x();
    double y = event->pos().y();
    p_Parent->SetXY(x,y);
}


//Functions for extracting saturation and lightness from RGB
void RgbHueTransform(qreal *rgb)
{
    //Convert RGB to a 3-tuple hue value
    qreal M = qMax(rgb[0], qMax(rgb[1], rgb[2]));
    qreal m = qMin(rgb[0], qMin(rgb[1], rgb[2]));
    qreal scale = 255.0 / (M-m);
    qreal r = (rgb[0] - m) * scale;
    qreal g = (rgb[1] - m) * scale;
    qreal b = (rgb[2] - m) * scale;

    //Normalise to rg-Hue
    rgb[0] = r / (r+g+b);
    rgb[1] = g / (r+g+b);
    rgb[2] = b / (r+g+b);
}

// 1D, distance within a line
qreal ColourPurity::MeasureHue(QColor original, QColor palette)
{
    WLPMode searchSpace = ToEnum(p_SearchSpace->currentText());
    qreal hueOne, hueTwo;
    if(searchSpace == WLP_MODE_HSL)
    {
        hueOne = original.hslHueF();
        hueTwo = palette.hslHueF();
    }
    else if(searchSpace == WLP_MODE_LAB)
    {
        QColor labO = lutRGBtoLab[original.red()][original.green()][original.blue()];
        QColor labT = lutRGBtoLab[palette.red()][palette.green()][palette.blue()];


        qreal CIE_a = (labO.greenF() - 0.5) * 256.0;
        qreal CIE_b = (labO.blueF() - 0.5) * 256.0;

        qreal var_H = atan2( CIE_b, CIE_a );  //Quadrant by signs

        if ( var_H > 0 ) var_H = ( var_H / M_PI ) * 180;
        else             var_H = 360 - ( std::abs( var_H ) / M_PI ) * 180;

        qreal CIE_HOne = var_H;
        if(CIE_HOne > 359) CIE_HOne -= 360;
        if(CIE_HOne < 0) CIE_HOne += 360;

        CIE_a = (labT.greenF() - 0.5) * 256.0;
        CIE_b = (labT.blueF() - 0.5) * 256.0;

        var_H = atan2( CIE_b, CIE_a );  //Quadrant by signs

        if ( var_H > 0 ) var_H = ( var_H / M_PI ) * 180;
        else             var_H = 360 - ( std::abs( var_H ) / M_PI ) * 180;

        qreal CIE_HTwo = var_H;
        if(CIE_HTwo > 359) CIE_HTwo -= 360;
        if(CIE_HTwo < 0) CIE_HTwo += 360;

        hueOne = atan2(labO.blueF(), labO.greenF());
        hueTwo = atan2(labT.blueF(), labT.greenF());
    }
    else {
        qreal a[3];
        a[0] = original.red(); a[1] = original.green(); a[2] = original.blue();
        RgbHueTransform(a);
        qreal b[3];
        b[0] = palette.red(); b[1] = palette.green(); b[2] = palette.blue();
        RgbHueTransform(b);

        qreal deltaE = qSqrt( qPow(a[0] - b[0], 2)
                              + qPow(a[1] - b[1], 2) );
        return deltaE;
    }

    qreal deltaE = qAbs(hueOne - hueTwo);
    return deltaE;
}

// 2D, distance within a rectangle
qreal ColourPurity::MeasureChroma(QColor original, QColor palette)
{
    WLPMode searchSpace = ToEnum(p_SearchSpace->currentText());
    qreal aO = 0, aT = 0, bO = 0, bT = 0;
    if(searchSpace == WLP_MODE_HSL)
    {
        aO = original.hslHueF(); //a*
        aT = palette.hslHueF(); //a*
        bO = original.hslSaturationF(); //b*
        bT = palette.hslSaturationF(); //b*
    }
    else if(searchSpace == WLP_MODE_LAB)
    {
        QColor labO = lutRGBtoLab[original.red()][original.green()][original.blue()];
        QColor labT = lutRGBtoLab[palette.red()][palette.green()][palette.blue()];
        aO = labO.greenF(); //a*
        aT = labT.greenF(); //a*
        bO = labO.blueF(); //b*
        bT = labT.blueF(); //b*
    }
    else
    {
        aO = RGBtoRGC(original).redF();
        aT = RGBtoRGC(palette).redF();
        bO = RGBtoRGC(original).greenF();
        bT = RGBtoRGC(palette).greenF();
    }

    qreal deltaE = qSqrt( qPow(aO - aT, 2) + qPow(bO - bT, 2) );
    return deltaE;
}

//3D, distance within a cuboid
qreal ColourPurity::MeasureColour(QColor original, QColor palette)
{
    WLPMode searchSpace = ToEnum(p_SearchSpace->currentText());
    qreal a[2], b[2], c[2];
    if(searchSpace == WLP_MODE_HSL)
    {
        a[0] = original.hslHueF(); a[1] = palette.hslHueF();
        b[0] = original.hslSaturationF(); b[1] = palette.hslSaturationF();
        c[0] = original.lightnessF(); c[1] = palette.lightnessF();
    }
    else if(searchSpace == WLP_MODE_LAB)
    {
        QColor labO = lutRGBtoLab[original.red()][original.green()][original.blue()];
        QColor labT = lutRGBtoLab[palette.red()][palette.green()][palette.blue()];
        a[0] = labO.redF(); a[1] = labT.redF();
        b[0] = labO.greenF(); b[1] = labT.greenF();
        c[0] = labO.blueF(); c[1] = labT.blueF();
    }
    else
    {
        a[0] = original.redF(); a[1] = palette.redF();
        b[0] = original.greenF(); b[1] = palette.greenF();
        c[0] = original.blueF(); c[1] = palette.blueF();
    }

    qreal deltaE = qSqrt( qPow(a[0] - a[1], 2)
                          + qPow(b[0] - b[1], 2)
                          + qPow(c[0] - c[1], 2) );

    return deltaE;
}
