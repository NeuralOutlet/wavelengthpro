#ifndef COLOURPURITY_H
#define COLOURPURITY_H

#include <QRadioButton>
#include <QGridLayout>
#include <QMouseEvent>
#include <QComboBox>
#include <QSlider>
#include <QWidget>
#include <QLabel>

#include "wlpTypes.h"

//Forward declerations
class ClickHue;
struct ThreeTuple {
    WLPMode mode;
    qreal a,b,c;
    ThreeTuple(){ a = 1; b = 1; c = 1; mode = WLP_MODE_RGB; }
    ThreeTuple(qreal _a, qreal _b, qreal _c, WLPMode _mode = WLP_MODE_RGB){
        a = _a; b = _b; c = _c; mode = _mode;
    }
};

//========================

class ColourPurity : public QWidget, public WLPBase
{
    friend class ColourSwatch;
    Q_OBJECT
public:
    explicit ColourPurity(WLPPool *_pool, QWidget *p_Parent);

    QColor ClosestCol(QColor pix, QList<QColor> swatch, int type);

    void AddColour(QColor _col){
        m_Swatch.push_front(_col);
        Update();
    }

    qreal MeasureHue(QColor, QColor);
    qreal MeasureChroma(QColor, QColor);
    qreal MeasureColour(QColor, QColor);

signals:

public slots:
    void CreateSwatch(QString);

    // Various update methods
    void Update(int i){ Q_UNUSED(i); Update(WLP_PREVIEW_ON); }
    void Update(){ Update(WLP_PREVIEW_ON); }
    void Update(WLPFlag _previewState);
    void Render();
    void Drop();

private:
    //Layout stuff
    QList<QColor> m_Swatch;
    QSlider *p_Purity;
    QRadioButton *p_ColType[3];
    QComboBox *p_SwatchDrop;
    QComboBox *p_SearchSpace;

    QImage m_NoColour;

    WLPPool *p_Pool;

    QImage *p_ColImgs[10];
    QLabel *p_Colours[10];
    QImage m_ImgFull;
    QImage m_ImgScaled;
    QImage m_ImgDisplay;
    QLabel *p_Label;
    QGridLayout *p_Layout;

    QColor NCS[6];
    QColor SIX[6];
};


//Colour Swatch class
class ColourSwatch : public QWidget, public WLPBase
{
    Q_OBJECT
public:
    explicit ColourSwatch(ColourPurity *_main, WLPMode _mode, QWidget *_parent = 0);



    WLPMode GetMode(){ return m_Mode; }
    void SetCol(ThreeTuple _col){ m_Selected = _col; }
    void SetXY(qreal _x, qreal _y);

signals:

public slots:
    //ThreeTuple ReturnColour(WLPMode _mode);
    void Update();
    void Update(int i){ Q_UNUSED(i); Update(); }
    void AddColour();


private:
    QImage m_Colour;
    QSlider *p_LightSlide;
    ColourPurity *p_Main;

    WLPMode m_Mode;

    QColor m_NextCol;
    QLabel *p_ColPreview;
    ClickHue *p_ClickLabel;
    ThreeTuple m_Selected;
    qreal m_X, m_Y;
};

//===========================

class ClickHue : public QLabel
{
public:
    ClickHue(ColourSwatch *parent){
        p_Parent = parent;
    }

private:
    //ClickLabel(){}

    void mousePressEvent(QMouseEvent *event);

    ColourSwatch *p_Parent;
};

#endif // COLOURPURITY_H
