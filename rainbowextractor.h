#ifndef RAINBOWEXTRACTOR_H
#define RAINBOWEXTRACTOR_H

#include <QRadioButton>
#include <QPushButton>
#include <QGridLayout>
#include <QMessageBox>
#include <QPaintEvent>
#include <QSpinBox>
#include <QPainter>
#include <QWidget>
#include <QSlider>
#include <QLabel>

#include "wlpTypes.h"

class RainbowExtractor : public QWidget, public WLPBase
{
    Q_OBJECT
public:
    explicit RainbowExtractor(WLPPool *_pool, QWidget *parent = 0);

    QColor NCS[6];

public slots:
    void SwitchBlur() { m_BlurOn = !m_BlurOn; }

    // Various Update methods
    void Update(int i){ Q_UNUSED(i); Update(WLP_PREVIEW_ON); }
    void Update(){ Update(WLP_PREVIEW_ON); }
    void Update(WLPFlag _previewState);
    void Render();
    void Drop();
    
signals:
    
public:
    WLPPool *p_Pool;
    QImage m_RefImg;
    QImage m_ScaledImg;
    QImage m_ExtractImg;
    QImage m_HueBar;

    bool m_BlurOn;

    QLabel *p_RefView;
    QLabel *p_ExtractView;
    QLabel *p_HueView;

    QRadioButton *p_BlendRadio;

    QSlider *p_HueSlide;
    QSlider *p_DeltaSlide;
    QSlider *p_SatSlide;
    QSlider *p_LightSlide;
    QSlider *p_AlphaSlide;
    
};

#endif // RAINBOWEXTRACTOR_H
