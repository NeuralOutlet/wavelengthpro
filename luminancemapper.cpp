#include "luminancemapper.h"

#include <QPushButton>
#include <QSpinBox>
#include <QtDebug>
#include <QLineEdit>

#include "wlpsettings.h"

extern WLPConfig g_Config;

void AddBases(QComboBox *box)
{
    box->addItem("RGB"); box->addItem("CMY");
    box->addItem("HSL"); box->addItem("HSV"); box->addItem("HSI");
    box->addItem("YUV (PAL TVs)"); box->addItem("YIQ (NTSC TVs)");
    box->addItem("CIE XYZ"); box->addItem("CIE UVW");
    box->addItem("CIE Luv"); box->addItem("CIE LCHuv");
    box->addItem("Hunter-Lab");
    box->addItem("CIE Lab"); box->addItem("CIE LCHab");
    box->addItem("CIE LMS");
    box->addItem("rgG"); box->addItem("rgI");
}

void AddComponents(QComboBox *box)
{
    box->addItem("RGB - Red");
    box->addItem("RGB - Green");
    box->addItem("RGB - Blue");
    box->insertSeparator(3);
    box->addItem("CMY - Cyan");
    box->addItem("CMY - Magenta");
    box->addItem("CMY - Yellow");
    box->insertSeparator(7);
    box->addItem("HSL - Hue");
    box->addItem("HSL - Saturation");
    box->addItem("HSL - Lightness");
    box->insertSeparator(11);
    box->addItem("HSV - Hue");
    box->addItem("HSV - Saturation");
    box->addItem("HSV - Value");
    box->insertSeparator(15);
    box->addItem("HSI - Hue");
    box->addItem("HSI - Saturation");
    box->addItem("HSI - Intensity");
    box->insertSeparator(19);
    box->addItem("YUV - Y (Luma)");
    box->addItem("YUV - U (Chrominance)");
    box->addItem("YUV - V (Chrominance)");
    box->insertSeparator(23);
    box->addItem("YIQ - Y (Luma)");
    box->addItem("YIQ - I (Chrominance)");
    box->addItem("YIQ - Q (Chrominance)");
    box->insertSeparator(27);
    box->addItem("XYZ - X");
    box->addItem("XYZ - Y");
    box->addItem("XYZ - Z");
    box->insertSeparator(31);
    box->addItem("UVW - U (Chrominance)");
    box->addItem("UVW - V (Chrominance)");
    box->addItem("UVW - W (Lightness)");
    box->insertSeparator(35);
    box->addItem("Luv - Lightness");
    box->addItem("Luv - u (Chrominance)");
    box->addItem("Luv - v (Chrominance)");
    box->insertSeparator(39);
    box->addItem("LCHuv - Lightness");
    box->addItem("LCHuv - Chromaticity");
    box->addItem("LCHuv - Hue");
    box->insertSeparator(43);
    box->addItem("H-Lab - Lightness");
    box->addItem("H-Lab - a (Opponent Colour)");
    box->addItem("H-Lab - b (Opponent Colour)");
    box->insertSeparator(47);
    box->addItem("Lab - Lightness");
    box->addItem("Lab - a (Opponent Colour)");
    box->addItem("Lab - b (Opponent Colour)");
    box->insertSeparator(51);
    box->addItem("LCHab - Lightness");
    box->addItem("LCHab - Chromaticity");
    box->addItem("LCHab - Hue");
    box->insertSeparator(55);
    box->addItem("LMS - Long");
    box->addItem("LMS - Medium");
    box->addItem("LMS - Short");
    box->insertSeparator(59);
    box->addItem("rg - Red Chromaticity");
    box->addItem("rg - Green Chromaticity");
    box->addItem("rg - Blue Chromaticity");
    box->insertSeparator(63);
    box->addItem("cm - Cyan Chromaticity");
    box->addItem("cm - Magenta Chromaticity");
    box->addItem("cm - Yellow Chromaticity");
    box->insertSeparator(67);
    box->addItem("CMYK - Cyan");
    box->addItem("CMYK - Magenta");
    box->addItem("CMYK - Yellow");
    box->addItem("CMYK - Key (Black)");
    box->insertSeparator(72);
    box->addItem("RGBW - Red%");
    box->addItem("RGBW - Green%");
    box->addItem("RGBW - Blue%");
    box->addItem("RGBW - White%");

}

LuminanceMapper::LuminanceMapper(WLPPool *_pool, QWidget *p_Parent_Ptr) :
    QWidget(p_Parent_Ptr)
{
    p_Pool = _pool;
    setWindowTitle("Colour-Space Mapper");
    m_MasterImgs_Ptr = p_Pool->GetPool();

    p_RgbSpace = new QComboBox;
    p_RgbSpace->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    p_RgbSpace->addItem("sRGB"); p_RgbSpace->addItem("Adobe RGB"); p_RgbSpace->addItem("Apple RGB"); p_RgbSpace->addItem("CIE RGB");
    p_RgbSpace->addItem("ProPhoto RGB"); p_RgbSpace->addItem("ECI RGB"); p_RgbSpace->addItem("NTSC RGB"); p_RgbSpace->addItem("PAL RGB");
    p_RgbSpace->addItem("Wide Gamut RGB"); p_RgbSpace->addItem("MatchColor RGB");
    p_RgbWhite = new QComboBox;
    p_RgbWhite->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    p_RgbWhite->addItem("A"); p_RgbWhite->addItem("C"); p_RgbWhite->addItem("D50"); p_RgbWhite->addItem("D55"); p_RgbWhite->addItem("D65");
    p_RgbWhite->addItem("D75"); p_RgbWhite->addItem("E"); p_RgbWhite->addItem("F2"); p_RgbWhite->addItem("F7"); p_RgbWhite->addItem("F11");
    p_RgbWhite->setCurrentIndex(4);
    p_RgbGamma = new QComboBox;
    p_RgbGamma->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    p_RgbGamma->addItem("1.0"); p_RgbGamma->addItem("1.8"); p_RgbGamma->addItem("2.2"); p_RgbGamma->addItem("sRGB"); p_RgbGamma->addItem("L*");
    p_RgbGamma->setCurrentIndex(3);

    //Combo box with presets
    m_ActivePreset = false;
    p_Presets = new QComboBox;
    p_Presets->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    p_Presets->addItem("-None-");
    AddBases(p_Presets);
    CustomSet rgG(15,60,61,1);
    m_CustomSets.push_back(rgG);
    CustomSet rgI(16,60,61,18);
    m_CustomSets.push_back(rgI);
    //CustomSet uvg(7,2,21,22);
    //m_CustomSets.push_back(uvg);
    p_PreName = new QLineEdit("My Preset");
    p_PreBase = new QComboBox; AddBases(p_PreBase);
    //p_PreBase->addItem("UV Gold");
    p_PreChanA = new QComboBox; AddComponents(p_PreChanA);
    p_PreChanA->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    p_PreChanB = new QComboBox; AddComponents(p_PreChanB);
    p_PreChanB->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    p_PreChanC = new QComboBox; AddComponents(p_PreChanC);
    p_PreChanC->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );


    m_BoxImgA_Ptr = new QComboBox;
    m_BoxImgA_Ptr->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    m_BoxImgB_Ptr = new QComboBox;
    m_BoxImgB_Ptr->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    m_BoxImgC_Ptr = new QComboBox;
    m_BoxImgC_Ptr->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );

    //Add the active channels:
    std::vector<QString>::iterator iter = p_Pool->m_Texts.begin();
    for(; iter != p_Pool->m_Texts.end(); ++iter)
    {
        QString imgType = (*iter);
        //add to channel mixers
        m_BoxImgA_Ptr->addItem(imgType);
        //m_BoxImgA_Ptr->setCurrentText(imgType);
        m_BoxImgB_Ptr->addItem(imgType);
        //m_BoxImgB_Ptr->setCurrentText(imgType);
        m_BoxImgC_Ptr->addItem(imgType);
        //m_BoxImgC_Ptr->setCurrentText(imgType);
    }

    iter = p_Pool->m_GenTexts.begin();
    for(; iter != p_Pool->m_GenTexts.end(); ++iter)
    {
        QString imgType = (*iter);
        //add to channel mixers
        m_BoxImgA_Ptr->addItem(imgType);
        //m_BoxImgA_Ptr->setCurrentText(imgType);
        m_BoxImgB_Ptr->addItem(imgType);
        //m_BoxImgB_Ptr->setCurrentText(imgType);
        m_BoxImgC_Ptr->addItem(imgType);
        //m_BoxImgC_Ptr->setCurrentText(imgType);
    }

    //Add all images:
    int activeImg = -1;
    for(int i = 0; i < WLP_POOL_MAX; ++i)
    {
        if(p_Pool->m_IsLoaded[i])
        {
            activeImg = i;
            m_ImgScaled[i] = m_MasterImgs_Ptr[i].scaled(s_ViewWidth, s_ViewHeight, Qt::KeepAspectRatio);
        }
    }

    // Account for only general images
    if(activeImg != -1)
        m_MixedImg = m_MixedImg = m_MasterImgs_Ptr[activeImg].scaled(s_ViewWidth, s_ViewHeight,Qt::KeepAspectRatio);
    else
        m_MixedImg = p_Pool->GetImgGeneral(0)->scaled(s_ViewWidth, s_ViewHeight,Qt::KeepAspectRatio);

    m_Width = m_MixedImg.width();
    m_Height = m_MixedImg.height();

    //Set up widget:
    m_Layout_Ptr = new QGridLayout;
    m_Base_Ptr = new QComboBox;
    m_Base_Ptr->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    AddBases(m_Base_Ptr);
    connect(m_Base_Ptr, SIGNAL(currentIndexChanged(QString)), this, SLOT(UpdateBase(QString)));

    m_BoxSpaceA_Ptr = new QComboBox; AddComponents(m_BoxSpaceA_Ptr);
    m_BoxSpaceA_Ptr->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    m_BoxSpaceB_Ptr = new QComboBox; AddComponents(m_BoxSpaceB_Ptr);
    m_BoxSpaceB_Ptr->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    m_BoxSpaceC_Ptr = new QComboBox; AddComponents(m_BoxSpaceC_Ptr);
    m_BoxSpaceC_Ptr->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );

    //Set the choice to RGB:
    m_BoxSpaceA_Ptr->setCurrentIndex(0);
    m_BoxSpaceB_Ptr->setCurrentIndex(1);
    m_BoxSpaceC_Ptr->setCurrentIndex(2);
    connect(m_BoxSpaceA_Ptr, SIGNAL(currentIndexChanged(QString)), this, SLOT(UpdateElementA(QString)));
    connect(m_BoxSpaceB_Ptr, SIGNAL(currentIndexChanged(QString)), this, SLOT(UpdateElementB(QString)));
    connect(m_BoxSpaceC_Ptr, SIGNAL(currentIndexChanged(QString)), this, SLOT(UpdateElementC(QString)));

    m_LabelA_Ptr = new QLabel("R");
    m_LabelB_Ptr = new QLabel("G");
    m_LabelC_Ptr = new QLabel("B");

    QGroupBox *outBox = new QGroupBox("Base Colour-space");
    QGridLayout *layout = new QGridLayout;
    layout->addWidget(new QLabel("Mode:"), 0,0);
    layout->addWidget(m_Base_Ptr,0,1);
    QPushButton *settingsButton = new QPushButton("Settings");
    connect(settingsButton, SIGNAL(clicked()), this, SLOT(EditSettings()));
    layout->addWidget(settingsButton, 0,2);
    outBox->setLayout(layout);

    QGroupBox *presetBox = new QGroupBox("Preset Mixes");
    layout = new QGridLayout;
    QPushButton *createPreset = new QPushButton("Add New...");
    connect(createPreset, SIGNAL(clicked()), this, SLOT(CreatePreset()));
    connect(p_Presets, SIGNAL(currentIndexChanged(int)), this, SLOT(SetPreset(int)));
    layout->addWidget(p_Presets, 0,1);
    layout->addWidget(createPreset, 0,2);
    presetBox->setLayout(layout);

    layout = new QGridLayout;
    m_InBox_Ptr = new QGroupBox("Colour-space Mixer");
    layout->addWidget(m_LabelA_Ptr, 0,1, Qt::AlignHCenter);
    layout->addWidget(m_LabelB_Ptr, 0,2, Qt::AlignHCenter);
    layout->addWidget(m_LabelC_Ptr, 0,3, Qt::AlignHCenter);
    layout->addWidget(new QLabel("Source Image:"));
    layout->addWidget(m_BoxImgA_Ptr, 1,1);
    layout->addWidget(m_BoxImgB_Ptr, 1,2);
    layout->addWidget(m_BoxImgC_Ptr, 1,3);
    layout->addWidget(new QLabel("Colour Component:"));
    layout->addWidget(m_BoxSpaceA_Ptr, 2,1);
    layout->addWidget(m_BoxSpaceB_Ptr, 2,2);
    layout->addWidget(m_BoxSpaceC_Ptr, 2,3);
    m_InBox_Ptr->setLayout(layout);

    p_LoadBar = new QProgressBar;
    p_LoadBar->setVisible(false);

    connect(this, SIGNAL(LoadStart()), this, SLOT(BarStart()));
    connect(this, SIGNAL(LoadUpdate(int)), this, SLOT(BarUpdate(int)));
    connect(this, SIGNAL(LoadComplete()), this, SLOT(BarComplete()));
    QPushButton *saveButton = new QPushButton("Save As");
    connect(saveButton, SIGNAL(clicked()), this, SLOT(Render()));
    QPushButton *dropButton = new QPushButton("Drop to Pool");
    connect(dropButton, SIGNAL(clicked()), this, SLOT(Drop()));

    m_MainLabel_Ptr = new QLabel;
    m_MainLabel_Ptr->setPixmap( QPixmap::fromImage(m_MixedImg) );
    m_Layout_Ptr->addWidget(m_MainLabel_Ptr, 0,0,1,4,Qt::AlignHCenter);
    m_Layout_Ptr->addWidget(outBox, 1,0, 1,2, Qt::AlignHCenter);
    m_Layout_Ptr->addWidget(presetBox, 1,2, 1,2, Qt::AlignHCenter);
    m_Layout_Ptr->addWidget(m_InBox_Ptr, 2,0, 1,4, Qt::AlignHCenter);
    m_Layout_Ptr->addWidget(dropButton, 3,0);
    m_Layout_Ptr->addWidget(p_LoadBar, 3,1, 1,2);
    m_Layout_Ptr->addWidget(saveButton, 3,3);
    setLayout(m_Layout_Ptr);
}


QImage LuminanceMapper::GetChannel(QString p_Channel, WLPFlag p_PreviewState = WLP_PREVIEW_ON)
{
    QImage temp;// = QImage(imgWidth, imgHeight, mixedImg.format());

    QString type = p_Channel;

    int index = IndexFromString(type);
    if(index == -1)
    {
        int genIndex = 0;
        std::vector<QString>::iterator iter = p_Pool->m_GenTexts.begin();
        for(; iter != p_Pool->m_GenTexts.end() && type != p_Pool->m_GenTexts[genIndex]; ++iter)
            ++genIndex;

        if(p_PreviewState==WLP_PREVIEW_ON)
            temp = p_Pool->GetImgGeneral(genIndex)->scaled(s_ViewWidth, s_ViewHeight, Qt::KeepAspectRatio);
        else
            temp = *p_Pool->GetImgGeneral(genIndex);
    }
    else
    {
        //Set the image to the type it is:
        if(p_PreviewState==WLP_PREVIEW_ON)
            temp = m_ImgScaled[index];
        else
            temp = m_MasterImgs_Ptr[index];
    }

    m_Width = temp.width();
    m_Height = temp.height();

    return temp;
}

void LuminanceMapper::Update(WLPFlag p_UpdateState, WLPFlag p_PreviewState)
{
    if(m_ActivePreset)
        return;

    if(p_UpdateState != WLP_UPDATE_ALL && p_PreviewState == WLP_PREVIEW_ON)
        p_Presets->setCurrentIndex(0);

    QImage srcImg;
    QImage srcFromA = GetChannel(m_BoxImgA_Ptr->currentText(), p_PreviewState);
    QImage srcFromB = GetChannel(m_BoxImgB_Ptr->currentText(), p_PreviewState);
    QImage srcFromC = GetChannel(m_BoxImgC_Ptr->currentText(), p_PreviewState);

    qreal R,G,B;
    emit LoadStart();
    QImage img(m_Width, m_Height, QImage::Format_RGB32);
    for (int x = 0; x < m_Width; ++x) {
         for (int y = 0; y < m_Height; ++y) {

             if(p_UpdateState != WLP_UPDATE_ALL)
             {
                 R = QColor(m_MixedImg.pixel(x,y)).redF();
                 G = QColor(m_MixedImg.pixel(x,y)).greenF();
                 B = QColor(m_MixedImg.pixel(x,y)).blueF();
             }
             QString mode; int val;
             //Convert only the element that has changed
             if(p_UpdateState == WLP_UPDATE_A || p_UpdateState == WLP_UPDATE_BASE || p_UpdateState == WLP_UPDATE_ALL)
             {
                srcImg = srcFromA;
                if(m_BoxSpaceA_Ptr->currentIndex() < 68)
                {
                    val = m_BoxSpaceA_Ptr->currentIndex() % 4; //3 elements + a seperator
                    mode = QString::number(val) + m_BoxSpaceA_Ptr->currentText();
                }
                else
                {
                    val = 50 + (m_BoxSpaceA_Ptr->currentIndex()-68) % 5; //4 elements + a seperator
                    mode = QString::number(val) + m_BoxSpaceA_Ptr->currentText();
                }
                R = ConvertPixel(srcImg.pixel(x,y), mode);
             }

             if(p_UpdateState == WLP_UPDATE_B || p_UpdateState == WLP_UPDATE_BASE || p_UpdateState == WLP_UPDATE_ALL)
             {
                srcImg = srcFromB;
                if(m_BoxSpaceB_Ptr->currentIndex()  < 68)
                {
                    val = m_BoxSpaceB_Ptr->currentIndex() % 4; //3 elements + a seperator
                    mode = QString::number(val) + m_BoxSpaceB_Ptr->currentText();
                }
                else
                {
                    val = 50 + (m_BoxSpaceB_Ptr->currentIndex()-68) % 5; //4 elements + a seperator
                    mode = QString::number(val) + m_BoxSpaceB_Ptr->currentText();
                }
                G = ConvertPixel(srcImg.pixel(x,y), mode);
             }

             if(p_UpdateState == WLP_UPDATE_C || p_UpdateState == WLP_UPDATE_BASE || p_UpdateState == WLP_UPDATE_ALL)
             {
                srcImg = srcFromC;
                if(m_BoxSpaceC_Ptr->currentIndex()  < 68)
                {
                    val = m_BoxSpaceC_Ptr->currentIndex() % 4; //3 elements + a seperator
                    mode = QString::number(val) + m_BoxSpaceC_Ptr->currentText();
                }
                else
                {
                    val = 50 + (m_BoxSpaceC_Ptr->currentIndex()-68) % 5; //4 elements + a seperator
                    mode = QString::number(val) + m_BoxSpaceC_Ptr->currentText();
                }
                B = ConvertPixel(srcImg.pixel(x,y), mode);
             }

             //Update image history on preview only
             if(p_PreviewState == WLP_PREVIEW_ON)
             {
                m_MixedImg.setPixel(x, y, QColor::fromRgbF(R,G,B).rgb());
             }

             //Update screen image completely
            QRgb col = GetRGBFromBase(QColor::fromRgbF(R,G,B), m_Base_Ptr->currentText()).rgb();
            img.setPixel(x, y, col);
         }

         int progress = ((static_cast<qreal>(x) / static_cast<qreal>(m_Width)) * 100.0);
         emit LoadUpdate(progress);
       }

    emit LoadComplete();

    if(p_PreviewState == WLP_PREVIEW_OFF)
    {
        m_SaveImg = img;

    }

    m_MainLabel_Ptr->setPixmap(QPixmap::fromImage(img));
}

void LuminanceMapper::Render()
{
    Update(WLP_UPDATE_ALL, WLP_PREVIEW_OFF);

    //Open Save As dialog
    QString newName = QFileDialog::getSaveFileName(this, tr("Save image"),
                                                   g_Config.Get(WLPC_DIR_SAVE), tr("JPG (*.jpg) ;; PNG (*.png) ;; TIF (*.tif)") );
    //Save to new path/name
    m_SaveImg.save(newName,NULL,100);

    //Set that path as new save path
    QFileInfo bloo(newName);
    WLPSettings s; s.UpdateSaveDir(bloo.absolutePath());
}

void LuminanceMapper::Drop()
{
    Update(WLP_UPDATE_ALL, WLP_PREVIEW_OFF);
    m_MasterImgs_Ptr[WLP_POOL_MIX_COL] = m_SaveImg;
    p_Pool->Drop("Colour-Space");
}

//=============================================

//Change the layout to accomidate different colour-space modes
void LuminanceMapper::UpdateBase(QString str)
{
    if(str == "RGB")
    {
        m_LabelA_Ptr->setText("Red");
        m_LabelB_Ptr->setText("Green");
        m_LabelC_Ptr->setText("Blue");
    }
    else if(str == "CMY")
    {
        m_LabelA_Ptr->setText("Cyan");
        m_LabelB_Ptr->setText("Magenta");
        m_LabelC_Ptr->setText("Yellow");
    }
    else if(str == "HSL")
    {
        m_LabelA_Ptr->setText("Hue");
        m_LabelB_Ptr->setText("Saturation");
        m_LabelC_Ptr->setText("Luminance");
    }
    else if(str == "HSI")
    {
        m_LabelA_Ptr->setText("Hue");
        m_LabelB_Ptr->setText("Saturation");
        m_LabelC_Ptr->setText("Intensity");
    }
    else if(str == "HSV")
    {
        m_LabelA_Ptr->setText("Hue");
        m_LabelB_Ptr->setText("Saturation");
        m_LabelC_Ptr->setText("Value");
    }
    else if(str == "LMS")
    {
        m_LabelA_Ptr->setText("Long");
        m_LabelB_Ptr->setText("Medium");
        m_LabelC_Ptr->setText("Short");
    }
    else if(str == "rgI")
    {
        m_LabelA_Ptr->setText("Red Chromo");
        m_LabelB_Ptr->setText("Green Chromo");
        m_LabelC_Ptr->setText("Pixel Intensity");
    }
    else if(str == "rgG")
    {
        m_LabelA_Ptr->setText("Red Chromo");
        m_LabelB_Ptr->setText("Green Chromo");
        m_LabelC_Ptr->setText("Green (RGB)");
    }
    else if(str == "CIE XYZ")
    {
        m_LabelA_Ptr->setText("X");
        m_LabelB_Ptr->setText("Y");
        m_LabelC_Ptr->setText("Z");
    }
    else if(str == "CIE Lab")
    {
        m_LabelA_Ptr->setText("Luminance");
        m_LabelB_Ptr->setText("a* (cyan/magenta)");
        m_LabelC_Ptr->setText("b* (blue/yellow)");
    }
    else if(str == "CIE Luv")
    {
        m_LabelA_Ptr->setText("Luminance");
        m_LabelB_Ptr->setText("u*");
        m_LabelC_Ptr->setText("v*");
    }
    else if(str == "YUV (PAL TVs)")
    {
        m_LabelA_Ptr->setText("Luma - Y'");
        m_LabelB_Ptr->setText("Chroma - U");
        m_LabelC_Ptr->setText("Chroma - V");
    }
    else if(str == "YIQ (NTSC TVs)")
    {
        m_LabelA_Ptr->setText("Luma - Y'");
        m_LabelB_Ptr->setText("Chroma - I");
        m_LabelC_Ptr->setText("Chroma - Q");
    }
    else if(str == "CIE LCHab")
    {
        m_LabelA_Ptr->setText("Luminance");
        m_LabelB_Ptr->setText("Chroma");
        m_LabelC_Ptr->setText("Hue");
    }
    else if(str == "CIE LCHuv")
    {
        m_LabelA_Ptr->setText("Luminance");
        m_LabelB_Ptr->setText("Chroma");
        m_LabelC_Ptr->setText("Hue");
    }
    else if(str == "CIE UVW")
    {
        m_LabelA_Ptr->setText("U");
        m_LabelB_Ptr->setText("V");
        m_LabelC_Ptr->setText("W");
    }
    else if(str == "CIE LMS")
    {
        m_LabelA_Ptr->setText("Long");
        m_LabelB_Ptr->setText("Medium");
        m_LabelC_Ptr->setText("Short");
    }
    else{
        m_LabelA_Ptr->setText("Err");
        m_LabelB_Ptr->setText("Err");
        m_LabelC_Ptr->setText("Err");
    }

    //Call base-only update
    Update(WLP_UPDATE_BASE, WLP_PREVIEW_ON);
}


qreal LuminanceMapper::ConvertPixel(QColor p_Col, QString p_Mode)
{
    qreal outCol;
    outCol=0.5;
    if(p_Mode.contains("RGB ") )
    {
        QColor temp = p_Col;
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("CMY ") )
    {
        QColor temp = RGBtoCMY(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("HSL"))
    {
        QColor temp = RGBtoHSL(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("HSV"))
    {
        QColor temp = RGBtoHSV(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("HSI"))
    {
        QColor temp = RGBtoHSI(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("LMS"))
    {
        QColor temp = RGBtoXYZ(p_Col);
        temp = XYZtoLMS(temp);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("XYZ"))
    {
        QColor temp = RGBtoXYZ(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("H-Lab"))
    {
        QColor temp = RGBtoXYZ(p_Col);
        temp = XYZtoHLAB(temp);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("Lab"))
    {
        QColor temp = RGBtoXYZ(p_Col);
        temp = XYZtoLab(temp);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("Luv"))
    {
        QColor temp = RGBtoXYZ(p_Col);
        temp = XYZtoLuv(temp);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("UVW"))
    {
        QColor temp = RGBtoXYZ(p_Col);
        temp = XYZtoUVW(temp);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("YIQ"))
    {
        QColor temp = RGBtoYIQ(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("YUV"))
    {
        QColor temp = RGBtoYUV(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("rg "))
    {
        QColor temp = RGBtoRGC(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("cm "))
    {
        QColor temp = RGBtoRGC(RGBtoCMY(p_Col));
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("LCHab"))
    {
        QColor temp = RGBtoXYZ(p_Col);
        temp = XYZtoLab(temp);
        temp = LabtoLCH(temp);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("LCHuv"))
    {
        QColor temp = RGBtoXYZ(p_Col);
        temp = XYZtoLuv(temp);
        temp = LabtoLCH(temp); //Same conversion for ab/uv
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("CMYK"))
    {
        QColor temp = RGBtoCMYK(p_Col);
        outCol = GetElement(temp, p_Mode);
    }
    else if(p_Mode.contains("RGBW"))
    {
        QColor temp = RGBtoHSI(p_Col);
        temp = HSItoRGBA(temp);
        outCol = GetElement(temp, p_Mode);
    }

    return outCol;
}

qreal LuminanceMapper::GetElement(QColor p_Col, QString p_Space)
{
    if(p_Space[0] == '0')
        return p_Col.redF();
    if(p_Space[0] == '1')
        return p_Col.greenF();
    if(p_Space[0] == '2')
        return p_Col.blueF();

    //4-Element spaces
    if(p_Space[0] == '5')
    {
        if(p_Space[1] == '0')
            return p_Col.cyanF();
        if(p_Space[1] == '1')
            return p_Col.magentaF();
        if(p_Space[1] == '2')
            return p_Col.yellowF();
        if(p_Space[1] == '3')
            return p_Col.blackF();
    }

    return 0.5;
}

QColor LuminanceMapper::GetRGBFromBase(QColor p_Col, QString p_Base)
{
    if(p_Base == "RGB")
        return p_Col;
    else if(p_Base == "CMY")
        return RGBtoCMY(p_Col);
    else if(p_Base == "HSL")
        return QColor::fromHslF(p_Col.redF(), p_Col.greenF(), p_Col.blueF());
    else if(p_Base == "HSV")
        return QColor::fromHsvF(p_Col.redF(), p_Col.greenF(), p_Col.blueF());
    else if(p_Base == "HSI")
        return HSItoRGB(p_Col);
    else if(p_Base == "CIE XYZ")
        return XYZtoRGB(p_Col);
    else if(p_Base == "YIQ (NTSC TVs)")
        return YIQtoRGB(p_Col);
    else if(p_Base == "YUV (PAL TVs)")
        return YUVtoRGB(p_Col);
    else if(p_Base == "rgI")
    {
        qreal intensity = p_Col.blueF() * 3;
        qreal red = qBound<qreal>(0, p_Col.redF() * intensity, 1);
        qreal green = qBound<qreal>(0, p_Col.greenF() * intensity, 1);
        qreal blue = qBound<qreal>(0, (1 - p_Col.redF() - p_Col.greenF()) * intensity, 1);
        return QColor::fromRgbF(red,green,blue);
    }
    else if(p_Base == "rgG")
    {
        qreal x = p_Col.redF();
        qreal y = p_Col.greenF();
        qreal Y = p_Col.blueF();

        qreal X = qBound<qreal>(0, (x * Y) / y, 1);
        qreal Z = qBound<qreal>(0, ((1 - x - y) * Y)/y, 1);

        return QColor::fromRgbF(X,Y,Z);
    }
    else if(p_Base == "CIE Lab")
    {
        QColor temp = LabtoXYZ(p_Col);
        return XYZtoRGB(temp);
    }
    else if(p_Base == "CIE Luv")
    {
        QColor temp = LuvtoXYZ(p_Col);
        return XYZtoRGB(temp);
    }
    else if(p_Base == "CIE UVW")
    {
        QColor temp = UVWtoXYZ(p_Col);
        return XYZtoRGB(temp);
    }
    else if(p_Base == "CIE LMS")
    {
        QColor temp = LMStoXYZ(p_Col);
        return XYZtoRGB(temp);
    }
    else if(p_Base == "Hunter-Lab")
    {
        QColor temp = HLABtoXYZ(p_Col);
        temp = XYZtoRGB(temp);
        return temp;
    }
    else if(p_Base == "CIE LCHab")
    {
        QColor temp = LCHtoLab(p_Col);
        temp = LabtoXYZ(temp);
        temp = XYZtoRGB(temp);
        return temp;
    }
    else if(p_Base == "CIE LCHuv")
    {
        QColor temp = LCHtoLab(p_Col);
        temp = LuvtoXYZ(temp);
        temp = XYZtoRGB(temp);
        return temp;
    }
    else{}

    return QColor::fromRgbF(1,0,0);
}


//====

void LuminanceMapper::EditSettings()
{
    //Edit widget
    QWidget *temp = new QWidget(0);
    QLabel *workSpace = new QLabel("RGB Working-Space:");
    QLabel *illumanant = new QLabel("Illumanant:");
    QLabel *gamma = new QLabel("Gamma value:");
    QSpinBox *dropCount = new QSpinBox; //SetDropMax(int
    dropCount->setMaximum(80); dropCount->setValue( g_Config.Get(WLPC_DROP_MAX) );
    connect(dropCount, SIGNAL(valueChanged(int)), this, SLOT(SetDropMax(int)));
    QPushButton *save = new QPushButton("Save Settings");
    QPushButton *cancel = new QPushButton("Cancel");

    QGridLayout *lay = new QGridLayout;
    lay->addWidget(workSpace, 0,0);
    lay->addWidget(illumanant, 1,0);
    lay->addWidget(gamma, 2,0);
    lay->addWidget(p_RgbSpace, 0,1);
    lay->addWidget(p_RgbWhite, 1,1);
    lay->addWidget(p_RgbGamma, 2,1);
    lay->addWidget(new QLabel("Max list items: "), 3,0);
    lay->addWidget(dropCount, 3,1);
    lay->addWidget(save, 4,0);
    lay->addWidget(cancel,4,1);
    temp->setLayout(lay);
    temp->show();
    connect(save, SIGNAL(clicked()), this, SLOT(SaveSettings()));
    connect(save, SIGNAL(clicked()), temp, SLOT(close()));
    connect(cancel, SIGNAL(clicked()), temp, SLOT(close()));
}

void LuminanceMapper::SetDropMax(int count)
{
    m_BoxSpaceA_Ptr->setMaxVisibleItems(count);
    m_BoxSpaceB_Ptr->setMaxVisibleItems(count);
    m_BoxSpaceC_Ptr->setMaxVisibleItems(count);
    m_Base_Ptr->setMaxVisibleItems(count);
    p_Presets->setMaxVisibleItems(count);

    //Update the global settings
    g_Config.Set(WLPC_DROP_MAX, count);
}

void LuminanceMapper::SaveSettings()
{
    int flagOffset = static_cast<int>(WLP_WORKSPACE_OFFSET);
    WLPFlag rgbFlag = static_cast<WLPFlag>(p_RgbSpace->currentIndex() + flagOffset);

    ChangeRGB(rgbFlag);
    ChangeWhite(p_RgbWhite->currentIndex());

    switch(p_RgbGamma->currentIndex())
    {
    case 0: //1.0
        m_GammaFlag = WLP_GAMMA_VALUE;
        m_Gamma = 1.0;
        break;
    case 1: //1.8
        m_GammaFlag = WLP_GAMMA_VALUE;
        m_Gamma = 1.8;
        break;
    case 2: //2.2
        m_GammaFlag = WLP_GAMMA_VALUE;
        m_Gamma = 2 + (51.0/256.0);
        break;
    case 3: //sRGB
        m_GammaFlag = WLP_GAMMA_SRGB;
        break;
    case 4: //L*
        m_GammaFlag = WLP_GAMMA_L;
        break;
    }
}

void LuminanceMapper::SetPreset(int _index)
{
    if( _index == 0 )
        return;

    //WLPMode preFlag = static_cast<WLPMode>(_index);
    m_ActivePreset = true;

    //Standard presets
    if( _index < 16 )
    {
        int element = (_index-1) * 4;
        m_BoxSpaceA_Ptr->setCurrentIndex(element);
        m_BoxSpaceB_Ptr->setCurrentIndex(element+1);
        m_BoxSpaceC_Ptr->setCurrentIndex(element+2);
        m_Base_Ptr->setCurrentIndex(_index-1);
    }
    else //rgG (16), rgI (17) & all Custom presets:
    {
        int listIndex = _index - 16;
        m_BoxSpaceA_Ptr->setCurrentIndex(m_CustomSets[listIndex].A);
        m_BoxSpaceB_Ptr->setCurrentIndex(m_CustomSets[listIndex].B);
        m_BoxSpaceC_Ptr->setCurrentIndex(m_CustomSets[listIndex].C);
        m_Base_Ptr->setCurrentIndex(m_CustomSets[listIndex].base);
    }

    m_ActivePreset = false;
    Update(WLP_UPDATE_ALL, WLP_PREVIEW_ON);
}

void LuminanceMapper::CreatePreset()
{
    //Edit widget
    QWidget *temp = new QWidget(0);
    QPushButton *save = new QPushButton("Add Custom Preset");
    QPushButton *cancel = new QPushButton("Cancel");

    QGridLayout *lay = new QGridLayout;
    lay->addWidget(new QLabel("Name:"),0,0);
    lay->addWidget(p_PreName, 0,1);
    lay->addWidget(new QLabel("Base:"),1,0);
    lay->addWidget(p_PreBase, 1,1);
    lay->addWidget(new QLabel("Channel A:"),2,0);
    lay->addWidget(p_PreChanA, 2,1);
    lay->addWidget(new QLabel("Channel B:"),3,0);
    lay->addWidget(p_PreChanB, 3,1);
    lay->addWidget(new QLabel("Channel C:"),4,0);
    lay->addWidget(p_PreChanC, 4,1);
    lay->addWidget(save, 5,0);
    lay->addWidget(cancel,5,1);
    temp->setLayout(lay);
    temp->show();

    connect(save, SIGNAL(clicked()), temp, SLOT(close()));
    connect(save, SIGNAL(clicked()), this, SLOT(AddPreset()));
    connect(cancel, SIGNAL(clicked()), temp, SLOT(close()));
}

void LuminanceMapper::AddPreset()
{
    //Load into vector of structs
    int base = p_PreBase->currentIndex();
    int A = p_PreChanA->currentIndex();
    int B = p_PreChanB->currentIndex();
    int C = p_PreChanC->currentIndex();
    CustomSet nextSet(base, A, B, C);

    //Add the name into the combobox and
    //the struct into the list
    p_Presets->addItem(p_PreName->text());
    m_CustomSets.push_back(nextSet);
}
