#ifndef IMAGEEXPLORER_H
#define IMAGEEXPLORER_H

#include <QLabel>
#include <QRubberBand>
#include <QMouseEvent>
#include <QPixmap>
#include <QResizeEvent>
#include <QPainter>
#include <QActionGroup>

namespace ImgExp {
    /*!
     *  The Features enum
     *        Contains the states of control
     *        a user can have on ImageExplorer
     */
    enum Features {
        INVALID    = 0,
        BOXSELECT  = 1 << 0,
        SCROLLZOOM = 1 << 1,
        DRAG       = 1 << 2,
        IMAGECLICK = 1 << 3,
        IMAGEDOUBLECLICK = 1 << 4
    };
}

/*!
 * \brief The ImageExplorer class
 *        An image label class for general image controls:
 *
 *            - signal for image clicked/double clicked with [x,y] point
 *            - signal for selection made with box selection
 *            - dragging (hold down moving of image)
 *            - zooming (scrolling middle mouse)
 *
 *        All points returned are in original image co-ordinates.
 */
class ImageExplorer : public QLabel
{
    Q_OBJECT
public:
    explicit ImageExplorer(QWidget *parent = 0);

    QString getFilePath() { return m_imageFileName; }
    void setFilePath(QString _filePath) { m_imageFileName = _filePath; }
    void setImage(QImage _newImg, bool keepTranslation);
    void updateDisplay(bool fromFull = false);
    void setFeatures(ImgExp::Features _feat) { m_features = _feat; }
    ImgExp::Features getFeatures() { return m_features; }
    void resetTranslation();
    void applyTransformation();

    void setID(int _id) { m_id = _id; }
    void setTransformType(Qt::TransformationMode _type) { m_transformType = _type; }
    void setActiveTranslations(bool _moveOn = true) { m_movezoom = _moveOn; }

    void setTranslation(QRect _box);
    void setTranslation(QPoint _start, bool _zoomIn);

    // For updating with overlay
    QImage getDisplayImage() { return m_displayImg; }
    QImage getOriginalImage() { return m_fullsizeImg; }
    void setDisplayImage(QImage _img) { m_displayImg = _img; updateDisplay(); }
    float getWidthScaleFactor() { return (float)m_displayImg.width() / (float)m_fullsizeImg.width(); }
    float getHeightScaleFactor() { return (float)m_displayImg.height() / (float)m_fullsizeImg.height(); }
    float getZoomFactor() { return m_zoomScale; }
    QPoint getOffset() { return QPoint(m_offsetX, m_offsetY); }
    QPoint getCenter() { return QPoint(m_centerX, m_centerY); }
    void setDisplayImageFromFull(QImage _newFull);
    QString internalText();
    void copyTransformation(int _centerX, int _centerY, float _zoomScale);

    QPoint originalPos(QMouseEvent* _event);
    void addAlternateImage(QString _imgPath, QString _name, bool _checked);
    void clearActionGroup() {
        delete p_actionGroup;
        p_actionGroup = new QActionGroup(this);
    }

protected:
    void wheelEvent(QWheelEvent *event);            // Zooming
    void mousePressEvent(QMouseEvent* event);       // click or start selection or drag
    void mouseReleaseEvent(QMouseEvent* event);     // finish selection
    void mouseMoveEvent(QMouseEvent* event);        // selection, drag
    void mouseDoubleClickEvent(QMouseEvent* event); // double click

    // Scaling while keeping aspect ratio
    void resizeEvent(QResizeEvent * event);

signals:
    void imageBoxZoomed(int, QRect);
    void imageDoubleClicked(int, QPoint);
    void imageClicked(int _id, QPoint);
    void imageScrolled(int, float);
    void imageDragged(int, QPoint);
    void imageReleased(int, QPoint);
    void altImageUsed(int, QString);
    void imageRightClicked(int x, int y, float z);

public slots:
    void showContextMenu(const QPoint &);
    void useAltImage(QAction*);
    void openLocalFolder();
    void openNatively();

private:
    QString m_imageFileName;
    ImgExp::Features m_features;
    QVector<QString> m_alternateImgs;
    QImage m_fullsizeImg;
    QImage m_displayImg;

    QActionGroup *p_actionGroup;
    QVector<QAction*> m_altActions;

    QRubberBand *m_rubberBand;
    QRect m_lastSelection;
    QPoint m_pointOrigin;

    int m_currentWidth; // only changed in resize events
    int m_currentHeight; // only changed in resize events

    float m_zoomScale;
    bool m_movezoom;
    int m_centerX;
    int m_centerY;
    int m_lastX;
    int m_lastY;

    int m_offsetX;
    int m_offsetY;

    int m_id;
    Qt::TransformationMode m_transformType;
};

#endif // IMAGEEXPLORER_H
