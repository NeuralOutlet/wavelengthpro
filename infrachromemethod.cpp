#include "infrachromemethod.h"
#include <QPushButton>
#include <QRadioButton>
#include <QMessageBox>
#include <fstream>

#include "wlpsettings.h"

extern WLPConfig g_Config;

InfrachromeMethod::InfrachromeMethod(WLPPool *_pool, QWidget *p_Parent_Ptr) :
    QWidget(p_Parent_Ptr)
{
    setWindowTitle("Digital EIR Emulator");
    p_Pool = _pool;

    m_MainLabel_Ptr = new QLabel;
    m_Img_Ptr = p_Pool->GetImg("FULL (350-1000nm)");
    m_SmallImg = m_Img_Ptr->scaled(s_ViewWidth, s_ViewHeight,Qt::KeepAspectRatio);
    m_DisplayImg = m_SmallImg;

    QGridLayout *blah = new QGridLayout;

    //Set up Fractional sliders
    for(int i = 0; i < 2; ++i)
    {
        //frac
        m_AllSlides[i][0].setOrientation(Qt::Horizontal);
        m_AllSlides[i][0].setRange(0,1000);
        m_AllSpins[i][0].setRange(0,1000);
        connect(&m_AllSlides[i][0], SIGNAL(valueChanged(int)), &m_AllSpins[i][0], SLOT(setValue(int)));
        connect(&m_AllSpins[i][0], SIGNAL(valueChanged(int)), &m_AllSlides[i][0], SLOT(setValue(int)));
        connect(&m_AllSlides[i][0], SIGNAL(valueChanged(int)), this, SLOT(Update()));

        //level
        m_AllSlides[i][1].setOrientation(Qt::Horizontal);
        m_AllSlides[i][1].setRange(2,4000);
        m_AllSpins[i][1].setRange(2,4000);
        connect(&m_AllSlides[i][1], SIGNAL(valueChanged(int)), &m_AllSpins[i][1], SLOT(setValue(int)));
        connect(&m_AllSpins[i][1], SIGNAL(valueChanged(int)), &m_AllSlides[i][1], SLOT(setValue(int)));
        connect(&m_AllSlides[i][1], SIGNAL(valueChanged(int)), this, SLOT(Update()));
    }
    //Set config values
    m_AllSlides[0][0].setValue(g_Config.Get(WLPC_FRAC_RX)/*605*/);     m_AllSlides[0][1].setValue(g_Config.Get(WLPC_LEVEL_R)/*835*/);
    m_AllSlides[1][0].setValue(g_Config.Get(WLPC_FRAC_GX)/*820*/);     m_AllSlides[1][1].setValue(g_Config.Get(WLPC_LEVEL_G)/*1150*/);

    //Set up Gamma Sliders
    for(int i = 2; i < 5; ++i)
    {
        //Vis
        m_AllSlides[i][0].setOrientation(Qt::Horizontal);
        m_AllSlides[i][0].setRange(1,3000);
        m_AllSpins[i][0].setRange(1,3000);
        connect(&m_AllSlides[i][0], SIGNAL(valueChanged(int)), &m_AllSpins[i][0], SLOT(setValue(int)));
        connect(&m_AllSpins[i][0], SIGNAL(valueChanged(int)), &m_AllSlides[i][0], SLOT(setValue(int)));
        connect(&m_AllSlides[i][0], SIGNAL(valueChanged(int)), this, SLOT(Update()));

        //IR Correction
        m_AllSlides[i][1].setOrientation(Qt::Horizontal);
        m_AllSlides[i][1].setRange(1,3000);
        m_AllSpins[i][1].setRange(1,3000);
        connect(&m_AllSlides[i][1], SIGNAL(valueChanged(int)), &m_AllSpins[i][1], SLOT(setValue(int)));
        connect(&m_AllSpins[i][1], SIGNAL(valueChanged(int)), &m_AllSlides[i][1], SLOT(setValue(int)));
        connect(&m_AllSlides[i][1], SIGNAL(valueChanged(int)), this, SLOT(Update()));
    }
    //Set config values
    m_AllSlides[2][0].setValue(g_Config.Get(WLPC_GAMMA_R_VIS)/*1150*/);    m_AllSlides[2][1].setValue(g_Config.Get(WLPC_GAMMA_R_IRCORR)/*1000*/);
    m_AllSlides[3][0].setValue(g_Config.Get(WLPC_GAMMA_G_VIS)/*1268*/);    m_AllSlides[3][1].setValue(g_Config.Get(WLPC_GAMMA_G_IRCORR)/*1000*/);
    m_AllSlides[4][0].setValue(g_Config.Get(WLPC_MASTER_LEVEL)/*955*/);    m_AllSlides[4][1].setValue(g_Config.Get(WLPC_MASTER_GAMMA)/*1071*/);

    //Set up GUI
    QGroupBox *boxA = new QGroupBox(tr("R->G Channel (red fraction)"));
    QGridLayout *layA = new QGridLayout;
    layA->addWidget(new QLabel("frac"), 0,0);
    layA->addWidget(&m_AllSlides[0][0], 0,1);
    layA->addWidget(&m_AllSpins[0][0], 0,2);
    layA->addWidget(new QLabel("level"), 1,0);
    layA->addWidget(&m_AllSlides[0][1], 1,1);
    layA->addWidget(&m_AllSpins[0][1], 1,2);
    boxA->setLayout(layA);

    QGroupBox *boxB = new QGroupBox(tr("G->B Channel (green fraction)"));
    QGridLayout *layB = new QGridLayout;
    layB->addWidget(new QLabel("frac"), 0,0);
    layB->addWidget(&m_AllSlides[1][0], 0,1);
    layB->addWidget(&m_AllSpins[1][0], 0,2);
    layB->addWidget(new QLabel("level"), 1,0);
    layB->addWidget(&m_AllSlides[1][1], 1,1);
    layB->addWidget(&m_AllSpins[1][1], 1,2);
    boxB->setLayout(layB);

    QGroupBox *boxC = new QGroupBox(tr("R->G Channel (red gamma)"));
    QGridLayout *layC = new QGridLayout;
    layC->addWidget(new QLabel("red"), 0,0);
    layC->addWidget(&m_AllSlides[2][0], 0,1);
    layC->addWidget(&m_AllSpins[2][0], 0,2);
    layC->addWidget(new QLabel("IR Corr."), 1,0);
    layC->addWidget(&m_AllSlides[2][1], 1,1);
    layC->addWidget(&m_AllSpins[2][1], 1,2);
    boxC->setLayout(layC);

    QGroupBox *boxD = new QGroupBox(tr("G->B Channel (green gamma)"));
    QGridLayout *layD = new QGridLayout;
    layD->addWidget(new QLabel("green"), 0,0);
    layD->addWidget(&m_AllSlides[3][0], 0,1);
    layD->addWidget(&m_AllSpins[3][0], 0,2);
    layD->addWidget(new QLabel("IR corr."), 1,0);
    layD->addWidget(&m_AllSlides[3][1], 1,1);
    layD->addWidget(&m_AllSpins[3][1], 1,2);
    boxD->setLayout(layD);

    QGroupBox *boxE = new QGroupBox(tr("I->R Channel (blue gamma)"));
    QGridLayout *layE = new QGridLayout;
    layE->addWidget(new QLabel("level"), 0,0);
    layE->addWidget(&m_AllSlides[4][0], 0,1);
    layE->addWidget(&m_AllSpins[4][0], 0,2);
    layE->addWidget(new QLabel("gamma master"), 1,0);
    layE->addWidget(&m_AllSlides[4][1], 1,1);
    layE->addWidget(&m_AllSpins[4][1], 1,2);
    boxE->setLayout(layE);

    QPushButton *final = new QPushButton("Save As");
    connect(final, SIGNAL(clicked()), this, SLOT(Render()));

    QPushButton *drop = new QPushButton("Drop to Pool");
    connect(drop, SIGNAL(clicked()), this, SLOT(Drop()));

    QPushButton *saveSet = new QPushButton("Save Settings");
    connect(saveSet, SIGNAL(clicked(bool)), this, SLOT(SaveSettings(bool)));

    QPushButton *saveFile = new QPushButton("Save Settings to File");
    connect(saveFile, SIGNAL(clicked()), this, SLOT(SaveToFile()));

    QPushButton *reSet = new QPushButton("Reset Settings");
    connect(reSet, SIGNAL(clicked()), this, SLOT(RestoreSettings()));

    QPushButton *loadSet = new QPushButton("Load Settings");
    connect(loadSet, SIGNAL(clicked()), this, SLOT(LoadSettings()));

    m_ChanState = WLP_PREVIEW_RGB;
    QGroupBox *other = new QGroupBox;
    QGridLayout *layOther = new QGridLayout;
    QRadioButton *radio1 = new QRadioButton(tr("RGB"));
    connect(radio1, SIGNAL(clicked()), this, SLOT(SetRGB()));
    connect(radio1, SIGNAL(clicked()), this, SLOT(Update()));
    QRadioButton *radio2 = new QRadioButton(tr("Red"));
    connect(radio2, SIGNAL(clicked()), this, SLOT(SetRed()));
    connect(radio2, SIGNAL(clicked()), this, SLOT(Update()));
    QRadioButton *radio3 = new QRadioButton(tr("Green"));
    connect(radio3, SIGNAL(clicked()), this, SLOT(SetGreen()));
    connect(radio3, SIGNAL(clicked()), this, SLOT(Update()));
    QRadioButton *radio4 = new QRadioButton(tr("Blue"));
    connect(radio4, SIGNAL(clicked()), this, SLOT(SetBlue()));
    connect(radio4, SIGNAL(clicked()), this, SLOT(Update()));

    radio1->setChecked(true);
    layOther->addWidget(final, 0,0, 1,2);
    layOther->addWidget(drop, 0,2, 1,2);
    layOther->addWidget(radio1, 1,0);
    layOther->addWidget(radio2, 1,1);
    layOther->addWidget(radio3, 1,2);
    layOther->addWidget(radio4, 1,3);
    other->setLayout(layOther);

    blah->addWidget(m_MainLabel_Ptr, 0,0,5,1);
    blah->addWidget(boxA, 0,1,1,2);
    blah->addWidget(boxB, 1,1,1,2);
    blah->addWidget(boxC, 2,1,1,2);
    blah->addWidget(boxD, 3,1,1,2);
    blah->addWidget(boxE, 4,1,1,2);
    blah->addWidget(saveSet, 5,1);
    blah->addWidget(reSet, 6,2);
    blah->addWidget(loadSet, 6,1);
    blah->addWidget(saveFile, 5,2);
    blah->addWidget(other, 5,0,2,1);
    setLayout(blah);

    Update();
}

void InfrachromeMethod::Update(WLPFlag p_PreviewState = WLP_PREVIEW_ON)
{
    qreal fX[3], fY[3]; //RGB fractional parts
    qreal gX[3], gY[3]; //RGB gamma parts

    qreal fracRx = (qreal)m_AllSlides[0][0].value() / 1000.0; //fracRG;
    qreal fracGx = (qreal)m_AllSlides[1][0].value() / 1000.0; //fracGB;

    qreal fracBy = 1.0;

    qreal fracRy = 1.0 - fracRx;
    qreal fracGy = 1.0 - fracGx;
    qreal fracBx = 1.0 - fracBy;
    qreal levelR = (qreal)m_AllSlides[0][1].value() / 1000.0;

    qreal levelG = (qreal)m_AllSlides[1][1].value() / 1000.0;

    qreal levelI = (qreal)m_AllSlides[4][0].value() / 1000.0;

    qreal gammaRx = levelI * ((qreal)m_AllSlides[2][0].value() / 1000.0);

    qreal gammaRy = (qreal)m_AllSlides[2][1].value() / 1000.0;

    qreal gammaGx = levelI * ((qreal)m_AllSlides[3][0].value() / 1000.0);

    qreal gammaGy = (qreal)m_AllSlides[3][1].value() / 1000.0;

    qreal gammaBy = (qreal)m_AllSlides[4][1].value() / 1000.0;


    fX[0] = fracRx; fX[1] = fracGx; fX[2] = fracBx;
    fY[0] = fracRy; fY[1] = fracGy; fY[2] = fracBy;
    gX[0] = gammaRx; gX[1] = gammaGx; //gX[2] = gammaBx;
    gY[0] = gammaRy; gY[1] = gammaGy; gY[2] = gammaBy;

    QImage refImg;
    if(p_PreviewState == WLP_PREVIEW_ON)
    {
        refImg = m_SmallImg;
    }
    else
    {
        refImg = *m_Img_Ptr;
    }

    qreal r,g,b;
    m_DisplayImg = refImg;
    for (int x = 0; x < refImg.width(); ++x) {
         for (int y = 0; y < refImg.height(); ++y) {

            QColor fullYellow(refImg.pixel(x,y));
            qreal source_B = (qreal)fullYellow.blueF();
            qreal source_R = (qreal)fullYellow.redF();
            qreal source_G = (qreal)fullYellow.greenF();

            // destination channel calculation
            qreal Y  = levelI * (1.0 - pow((1.0 - source_B / fY[2]) , (1.0 / gY[2])));
            qreal X1 = levelR * (1.0 - pow((qreal)(1.0 - ((source_R - (fY[0] * (1.0 - pow((qreal)(1.0 - source_B),(qreal) (gY[0])))) / fX[0]))) , (qreal)((1.0 / gX[0]))));
            qreal X2 = levelG * (1.0 - pow((qreal)(1.0 - ((source_G - (fY[1] * (1.0 - pow((qreal)(1.0 - source_B), (qreal)(gY[1])))) / fX[1]))) , (qreal)(1.0 / gX[1])));

            //Show selected channel
            switch(m_ChanState)
            {
            case WLP_PREVIEW_RGB:
                r = qBound<qreal>(0, (Y), 1);
                g = qBound<qreal>(0, (X1), 1);
                b = qBound<qreal>(0, (X2), 1);
                break;

            case WLP_PREVIEW_RED:
                r = qBound<qreal>(0, (Y), 1);
                g = b = r;
                break;
            case WLP_PREVIEW_GREEN:
                g = qBound<qreal>(0, (X1), 1);
                b = r = g;
                break;

            case WLP_PREVIEW_BLUE:
                b = qBound<qreal>(0, (X2), 1);
                r = g = b;
                break;
            default:
                r=g=b=0.5;
                break;
            }

            m_DisplayImg.setPixel(x,y, QColor::fromRgbF(r,g,b).rgb());
         }
    }

    m_OutImg = m_DisplayImg;
    m_MainLabel_Ptr->setPixmap(QPixmap::fromImage(m_OutImg));
}

//Just a method to link to the save as button signal
void InfrachromeMethod::Render()
{
    Update(WLP_PREVIEW_OFF);

    //Open Save As dialog
    QString newName = QFileDialog::getSaveFileName(this, tr("Save image"),
                                                   g_Config.Get(WLPC_DIR_SAVE), tr("JPG (*.jpg) ;; PNG (*.png) ;; TIF (*.tif)") );
    //Save to new path/name
    m_OutImg.save(newName,NULL,100);

    //Set that path as new save path
    QFileInfo bloo(newName);
    WLPSettings s; s.UpdateSaveDir(bloo.absolutePath());
}

void InfrachromeMethod::Drop()
{
    Update(WLP_PREVIEW_OFF);
    QImage *drop = p_Pool->GetPool();
    drop[WLP_POOL_EIR] = m_OutImg;
    p_Pool->Drop("Emulated IRG");
}

void InfrachromeMethod::SaveSettings(bool msgOff = true)
{
    //Safety check:
    QMessageBox::StandardButton reply = QMessageBox::No;
    if(!msgOff)
    {
        reply = QMessageBox::question(this, "Save new configuraion", "Overwrite previous config with new settings?",
                                QMessageBox::Yes|QMessageBox::No);
    }else{ reply = QMessageBox::Yes; }

    if (reply == QMessageBox::Yes)
    {
        //The new config
        g_Config.numArray[WLPC_FRAC_RX] = m_AllSlides[0][0].value();   g_Config.numArray[WLPC_LEVEL_R] = m_AllSlides[0][1].value();
        g_Config.numArray[WLPC_FRAC_GX] = m_AllSlides[1][0].value();   g_Config.numArray[WLPC_LEVEL_G] = m_AllSlides[1][1].value();

        g_Config.numArray[WLPC_GAMMA_R_VIS] = m_AllSlides[2][0].value();   g_Config.numArray[WLPC_GAMMA_R_IRCORR] = m_AllSlides[2][1].value();
        g_Config.numArray[WLPC_GAMMA_G_VIS] = m_AllSlides[3][0].value();   g_Config.numArray[WLPC_GAMMA_G_IRCORR] = m_AllSlides[3][1].value();
        g_Config.numArray[WLPC_MASTER_LEVEL] = m_AllSlides[4][0].value();   g_Config.numArray[WLPC_MASTER_GAMMA] = m_AllSlides[4][1].value();

        //Edit the INI file
        WLPSettings s;
        s.SyncFromBasic();
        s.SaveSettings();
    }
}

//For loading IRG Settings via a txt or ini file
void InfrachromeMethod::LoadSettings()
{
    //Open load dialog
    QString fileName = "NULL";
    fileName = QFileDialog::getOpenFileName(this, QString("Open IRG Settings File"), g_Config.Get(WLPC_DIR_LOAD), QString("Image Files (*.ini)"));
    qDebug() << "fileName: " + fileName;
    if(fileName != "NULL")
    {
        QSettings s(fileName, QSettings::IniFormat);
        //EIR Emulator
        for(int i = (int)'A'; i < (int)'K'; ++i)
        {
            QString variable = "IRG-Emulator/preset" + QString(std::string(1,(char)i).c_str());
            QVariant var = s.value(variable, "666");
            WLPC_IntFlag slider = static_cast<WLPC_IntFlag>(WLPC_BASE_IRG + 1 + i - (int)'A');
            g_Config.Set(slider, var.toInt());
        }
    }

    qDebug() << "rx Frac before: " << g_Config.Get(WLPC_FRAC_RX);
    WLPSettings wlps;
    wlps.SyncFromBasic();
    wlps.SaveSettings();
    qDebug() << "rx Frac after: " << g_Config.Get(WLPC_FRAC_RX);

    //Set config values
    m_AllSlides[0][0].setValue(g_Config.Get(WLPC_FRAC_RX)/*605*/);     m_AllSlides[0][1].setValue(g_Config.Get(WLPC_LEVEL_R)/*835*/);
    m_AllSlides[1][0].setValue(g_Config.Get(WLPC_FRAC_GX)/*820*/);     m_AllSlides[1][1].setValue(g_Config.Get(WLPC_LEVEL_G)/*1150*/);
    m_AllSlides[2][0].setValue(g_Config.Get(WLPC_GAMMA_R_VIS)/*1150*/);    m_AllSlides[2][1].setValue(g_Config.Get(WLPC_GAMMA_R_IRCORR)/*1000*/);
    m_AllSlides[3][0].setValue(g_Config.Get(WLPC_GAMMA_G_VIS)/*1268*/);    m_AllSlides[3][1].setValue(g_Config.Get(WLPC_GAMMA_G_IRCORR)/*1000*/);
    m_AllSlides[4][0].setValue(g_Config.Get(WLPC_MASTER_LEVEL)/*955*/);    m_AllSlides[4][1].setValue(g_Config.Get(WLPC_MASTER_GAMMA)/*1071*/);
}

void InfrachromeMethod::SaveToFile()
{
    //Open Save As dialog
    QString newName = QFileDialog::getSaveFileName(this, tr("Save IRG Settings File"),
                                                   g_Config.Get(WLPC_DIR_SAVE), tr("Config file (*.ini)") );
    //Save to new path/name
    QSettings file(newName, QSettings::IniFormat);
    for(int i = (int)'A'; i < (int)'K'; ++i)
    {
        QString variable = "IRG-Emulator/preset" + QString(std::string(1,(char)i).c_str());
        WLPC_IntFlag slider = static_cast<WLPC_IntFlag>(WLPC_BASE_IRG + 1 + i - (int)'A');
        file.setValue(variable, QString::number(g_Config.Get(slider)));
    }
    file.sync();
}

//Back to the pre-set IRG_V4B filter settings
void InfrachromeMethod::RestoreSettings()
{
    //Safety check:
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Restore the default settings", "This will restore the EIR tool settings back to the very first software default and save over your config settings, are you sure you want to do this?\n\n(To reset back to your config settings just close the EIR tool then re-open it again)",
                                QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        //Set config values
        m_AllSlides[0][0].setValue(605);     m_AllSlides[0][1].setValue(835);
        m_AllSlides[1][0].setValue(820);     m_AllSlides[1][1].setValue(1150);
        m_AllSlides[2][0].setValue(1150);    m_AllSlides[2][1].setValue(1000);
        m_AllSlides[3][0].setValue(1268);    m_AllSlides[3][1].setValue(1000);
        m_AllSlides[4][0].setValue(955);    m_AllSlides[4][1].setValue(1071);

        //Write to wlpConfig.ini
        SaveSettings(true);
    }
}


