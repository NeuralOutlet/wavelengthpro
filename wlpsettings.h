#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QDialog>
#include <QSettings>
#include <QTextEdit>

#include <QDebug>

#include "wlpDefs.h"

class Highlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    Highlighter(QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text) Q_DECL_OVERRIDE;

private:
    struct HighlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

    QTextCharFormat singleLineCommentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat variableFormat;
    QTextCharFormat keywordFormat;
    QTextCharFormat moduleFormat;
    QTextCharFormat numberFormat;
};

class WLPSettings : public QDialog
{
    Q_OBJECT

public:
    WLPSettings(QWidget *parent = 0);

    void UpdateSaveDir(QString dir);
    void UpdateLoadDir(QString dir);

public slots:
    void SyncFromBasic();
    void SaveSettings();
    void SyncConfig();

private:
    //Regex function
    bool ReplaceValue(QString variable, int value, QString &data);
    bool ReplaceValue(QString variable, QString value, QString &data);

    //Main widget
    QTabWidget *p_TabWidget;
    QTextEdit *p_Notepad;
    QSettings m_Settings;
    int m_MyWidth, m_MyHeight;

    //General
    QSpinBox *p_PrevWidth;
    QSpinBox *p_PrevHeight;
    QSpinBox *p_DropMax;
    QCheckBox *p_FlipIR;

    //Adjustment
    QComboBox *p_Modes[5];
    WLPC_IntFlag m_ModesFlags[5];

    //Working-Space
    QComboBox *p_Workspace;
    QComboBox *p_Illuminant;
    QComboBox *p_Gamma;
};

#endif // HIGHLIGHTER_H
