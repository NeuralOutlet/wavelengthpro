#ifndef DUALPROCESS_H
#define DUALPROCESS_H

#include <QPaintEvent>
#include <QPainter>
#include <QWidget>
#include <QSlider>
#include <QLabel>

#include "wlpTypes.h"

class DualProcess;

//================== RGB Bar ===================//

class RGBbar : public QWidget
{
    friend class DualProcess;
    Q_OBJECT

public:
    explicit RGBbar(DualProcess *p_Control_Ptr, QWidget *p_Parent_Ptr = 0);

protected:
    void paintEvent(QPaintEvent *);
    DualProcess *m_Control_Ptr;
};

//================== Dual Process ===================//

class DualProcess : public QWidget, public WLPBase
{
    Q_OBJECT
public:
    explicit DualProcess(WLPPool *_pool, QWidget *p_Parent_Ptr = 0);

    float GetBalance(){return m_Balance;}
    float GetGreenFrac(){return m_GreenDom_Ptr->value();}
    float GetGreenInt(){return m_GreenInt_Ptr->value();}
    
public slots:
    void Update(){ Update(WLP_PREVIEW_ON); }
    void Update(WLPFlag _previewState);
    void Render();
    void Drop();

public:
    QLabel *m_MainLabel_Ptr;
    WLPPool *p_Pool;
    QImage *m_ImgUV_Ptr;
    QImage m_SmallImg;
    QImage m_LargeImg;

    QSlider *m_GreenInt_Ptr;
    QSlider *m_GreenDom_Ptr;
    QSlider *m_Balance_Ptr;

    float m_Balance;
};

#endif // DUALPROCESS_H
