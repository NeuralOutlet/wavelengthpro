#include "wlpsettings.h"

#include <QLabel>
#include <QLayout>
#include <QToolButton>
#include <QPlainTextEdit>
#include <QSignalMapper>
#include <QPushButton>
#include <QFileDialog>
#include <QTabWidget>
#include <QDebug>
#include <QGroupBox>

#include <QRegularExpression>
#include <QRegExp>



//Global config data
extern WLPConfig g_Config;

Highlighter::Highlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;


    keywordFormat.setForeground( QBrush(g_Config.Get(WLPC_NOTE_KEYWORD)) );
    keywordFormat.setFontWeight(QFont::Bold);
    QStringList keywordPatterns;
    keywordPatterns << "\\bWavelengthPro\\b" << "\\bwavelengthpro\\b" << "\\bWLP\\b"
                    << "\\bwlp\\b" << "\\bNikon\\b" << "\\bnikon\\b"
                    << "\\bCanon\\b" << "\\bcanon\\b" << "\\bSony\\b"
                    << "\\bsony\\b" << "\\bFuji\\b" << "\\bfuji\\b";
    foreach (const QString &pattern, keywordPatterns) {
        rule.pattern = QRegExp(pattern);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    moduleFormat.setFontWeight(QFont::Bold);
    moduleFormat.setForeground( QBrush(g_Config.Get(WLPC_NOTE_MODULE)) );
    rule.pattern = QRegExp("\\[[A-Z\\-a-z]+\\]");
    rule.format = moduleFormat;
    highlightingRules.append(rule);

    variableFormat.setForeground(QBrush(g_Config.Get(WLPC_NOTE_VARIABLE)));
    rule.pattern = QRegExp("\\b[A-Za-z]+\\s*\\=");
    rule.format = variableFormat;
    highlightingRules.append(rule);

    numberFormat.setForeground( QBrush(g_Config.Get(WLPC_NOTE_NUMBER)) );
    rule.pattern = QRegExp("\\b[0-9\\.0-9]+\\b");
    rule.format = numberFormat;
    highlightingRules.append(rule);

    numberFormat.setForeground( QBrush(g_Config.Get(WLPC_NOTE_NUMBER)) );
    rule.pattern = QRegExp("\\b0x[0-9A-Fa-f]+\\b");
    rule.format = numberFormat;
    highlightingRules.append(rule);

    quotationFormat.setFontItalic(true);
    quotationFormat.setForeground( QBrush(g_Config.Get(WLPC_NOTE_STRING)) );
    rule.pattern = QRegExp("\".*\"");
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    singleLineCommentFormat.setForeground( QBrush(g_Config.Get(WLPC_NOTE_COMMENT)) );
    rule.pattern = QRegExp("\\;[^\n]*");
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);
}

void Highlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegExp expression(rule.pattern);
        int index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = expression.indexIn(text, index + length);
        }
    }
    setCurrentBlockState(0);
}


WLPSettings::WLPSettings(QWidget *parent)
    : QDialog(parent, Qt::WindowSystemMenuHint | Qt::WindowTitleHint) // now '?' button
{
    setWindowTitle("Settings / Preferences");

    //Initialise data
    m_MyWidth = 450;
    m_MyHeight = 500;
    p_PrevWidth = new QSpinBox;
    p_PrevWidth->setRange(1,1000);
    p_PrevWidth->setValue(g_Config.Get(WLPC_WIDTH));
    p_PrevHeight = new QSpinBox;
    p_PrevHeight->setRange(1,1000);
    p_PrevHeight->setValue(g_Config.Get(WLPC_HEIGHT));
    p_DropMax = new QSpinBox;
    p_DropMax->setRange(1,100);
    p_DropMax->setValue(g_Config.Get(WLPC_DROP_MAX));
    p_FlipIR = new QCheckBox;
    p_FlipIR->setChecked( g_Config.Get(WLPC_FLIP_IR) );

    p_Workspace = new QComboBox;
    p_Workspace->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    QStringList workspaces; workspaces << "sRGB" << "Adobe RGB" << "Apple RGB" << "CIE RGB" << "ProPhoto RGB" << "ECI RGB" << "NTSC RGB" << "PAL RGB" << "Wide Gamut RGB" << "MatchColor RGB";
    p_Workspace->addItems(workspaces);
    p_Workspace->setCurrentIndex(g_Config.Get(WLPC_WORKSPACE));

    p_Illuminant = new QComboBox;
    p_Illuminant->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    QStringList illuminants; illuminants << "A" << "C" << "D50" << "D55" << "D65" << "D75" << "E" << "F2" << "F7" << "F11";
    p_Illuminant->addItems(illuminants);
    p_Illuminant->setCurrentIndex(g_Config.Get(WLPC_ILLUMINANT));

    p_Gamma = new QComboBox;
    p_Gamma->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
    QStringList gammas; gammas << "1.0" << "1.8" << "2.2" << "sRGB" << "L*";
    p_Gamma->addItems(gammas);
    p_Gamma->setCurrentIndex(g_Config.Get(WLPC_SCREEN_GAMMA));

    //rgG,RGB,RGBW, HSL,HSV,HSI, CMY,CMYK, YUV,YIQ,YCbCr,
    //xyY,XYZ,Hunter-Lab,UVW,Luv,LCHuv,Lab,LCHab,LMS
    QStringList modes; modes << "rgG" << "RGB" << "RGBW" << "HSL" << "HSV" << "HSI" << "CMY" << "CMYK" <<
                                "YUV" << "YIQ" << "YCbCr" << "xyY" << "XYZ" << "Hunter-Lab" << "UVW" << "Luv" <<
                                "LCHuv" << "Lab" << "LCHab" << "LMS";

    m_ModesFlags[0] = WLPC_TOPMODE_1;
    m_ModesFlags[1] = WLPC_TOPMODE_2;
    m_ModesFlags[2] = WLPC_TOPMODE_3;
    m_ModesFlags[3] = WLPC_TOPMODE_4;
    m_ModesFlags[4] = WLPC_TOPMODE_5;


    //Set up the basic and advanced settings tabs
    p_TabWidget = new QTabWidget;

    //Basic Settings tab
    QWidget *basic = new QWidget(this);
    QGridLayout *mainLayout = new QGridLayout;

    QGroupBox *box = new QGroupBox("General Preferences");
    QLabel *wText = new QLabel("Prevew width:");
    QLabel *hText = new QLabel("Prevew height:");
    QGridLayout *lay = new QGridLayout;
    lay->addWidget(wText, 0,0);
    lay->addWidget(hText, 1,0);
    lay->addWidget(p_PrevWidth, 0,1);
    lay->addWidget(p_PrevHeight, 1,1);
    lay->addWidget(new QLabel("Max visible dropdown items: "), 2,0,1,1);
    lay->addWidget(p_DropMax, 2,1,1,1);
    lay->addWidget(new QLabel("Red/Blue Swap 'IR (720-100nm)':"), 3,0,1,1);
    lay->addWidget(p_FlipIR, 3,1,1,1);
    box->setLayout(lay);
    mainLayout->addWidget(box,0,0,1,2);

    box = new QGroupBox("Colour-Space Settings");
    QLabel *workspace = new QLabel("RGB Working Space:");
    QLabel *white = new QLabel("Illuminant:");
    QLabel *gamma = new QLabel("Gamma:");
    lay = new QGridLayout;
    lay->addWidget(workspace, 0,0);
    lay->addWidget(white, 1,0);
    lay->addWidget(gamma, 2,0);
    lay->addWidget(p_Workspace, 0,1);
    lay->addWidget(p_Illuminant, 1,1);
    lay->addWidget(p_Gamma, 2,1);
    box->setLayout(lay);
    mainLayout->addWidget(box,1,0,1,1);

    box = new QGroupBox("Default Colour-spaces");
    lay = new QGridLayout;
    for(int i = 0; i < 5; ++i)
    {
        p_Modes[i] = new QComboBox;
        p_Modes[i]->setMaxVisibleItems( g_Config.Get(WLPC_DROP_MAX) );
        p_Modes[i]->addItems(modes);
        p_Modes[i]->setCurrentIndex( g_Config.Get(m_ModesFlags[i]));
        lay->addWidget(new QLabel("Default Mode:"), i,0);
        lay->addWidget(p_Modes[i], i,1);
    }
    box->setLayout(lay);
    mainLayout->addWidget(box,1,1,1,1);

    basic->setLayout(mainLayout);
    p_TabWidget->addTab(basic, "Basic");


    p_Notepad = new QTextEdit;
    QFont fixedFont("Monospace");
    fixedFont.setStyleHint(QFont::TypeWriter);
    QFontMetrics metrics(fixedFont);
    p_Notepad->setTabStopWidth(4 * metrics.width(' '));
    p_Notepad->setFont(fixedFont);
    p_Notepad->setWordWrapMode(QTextOption::NoWrap);
    Highlighter *syntax = new Highlighter(p_Notepad->document());
    Q_UNUSED(syntax)

    //Advanced Settings tab
    QString fileName("wlpConfig.ini");
    QFile file(fileName);
    QString data;
    if (file.open(QFile::ReadOnly | QFile::Text))
    {
        data = file.readAll();
        p_Notepad->setPlainText(data);
    }
    else
        qDebug() << "Invalid config file :(";

    p_TabWidget->addTab(p_Notepad, "Advanced");

    //Se up full widget
    lay = new QGridLayout;
    QPushButton *save = new QPushButton("Save");
    QPushButton *cancel = new QPushButton("Cancel");
    lay->addWidget(p_TabWidget,0,0,1,2);
    lay->addWidget(save, 1,0);
    lay->addWidget(cancel,1,1);
    setLayout(lay);
    setMinimumSize(m_MyWidth, m_MyHeight);

    connect(save, SIGNAL(clicked()), this, SLOT(SaveSettings()));
    connect(save, SIGNAL(clicked()), this, SLOT(close()));
    connect(cancel, SIGNAL(clicked()), this, SLOT(close()));
}

void WLPSettings::SyncConfig()
{
    QVariant var;
    QSettings s("wlpConfig.ini", QSettings::IniFormat);

    var = s.value("width", "500");
    g_Config.Set(WLPC_WIDTH, var.toInt());
    var = s.value("height", "600");
    g_Config.Set(WLPC_HEIGHT, var.toInt());
    var = s.value("flipIR", "0");
    g_Config.Set(WLPC_FLIP_IR, var.toInt());
    var = s.value("dropMax", "12");
    g_Config.Set(WLPC_DROP_MAX, var.toInt());
    var = s.value("lastSave", "");
    g_Config.Set(WLPC_DIR_SAVE, var.toString());
    var = s.value("lastLoad", "");
    g_Config.Set(WLPC_DIR_LOAD, var.toString());

    //Adjustment modes
    var = s.value("Adjustment/modeA", "1");
    g_Config.Set(WLPC_TOPMODE_1, var.toInt());
    var = s.value("Adjustment/modeB", "3");
    g_Config.Set(WLPC_TOPMODE_2, var.toInt());
    var = s.value("Adjustment/modeC", "7");
    g_Config.Set(WLPC_TOPMODE_3, var.toInt());
    var = s.value("Adjustment/modeD", "9");
    g_Config.Set(WLPC_TOPMODE_4, var.toInt());
    var = s.value("Adjustment/modeE", "17");
    g_Config.Set(WLPC_TOPMODE_5, var.toInt());

    //Colour-Space Mixer
    var = s.value("CSpace/workingRGB", "0");
    g_Config.Set(WLPC_WORKSPACE, var.toInt());
    var = s.value("CSpace/illuminant", "4");
    g_Config.Set(WLPC_ILLUMINANT, var.toInt());
    var = s.value("CSpace/gammaType", "3");
    g_Config.Set(WLPC_SCREEN_GAMMA, var.toInt());

    //EIR Emulator
    for(int i = (int)'A'; i < (int)'K'; ++i)
    {
        QString variable = "IRG-Emulator/preset" + QString(std::string(1,(char)i).c_str());
        var = s.value(variable, "Error");
        WLPC_IntFlag slider = static_cast<WLPC_IntFlag>(WLPC_BASE_IRG + 1 + i - (int)'A');
        g_Config.Set(slider, var.toInt());
    }

    //Notepad syntax highlight colours
    var = s.value("Notepad/modules", "0x0000ff");
    g_Config.Set(WLPC_NOTE_MODULE, var.toString().toInt(0,16));
    var = s.value("Notepad/variables", "0x000080");
    g_Config.Set(WLPC_NOTE_VARIABLE, var.toString().toInt(0,16));
    var = s.value("Notepad/numbers", "0x008000");
    g_Config.Set(WLPC_NOTE_NUMBER, var.toString().toInt(0,16));
    var = s.value("Notepad/strings", "0x800000");
    g_Config.Set(WLPC_NOTE_STRING, var.toString().toInt(0,16));
    var = s.value("Notepad/keywords", "0x000000");
    g_Config.Set(WLPC_NOTE_KEYWORD, var.toString().toInt(0,16));
    var = s.value("Notepad/default", "0x000000");
    g_Config.Set(WLPC_NOTE_DEFAULT, var.toString().toInt(0,16));
    var = s.value("Notepad/comments", "0xa0a0a4");
    g_Config.Set(WLPC_NOTE_COMMENT, var.toString().toInt(0,16));

    WLPBase::s_ViewWidth  = g_Config.numArray[WLPC_WIDTH];
    WLPBase::s_ViewHeight = g_Config.numArray[WLPC_HEIGHT];
}

void WLPSettings::SaveSettings()
{
    if(p_TabWidget->currentIndex() == 0)
        SyncFromBasic();

    //Overwrite ini file with contents of Advanced Settings QTextEdit
    QString data = p_Notepad->toPlainText();
    std::ofstream foutini("wlpConfig.ini");
    foutini << data.toStdString();
    foutini.close();

    //Update config to match wlpConfig.ini
    SyncConfig();
}

//Syncs the text to the
//values chosen in the 'Basic' tab.
void WLPSettings::SyncFromBasic()
{
    QString data = p_Notepad->toPlainText();

    QRegularExpression reModule("(\\[[A-Z\\-a-z]+\\])");
    QRegularExpressionMatch match = reModule.match(data);

    int strStart = 0;
    QString outData = "", module = "", theMod = "";
    QStringList dataList = data.split( reModule );
    foreach(module, dataList)
    {
        match = reModule.match(data, strStart);
        strStart = match.capturedEnd();

        if(theMod == "[General]")
        {
            ReplaceValue("width", p_PrevWidth->value(), module);
            ReplaceValue("height", p_PrevHeight->value(), module);
            ReplaceValue("dropMax", p_DropMax->value(), module);
            ReplaceValue("flipIR", p_FlipIR->isChecked(), module);

            //Strings
            ReplaceValue("lastSave", g_Config.Get(WLPC_DIR_SAVE), module);
            ReplaceValue("lastLoad", g_Config.Get(WLPC_DIR_LOAD), module);
        }
        else if(theMod == "[Adjustment]")
        {
            for(int i = 0; i < 5; ++i)
            {
                ReplaceValue("mode" + QString(std::string(1,(char)(i+(int)'A')).c_str()), p_Modes[i]->currentIndex(), module);
            }
        }
        else if(theMod == "[CSpace]")
        {
            ReplaceValue("workingRGB", p_Workspace->currentIndex(), module);
            ReplaceValue("illuminant", p_Illuminant->currentIndex(), module);
            ReplaceValue("gammaType", p_Gamma->currentIndex(), module);
        }
        else if(theMod == "[IRG-Emulator]")
        {
            for(int i = (int)'A'; i < (int)'K'; ++i)
            {
                QString variable = "preset" + QString(std::string(1,(char)i).c_str());
                WLPC_IntFlag slider = static_cast<WLPC_IntFlag>(WLPC_BASE_IRG + 1 + i - (int)'A');
                ReplaceValue(variable, g_Config.Get(slider), module);
            }
        }
        else {}

        //Get next module header and add
        theMod =  match.captured();
        outData += module;
        outData += theMod;
    }

    //Sync the basic values into the advanced notepad
    p_Notepad->setPlainText(outData);
}

bool WLPSettings::ReplaceValue(QString variable, int value, QString &data)
{
    QRegularExpression reVarval("(\\s*" + variable + "\\s*\\=\\s*)[0-9]+");
    if(!data.contains(reVarval))
    {
        qDebug() << "Failed: This module does not contain this variable.";
        return false;
    }

    //Replace old value with new
    data.replace(reVarval, "\\1" + QString::number(value));
    return true;
}

bool WLPSettings::ReplaceValue(QString variable, QString value, QString &data)
{
    QRegularExpression reVarval("(\\s*" + variable + "\\s*\\=\\s*)\\\".*\\\"");
    if(!data.contains(reVarval))
    {
        qDebug() << "Failed: This module does not contain this variable.";
        return false;
    }

    //Replace old value with new
    data.replace(reVarval, "\\1\"" + value + "\"");
    return true;
}

void WLPSettings::UpdateSaveDir(QString dir)
{
    g_Config.Set(WLPC_DIR_SAVE, dir);
    SyncFromBasic();
    SaveSettings();
}


void WLPSettings::UpdateLoadDir(QString dir)
{
    g_Config.Set(WLPC_DIR_LOAD, dir);
    SyncFromBasic();
    SaveSettings();
}
