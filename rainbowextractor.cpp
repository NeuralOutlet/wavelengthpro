#include "rainbowextractor.h"
#include "wlpsettings.h"

extern WLPConfig g_Config;

RainbowExtractor::RainbowExtractor(WLPPool *_pool, QWidget *parent) :
    QWidget(parent)
{
    setWindowTitle("Rainbow Extractor");
    p_Pool = _pool;

    //Pass through the full image and get the scaled versions for display:
    m_RefImg = *p_Pool->GetImg("VIS (400-650nm)");
    m_ScaledImg = m_RefImg.scaled(s_ViewWidth, s_ViewHeight,Qt::KeepAspectRatio);
    m_ExtractImg = QImage(m_ScaledImg.width(), m_ScaledImg.height(), QImage::Format_ARGB32);

    m_ScaledImg = m_ScaledImg.convertToFormat(QImage::Format_ARGB32);

    //Set up the two sliders (hue and hue-variant)
    p_HueSlide = new QSlider(Qt::Horizontal);
    p_HueSlide->setRange(0,359);
    p_HueSlide->setFixedWidth(360);
    connect(p_HueSlide, SIGNAL(valueChanged(int)), this, SLOT(Update(int)));

    p_DeltaSlide = new QSlider(Qt::Horizontal);
    p_DeltaSlide->setRange(0,360);
    p_DeltaSlide->setFixedWidth(360);
    connect(p_DeltaSlide, SIGNAL(valueChanged(int)), this, SLOT(Update(int)));

    //Set up the saturation slider
    p_SatSlide = new QSlider(Qt::Horizontal);
    p_SatSlide->setRange(0,255); p_SatSlide->setValue(255);
    p_SatSlide->setMaximumWidth(m_ScaledImg.width()/2);
    connect(p_SatSlide, SIGNAL(valueChanged(int)), this, SLOT(Update(int)));

    //Set up the lightness slider
    p_LightSlide = new QSlider(Qt::Horizontal);
    p_LightSlide->setRange(0,255); p_LightSlide->setValue(127);
    p_LightSlide->setMaximumWidth(m_ScaledImg.width()/2);
    connect(p_LightSlide, SIGNAL(valueChanged(int)), this, SLOT(Update(int)));

    //Set up the alpha slider
    p_AlphaSlide = new QSlider(Qt::Horizontal);
    p_AlphaSlide->setRange(0,255); p_AlphaSlide->setValue(255);
    p_AlphaSlide->setMaximumWidth(m_ScaledImg.width()/2);
    connect(p_AlphaSlide, SIGNAL(valueChanged(int)), this, SLOT(Update(int)));

    //Experimenal smoothening blur
    m_BlurOn = false;
    p_BlendRadio = new QRadioButton("Use Feathered Selection");
    p_BlendRadio->setChecked(false);
    connect(p_BlendRadio, SIGNAL(clicked()), this, SLOT(SwitchBlur()));
    connect(p_BlendRadio, SIGNAL(clicked()), this, SLOT(Update()));

    //Set up the hue bar:
    p_HueView = new QLabel;
    m_HueBar = QImage(360, 20, m_ScaledImg.format());
    int h,s,l;
    for (int x = 0; x < 360; ++x) {
        for (int y = 0; y < 20; ++y) {

             QColor hsl;
             h = x;
             s = 255;
             l = 127;

             hsl.setHsl(h, s, l);
             QRgb col = QColor::fromRgb( hsl.red(),hsl.green(),hsl.blue() ).rgb();

           m_HueBar.setPixel(x, y, col);
         }
       }
    p_HueView->setPixmap(QPixmap::fromImage(m_HueBar));
    p_RefView = new QLabel; p_RefView->setPixmap(QPixmap::fromImage(m_ScaledImg));
    p_ExtractView = new QLabel; p_ExtractView->setPixmap(QPixmap::fromImage(m_ExtractImg));

    //Set up the layout of the widget
    QGridLayout *layout = new QGridLayout;

    //Left side
    layout->addWidget(p_RefView, 0,0,1,2,Qt::AlignCenter);
    layout->addWidget(p_HueView, 1,1,1,1);
    layout->addWidget(new QLabel("Hue"), 2,0,1,1);
    layout->addWidget(p_HueSlide, 2,1,1,1);
    layout->addWidget(new QLabel("Range"), 3,0,1,1);
    layout->addWidget(p_DeltaSlide, 3,1,1,1);
    QPushButton *save = new QPushButton("Save As");
    layout->addWidget(save, 4,0,1,1,Qt::AlignLeft);
    QPushButton *drop = new QPushButton("Drop to Pool");
    layout->addWidget(drop, 4,1,1,1,Qt::AlignRight);

    //Right side
    layout->addWidget(p_ExtractView, 0, 2,1,2,Qt::AlignCenter);
    layout->addWidget(new QLabel("Saturation"), 1,2,1,1);
    layout->addWidget(p_SatSlide, 1,3,1,1);
    layout->addWidget(new QLabel("Lightness"), 2,2,1,1);
    layout->addWidget(p_LightSlide, 2,3,1,1);
    layout->addWidget(new QLabel("Alpha"), 3,2,1,1);
    layout->addWidget(p_AlphaSlide, 3,3,1,1);
    layout->addWidget(new QLabel("Blur"), 4,2,1,1);
    layout->addWidget(p_BlendRadio, 4,3,1,1);

    setLayout(layout);

    connect(save, SIGNAL(clicked()), this, SLOT(Render()));
    connect(drop, SIGNAL(clicked()), this, SLOT(Drop()));
}

bool inVariant(int x, int hue, int variant)
{
    int vHalf = variant/2;

    if(x >= hue-vHalf && x <= hue+vHalf)
            return true;

    if(hue-vHalf < 0)
    {
        if(x >= 360-abs(hue-vHalf))
            return true;
    }

    if(hue+vHalf > 360)
    {
        if(x <= abs(360-(hue+vHalf)))
            return true;
    }
    return false;
}

QColor FeatherPixel(QColor _before, QColor _after, qreal _alpha)
{
    qreal r = (_alpha * _after.redF()) + ((1.0-_alpha) * _before.redF());
    qreal g = (_alpha * _after.greenF()) + ((1.0-_alpha) * _before.greenF());
    qreal b = (_alpha * _after.blueF()) + ((1.0-_alpha) * _before.blueF());

    return QColor::fromRgbF(r,g,b).rgba();
}

qreal FeatherAlpha(qreal _before, qreal _after, qreal _alpha)
{
    _before /= 255;
    _after /= 255;
    return (_alpha * _after) + ((1.0-_alpha) * _before);
}


bool SameCol(QColor before, QColor after)
{
    if( before.red() == after.red() &&
        before.green() == after.green() &&
        before.blue() == after.blue() &&
        before.alpha() == after.alpha() )
        return true;
    else if(before.alpha() != after.alpha())
        qDebug() << "Waht";

    return false;
}

int linear(int a, int b, float t)
{
    return a * (1 - t) + b * t;
}

void RainbowExtractor::Update(WLPFlag _previewState)
{
    //Get slider values for HSLA model
    qreal hue = p_HueSlide->value();       //The exact hue
    int variant = p_DeltaSlide->value(); //The range of hue
    int sat = p_SatSlide->value();
    int light = p_LightSlide->value();
    int alpha = p_AlphaSlide->value();

    QImage refImg;
    if(_previewState == WLP_PREVIEW_ON)
        refImg = m_ExtractImg = m_ScaledImg.convertToFormat(QImage::Format_ARGB32);
    else
        refImg = m_ExtractImg = m_RefImg;

    for (int x = 0; x < refImg.width(); ++x)
    {
        for (int y = 0; y < refImg.height(); ++y)
        {
            //If it's a background colour
            qreal h = static_cast<qreal>(hue)/360.0;
            qreal s = static_cast<qreal>(sat)/255.0;
            qreal l = static_cast<qreal>(light)/255.0;
            qreal a = static_cast<qreal>(alpha)/255.0;
            QColor col = QColor::fromHslF(h,s,l, a);

            //If it's an important colour
            QColor vis(refImg.pixel(x,y));
            if(inVariant(vis.hslHue(), hue, variant))
                col = QColor::fromRgb( vis.red(),vis.green(),vis.blue() );

            m_ExtractImg.setPixel(x, y, col.rgba());
        }
    }


    if(m_BlurOn)
    {
        //Attempted blur:
        QImage original = refImg.convertToFormat(QImage::Format_ARGB32);
        QImage temp(original.width(), original.height(), QImage::Format_ARGB32);
        QColor before,after;

        for (int x = 10; x < original.width() - 10; ++x)
        {
            for (int y = 10; y < original.height() - 10; ++y)
            {
                //Calculate alpha value (blend weighting)
                 int sameCount = 0;
                 sameCount += SameCol(original.pixel(x,y), m_ExtractImg.pixel(x,y));

                 for(int i = 2; i < 11; i+=2)
                 {
                     for(int j = 2; j < 11; j+=2)
                     {
                         sameCount += SameCol(original.pixel(x+i,y+j), m_ExtractImg.pixel(x+i,y+j));
                         sameCount += SameCol(original.pixel(x+i,y-j), m_ExtractImg.pixel(x+i,y-j));
                         sameCount += SameCol(original.pixel(x-i,y+j), m_ExtractImg.pixel(x-i,y+j));
                         sameCount += SameCol(original.pixel(x-i,y-j), m_ExtractImg.pixel(x-i,y-j));
                     }
                 }

                 //Get original pixels
                 before = original.pixel(x,y);
                 after = m_ExtractImg.pixel(x,y);

                 //Call feather function
                 qreal alpha = qBound<qreal>(0, static_cast<qreal>(sameCount) / (25.0), 1);

                 QColor tempCol = FeatherPixel(after, before, alpha);

                 tempCol.setAlphaF(alpha);
                 temp.setPixel(x, y, tempCol.rgba());
             }
           }
        m_ExtractImg = temp;
    }
    p_ExtractView->setPixmap(QPixmap::fromImage(m_ExtractImg));

    int h,s,l;
    for (int x = 0; x < 360; ++x)
    {
        for (int y = 0; y < 20; ++y)
        {
            QColor hsl;
            h = x;

            //If the hue is within the bounds of the variant
            //give it half saturation, else full:
            (inVariant(x, hue, variant)) ? s=100 : s=255;
            //If the hue is the chosen one, make it black:
            (x!=hue && x % 60 != 0) ? l=127 : l=0;

            hsl.setHsl(h, s, l);
            QRgb col = QColor::fromRgb( hsl.red(),hsl.green(),hsl.blue() ).rgb();

            m_HueBar.setPixel(x, y, col);
         }
       }
    p_HueView->setPixmap(QPixmap::fromImage(m_HueBar));
}

void RainbowExtractor::Render()
{
    Update(WLP_PREVIEW_OFF);
    //Open Save As dialog
    QString newName = QFileDialog::getSaveFileName(this, tr("Save image"),
                                                   g_Config.Get(WLPC_DIR_SAVE), tr("JPG (*.jpg) ;; PNG (*.png) ;; TIF (*.tif)") );
    //Save to new path/name
    m_ExtractImg.save(newName,NULL,100);

    //Set that path as new save path
    QFileInfo bloo(newName);
    WLPSettings s; s.UpdateSaveDir(bloo.absolutePath());
}

void RainbowExtractor::Drop()
{
    Update(WLP_PREVIEW_OFF);
    QImage *drop = p_Pool->GetPool();
    drop[WLP_POOL_MASK] = m_ExtractImg;
    p_Pool->Drop("Rainbow Mask");
}
