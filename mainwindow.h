#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QListWidgetItem>
#include <QMdiSubWindow>
#include <QProgressBar>
#include <QActionGroup>
#include <QMainWindow>
#include <QListWidget>
#include <QComboBox>
#include <QTextEdit>
#include <QCheckBox>
#include <string>
#include <QLabel>
#include <QMdiArea>
#include <QMenu>

#include "infrachromemethod.h"
#include "rainbowextractor.h"
#include "spectralexplorer.h"
#include "luminancemapper.h"
#include "generalviewediting.h"
#include "pooleditor.h"
#include "dualprocess.h"
#include "colourpurity.h"

#include "wlpTypes.h"

class QSignalMapper;

namespace Ui {
class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void SelectedLoad(QString);

public slots:
    //Loading in photos
    void CatchDropped(QString _type);
    void PhotoLoad(QString fileName, QString imgType);
    void PhotoLoadDialog(QString _imgType);
    void PhotoLoadDrag(QString _url);
    void PhotoLoadGeneral();
    void PhotoSetSpectral(bool isit){ m_IsSpectral = !isit; }

    //General Settings
    void OpenSettings();
    void ExampleLinks();
    void AboutWLP();

    // WLPTools
    void runChrome();
    void runDual();
    void runChanMix();
    void runRainbow();
    void runHSLMap();
    void runPurity();
    void runScale(){ runPool(WLP_EDIT_SCALE); }
    void runAlign(){ runPool(WLP_EDIT_ALIGN); }
    void runCloud(){ runPool(WLP_EDIT_CLOUD); }

    //StatusBar slots
    void LoadUpdate(int _progress){ p_StatProgress->setValue( _progress ); }
    void LoadText(QString _str){ p_StatLabel->setText( _str ); }
    void LoadComplete(){ p_StatProgress->setValue( 0 ); p_StatLabel->setText( "" ); }

    //General editing
    void Adjustment(QMdiSubWindow* _subWnd);
    void ChangeMode(QAction *_act);
    void ShowHideMode();
    void GEditorControl();
    void GEditorInvert();
    void GEditorBright(){ GEditorBright(m_ContrastType); }
    void GEditorBright(QString _type);

    //General viewing
    void PoolListClickedGeneral(QModelIndex _index);
    void PoolListClickedGeneralGif(QModelIndex _index);
    void PoolListClicked(QListWidgetItem* item);
    void OpenContextMenu(QPoint _point);
    void OpenGifViewer(QPoint _point);

protected:
    void dropEvent(QDropEvent *ev);
    void dragEnterEvent(QDragEnterEvent *ev);

private:
    Ui::MainWindow *ui;

    //Private Functions
    void SetUpMenu();
    QAction* CreateLoadOption(QMenu* const loadMenu, QSignalMapper* const signalMapper, const QString& name, const QString& description);
    WLPFlag SetImage(QImage img, QString type);
    void runPool(WLPFlag _editType);

    QImage* GetImage(QString type);

    //WLP Tools
    QMenu *p_ToolsMenu;
    WLPTool<InfrachromeMethod> m_Infrachrome;
    WLPTool<DualProcess> m_DualProcess;
    WLPTool<SpectralExplorer> m_ChannelMix;
    WLPTool<RainbowExtractor> m_HueExtract;
    WLPTool<LuminanceMapper> m_SpaceMix;
    WLPTool<ColourPurity> m_Purity;
    WLPTool<PoolEditor> m_PoolEdit;

    //Private Variables
    QMdiArea *specialArea;
    QMdiSubWindow *sw;
    QLabel *p_StatLabel;
    QProgressBar *p_StatProgress;

    //Loading Images
    bool m_IsSpectral;
    WLPPool * p_ImgPool;

    //Image pool
    QTabWidget *p_PoolTabs;
    QListWidget *p_PoolSpectral;
    QListWidget *p_PoolGeneral;
    ImageLister *p_PoolGeneralTest;
    QListWidget *p_PoolCreated;
    QMdiSubWindow *p_PoolSubWnd;

    int m_CountSpectral;
    int m_CountCreated;
    WLPFlag m_PoolFlag;

    //Automatic makers
    QLabel *infrachrome;
    QAction *eirChrome;
    QAction *dualProaction;
    QAction *rainbowAction;
    QAction *hslMapAction;
    QAction *purityAction;
    QAction *chanMixAction;

    //Pool Actions
    QMenu *p_PoolMenu;
    QAction *p_AlignAction;
    QAction *p_CloudAction;
    QAction *p_ScaleAction;

    //Adjust Actions
    QActionGroup *p_ModeGroup;
    QMenu *p_AdjustMenu;
    QAction *p_ControlAction;
    QAction *p_BrightConAction;
    QString m_ContrastType;
    QAction *p_ModeActions[20];
    QAction *p_ShowAll;
};

#endif // MAINWINDOW_H
