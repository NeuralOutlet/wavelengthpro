#include "wlpDefs.h"

#define DEG_TO_RAD(X) (M_PI*(X)/180)

//Static declerations
QColor WLPBase::lutRGBtoLCH[256][256][256];
QColor WLPBase::lutLCHtoRGB[256][256][256];
QColor WLPBase::lutRGBtoLab[256][256][256];
QColor WLPBase::lutLabtoRGB[256][256][256];

void WLPBase::Initialise()
{
    ChangeWhite(4); //Set to D65 illuminant
    ChangeRGB(WLP_WORKSPACE_SRGB); //Set sRGB
    m_GammaFlag = WLP_GAMMA_SRGB;
    m_Gamma = 2 + (51.0/256.0); //Not used by sRGB

    /*
     * [ Y ]     [ 0.299   0.587   0.114 ] [ R ]
     * [ I ]  =  [ 0.596  -0.275  -0.321 ] [ G ]
     * [ Q ]     [ 0.212  -0.523   0.311 ] [ B ]
     *
     */

    rgb2yiqMatrix(0, 0) = 0.299; rgb2yiqMatrix(1, 0) = 0.587;  rgb2yiqMatrix(2, 0) = 0.114;
    rgb2yiqMatrix(0, 1) = 0.596; rgb2yiqMatrix(1, 1) = -0.275; rgb2yiqMatrix(2, 1) = -0.321;
    rgb2yiqMatrix(0, 2) = 0.212; rgb2yiqMatrix(1, 2) = -0.523; rgb2yiqMatrix(2, 2) = 0.311;

    /*
     * [ R ]     [ 1   0.956   0.621 ] [ Y ]
     * [ G ]  =  [ 1  -0.272  -0.647 ] [ I ]
     * [ B ]     [ 1  -1.105   1.702 ] [ Q ]
    */

    yiq2rgbMatrix(0, 0) = 1; yiq2rgbMatrix(1, 0) = 0.956; yiq2rgbMatrix(2, 0) = 0.621;
    yiq2rgbMatrix(0, 1) = 1; yiq2rgbMatrix(1, 1) = -0.272; yiq2rgbMatrix(2, 1) = -0.647;
    yiq2rgbMatrix(0, 2) = 1; yiq2rgbMatrix(1, 2) = -1.105; yiq2rgbMatrix(2, 2) = 1.702;

    /*
     * [ Y ]     [  0.299     0.587     0.114   ] [ R ]
     * [ U ]  =  [ -0.14713  -0.28886   0.436   ] [ G ]
     * [ V ]     [  0.615    -0.51499  -0.10001 ] [ B ]
     *
     */

    rgb2yuvMatrix(0, 0) = 0.299;    rgb2yuvMatrix(1, 0) = 0.587;    rgb2yuvMatrix(2, 0) = 0.114;
    rgb2yuvMatrix(0, 1) = -0.14713; rgb2yuvMatrix(1, 1) = -0.28886; rgb2yuvMatrix(2, 1) = 0.436;
    rgb2yuvMatrix(0, 2) = -0.51499; rgb2yuvMatrix(1, 2) = -0.523;   rgb2yuvMatrix(2, 2) = -0.10001;



    //LMS via CIECAM02 Conversion
    xyz2lmsMatrix(0, 0) =  0.7328; xyz2lmsMatrix(1, 0) =  0.4296; xyz2lmsMatrix(2, 0) = -0.1624;
    xyz2lmsMatrix(0, 1) = -0.7036; xyz2lmsMatrix(1, 1) =  1.6975; xyz2lmsMatrix(2, 1) =  0.0061;
    xyz2lmsMatrix(0, 2) =  0.0030; xyz2lmsMatrix(1, 2) =  0.0136; xyz2lmsMatrix(2, 2) =  0.9834;

    //Inverse
    lms2xyzMatrix(0, 0) =  1.09612; lms2xyzMatrix(1, 0) = -0.27886; lms2xyzMatrix(2, 0) =  0.182645;
    lms2xyzMatrix(0, 1) =  0.454369; lms2xyzMatrix(1, 1) =  0.473533; lms2xyzMatrix(2, 1) =  0.072097;
    lms2xyzMatrix(0, 2) =  -0.009627; lms2xyzMatrix(1, 2) =  -0.00569803; lms2xyzMatrix(2, 2) =  1.0153256;

}

void WLPBase::ChangeWhite(int _index)
{
    switch(_index)
    {
    case 0: //A
        m_Illuminant = Tristimulus(109.850, 100, 35.585);
        break;
    case 1: //C
        m_Illuminant = Tristimulus(98.074, 100, 118.232);
        break;
    case 2: //D50
        m_Illuminant = Tristimulus(96.422, 100, 82.521);
        break;
    case 3: //D55
        m_Illuminant = Tristimulus(95.682, 100, 92.149);
        break;
    case 4: //D65
        m_Illuminant = Tristimulus(95.047, 100, 108.883);
        break;
    case 5: //D75
        m_Illuminant = Tristimulus(94.972, 100, 122.638);
        break;
    case 6: //E
        m_Illuminant = Tristimulus(100, 100, 100);
        break;
    case 7: //F2
        m_Illuminant = Tristimulus(99.187, 100, 67.395);
        break;
    case 8: //F7
        m_Illuminant = Tristimulus(95.044, 100, 108.755);
        break;
    case 9: //F11
        m_Illuminant = Tristimulus(100.966, 100, 64.370);
        break;
    }
}

void WLPBase::ChangeRGB(WLPFlag _workingSpace)
{
    switch(_workingSpace)
    {
    case WLP_WORKSPACE_ADOBE:
        rgb2xyzMatrix(0, 0) = 0.5767309; rgb2xyzMatrix(1, 0) = 0.1855540; rgb2xyzMatrix(2, 0) = 0.1881852;
        rgb2xyzMatrix(0, 1) = 0.2973769; rgb2xyzMatrix(1, 1) = 0.6273491; rgb2xyzMatrix(2, 1) = 0.0752741;
        rgb2xyzMatrix(0, 2) = 0.0270343; rgb2xyzMatrix(1, 2) = 0.0706872; rgb2xyzMatrix(2, 2) = 0.9911085;

        xyz2rgbMatrix(0, 0) = 2.0413690;  xyz2rgbMatrix(1, 0) = -0.5649464; xyz2rgbMatrix(2, 0) = -0.3446944;
        xyz2rgbMatrix(0, 1) = -0.9692660; xyz2rgbMatrix(1, 1) = 1.8760108;  xyz2rgbMatrix(2, 1) = 0.0415560;
        xyz2rgbMatrix(0, 2) = 0.0134474;  xyz2rgbMatrix(1, 2) = -0.1183897; xyz2rgbMatrix(2, 2) = 1.0154096;
        break;
    case WLP_WORKSPACE_APPLE:
        rgb2xyzMatrix(0, 0) = 0.4497288; rgb2xyzMatrix(1, 0) = 0.3162486; rgb2xyzMatrix(2, 0) = 0.1844926;
        rgb2xyzMatrix(0, 1) = 0.2446525; rgb2xyzMatrix(1, 1) = 0.6720283; rgb2xyzMatrix(2, 1) = 0.0833192;
        rgb2xyzMatrix(0, 2) = 0.0251848; rgb2xyzMatrix(1, 2) = 0.1411824; rgb2xyzMatrix(2, 2) = 0.9224628;

        xyz2rgbMatrix(0, 0) = 2.9515373;  xyz2rgbMatrix(1, 0) = -1.2894116; xyz2rgbMatrix(2, 0) = -0.4738445;
        xyz2rgbMatrix(0, 1) = -1.0851093; xyz2rgbMatrix(1, 1) = 1.9908566;  xyz2rgbMatrix(2, 1) = 0.0372026;
        xyz2rgbMatrix(0, 2) = 0.0854934;  xyz2rgbMatrix(1, 2) = -0.2694964; xyz2rgbMatrix(2, 2) = 1.0912975;
        break;
    case WLP_WORKSPACE_WIDE:
        rgb2xyzMatrix(0, 0) = 0.7161046; rgb2xyzMatrix(1, 0) = 0.1009296; rgb2xyzMatrix(2, 0) = 0.1471858;
        rgb2xyzMatrix(0, 1) = 0.2581874; rgb2xyzMatrix(1, 1) = 0.7249378; rgb2xyzMatrix(2, 1) = 0.0168748;
        rgb2xyzMatrix(0, 2) = 0.0000000; rgb2xyzMatrix(1, 2) = 0.0517813; rgb2xyzMatrix(2, 2) = 0.7734287;

        xyz2rgbMatrix(0, 0) = 1.4628067;  xyz2rgbMatrix(1, 0) = -0.1840623; xyz2rgbMatrix(2, 0) = -0.2743606;
        xyz2rgbMatrix(0, 1) = -0.5217933; xyz2rgbMatrix(1, 1) = 1.4472381;  xyz2rgbMatrix(2, 1) = 0.0677227;
        xyz2rgbMatrix(0, 2) = 0.0349342;  xyz2rgbMatrix(1, 2) = -0.0968930; xyz2rgbMatrix(2, 2) = 1.2884099;
        break;
    case WLP_WORKSPACE_PRO:
        rgb2xyzMatrix(0, 0) = 0.7976749; rgb2xyzMatrix(1, 0) = 0.1351917; rgb2xyzMatrix(2, 0) = 0.0313534;
        rgb2xyzMatrix(0, 1) = 0.2880402; rgb2xyzMatrix(1, 1) = 0.7118741; rgb2xyzMatrix(2, 1) = 0.0000857;
        rgb2xyzMatrix(0, 2) = 0.0000000; rgb2xyzMatrix(1, 2) = 0.0000000; rgb2xyzMatrix(2, 2) = 0.8252100;

        xyz2rgbMatrix(0, 0) = 1.3459433;  xyz2rgbMatrix(1, 0) = -0.2556075; xyz2rgbMatrix(2, 0) = -0.0511118;
        xyz2rgbMatrix(0, 1) = -0.5445989; xyz2rgbMatrix(1, 1) = 1.5081673;  xyz2rgbMatrix(2, 1) = 0.0205351;
        xyz2rgbMatrix(0, 2) = 0.0000000;  xyz2rgbMatrix(1, 2) = 0.0000000;  xyz2rgbMatrix(2, 2) = 1.2118128;
        break;
    case WLP_WORKSPACE_CIE:
        rgb2xyzMatrix(0, 0) = 0.4887180; rgb2xyzMatrix(1, 0) = 0.3106803; rgb2xyzMatrix(2, 0) = 0.2006017;
        rgb2xyzMatrix(0, 1) = 0.1762044; rgb2xyzMatrix(1, 1) = 0.8129847; rgb2xyzMatrix(2, 1) = 0.0108109;
        rgb2xyzMatrix(0, 2) = 0.0000000; rgb2xyzMatrix(1, 2) = 0.0102048; rgb2xyzMatrix(2, 2) = 0.9897952;

        xyz2rgbMatrix(0, 0) = 2.3706743;  xyz2rgbMatrix(1, 0) = -0.9000405; xyz2rgbMatrix(2, 0) = -0.4706338;
        xyz2rgbMatrix(0, 1) = -0.5138850; xyz2rgbMatrix(1, 1) = 1.4253036;  xyz2rgbMatrix(2, 1) = 0.0885814;
        xyz2rgbMatrix(0, 2) = 0.0052982;  xyz2rgbMatrix(1, 2) = -0.0146949; xyz2rgbMatrix(2, 2) = 1.0093968;
        break;
    case WLP_WORKSPACE_ECI:
        rgb2xyzMatrix(0, 0) = 0.6502043; rgb2xyzMatrix(1, 0) = 0.1780774; rgb2xyzMatrix(2, 0) = 0.1359384;
        rgb2xyzMatrix(0, 1) = 0.3202499; rgb2xyzMatrix(1, 1) = 0.6020711; rgb2xyzMatrix(2, 1) = 0.0776791;
        rgb2xyzMatrix(0, 2) = 0.0000000; rgb2xyzMatrix(1, 2) = 0.0678390; rgb2xyzMatrix(2, 2) = 0.7573710;

        xyz2rgbMatrix(0, 0) = 1.7827618;  xyz2rgbMatrix(1, 0) = -0.4969847; xyz2rgbMatrix(2, 0) = -0.2690101;
        xyz2rgbMatrix(0, 1) = -0.9593623; xyz2rgbMatrix(1, 1) = 1.9477962;  xyz2rgbMatrix(2, 1) = -0.0275807;
        xyz2rgbMatrix(0, 2) = 0.0859317;  xyz2rgbMatrix(1, 2) = -0.1744674; xyz2rgbMatrix(2, 2) = 1.3228273;
        break;
    case WLP_WORKSPACE_NTSC:
        rgb2xyzMatrix(0, 0) = 0.6068909; rgb2xyzMatrix(1, 0) = 0.1735011; rgb2xyzMatrix(2, 0) = 0.2003480;
        rgb2xyzMatrix(0, 1) = 0.2989164; rgb2xyzMatrix(1, 1) = 0.5865990; rgb2xyzMatrix(2, 1) = 0.1144845;
        rgb2xyzMatrix(0, 2) = 0.0000000; rgb2xyzMatrix(1, 2) = 0.0660957; rgb2xyzMatrix(2, 2) = 1.1162243;

        xyz2rgbMatrix(0, 0) = 1.9099961;  xyz2rgbMatrix(1, 0) = -0.5324542; xyz2rgbMatrix(2, 0) = -0.2882091;
        xyz2rgbMatrix(0, 1) = -0.9846663; xyz2rgbMatrix(1, 1) = 1.9991710;  xyz2rgbMatrix(2, 1) = -0.0283082;
        xyz2rgbMatrix(0, 2) = 0.0583056;  xyz2rgbMatrix(1, 2) = -0.1183781; xyz2rgbMatrix(2, 2) = 0.8975535;
        break;
    case WLP_WORKSPACE_PAL:
        rgb2xyzMatrix(0, 0) = 0.4306190; rgb2xyzMatrix(1, 0) = 0.3415419; rgb2xyzMatrix(2, 0) = 0.1783091;
        rgb2xyzMatrix(0, 1) = 0.2220379; rgb2xyzMatrix(1, 1) = 0.7066384; rgb2xyzMatrix(2, 1) = 0.0713236;
        rgb2xyzMatrix(0, 2) = 0.0201853; rgb2xyzMatrix(1, 2) = 0.1295504; rgb2xyzMatrix(2, 2) = 0.9390944;

        xyz2rgbMatrix(0, 0) = 3.0628971;  xyz2rgbMatrix(1, 0) = -1.3931791; xyz2rgbMatrix(2, 0) = -0.4757517;
        xyz2rgbMatrix(0, 1) = -0.9692660; xyz2rgbMatrix(1, 1) = 1.8760108;  xyz2rgbMatrix(2, 1) = -0.0415560;
        xyz2rgbMatrix(0, 2) = 0.0678775;  xyz2rgbMatrix(1, 2) = -0.2288548; xyz2rgbMatrix(2, 2) = 1.0693490;
        break;
    case WLP_WORKSPACE_MATCH:
        rgb2xyzMatrix(0, 0) = 0.5093439; rgb2xyzMatrix(1, 0) = 0.3209071; rgb2xyzMatrix(2, 0) = 0.1339691;
        rgb2xyzMatrix(0, 1) = 0.2748840; rgb2xyzMatrix(1, 1) = 0.6581315; rgb2xyzMatrix(2, 1) = 0.0669845;
        rgb2xyzMatrix(0, 2) = 0.0242545; rgb2xyzMatrix(1, 2) = 0.1087821; rgb2xyzMatrix(2, 2) = 0.6921735;

        xyz2rgbMatrix(0, 0) = 2.6422874;  xyz2rgbMatrix(1, 0) = -1.2234270; xyz2rgbMatrix(2, 0) = -0.3930143;
        xyz2rgbMatrix(0, 1) = -1.1119763; xyz2rgbMatrix(1, 1) = 2.0590183;  xyz2rgbMatrix(2, 1) = -0.0159614;
        xyz2rgbMatrix(0, 2) = 0.0821699;  xyz2rgbMatrix(1, 2) = -0.2807254; xyz2rgbMatrix(2, 2) = 1.4559877;
        break;
    case WLP_WORKSPACE_SRGB:
    default:
        rgb2xyzMatrix(0, 0) = 0.412453; rgb2xyzMatrix(1, 0) = 0.357580; rgb2xyzMatrix(2, 0) = 0.180423;
        rgb2xyzMatrix(0, 1) = 0.212671; rgb2xyzMatrix(1, 1) = 0.715160; rgb2xyzMatrix(2, 1) = 0.072169;
        rgb2xyzMatrix(0, 2) = 0.019334; rgb2xyzMatrix(1, 2) = 0.119193; rgb2xyzMatrix(2, 2) = 0.950227;

        xyz2rgbMatrix(0, 0) = 3.240479;  xyz2rgbMatrix(1, 0) = -1.537150; xyz2rgbMatrix(2, 0) = -0.498535;
        xyz2rgbMatrix(0, 1) = -0.969256; xyz2rgbMatrix(1, 1) = 1.875992;  xyz2rgbMatrix(2, 1) = 0.041556;
        xyz2rgbMatrix(0, 2) = 0.055648;  xyz2rgbMatrix(1, 2) = -0.204043; xyz2rgbMatrix(2, 2) = 1.057311;
        break;
    }
}

//=========Convertors============//

QColor WLPBase::RGBtoXYZ(QColor p_Col)
{
    QColor colOut;

    //Test Colour:
    //QColor test(0,100,50);
    //p_Col = test;

    qreal var_R = p_Col.redF();
    qreal var_G = p_Col.greenF();
    qreal var_B = p_Col.blueF();

    //Inverse Companding
    if(m_GammaFlag == WLP_GAMMA_SRGB)
    {
        if ( var_R > 0.04045 ) var_R = pow( (var_R+0.055)/1.055, 2.4);
        else                   var_R = var_R / 12.92;
        if ( var_G > 0.04045 ) var_G = pow( (var_G+0.055)/1.055, 2.4);
        else                   var_G = var_G / 12.92;
        if ( var_B > 0.04045 ) var_B = pow( (var_B+0.055)/1.055, 2.4);
        else                   var_B = var_B / 12.92;
    }
    else if(m_GammaFlag == WLP_GAMMA_VALUE)
    {
        var_R = qPow(var_R, m_Gamma);
        var_G = qPow(var_G, m_Gamma);
        var_B = qPow(var_B, m_Gamma);
    }
    else if(m_GammaFlag == WLP_GAMMA_L)
    {
        if( var_R <= 0.08) var_R = (100.0 * var_R) / 903.3;
        else               var_R = pow(((var_R + 0.16)/1.16),3.0);
        if( var_G <= 0.08) var_G = (100.0 * var_G) / 903.3;
        else               var_G = pow(((var_G + 0.16)/1.16),3.0);
        if( var_B <= 0.08) var_B = (100.0 * var_B) / 903.3;
        else               var_B = pow(((var_B + 0.16)/1.16),3.0);
    }


    //Matrices:
    rgbVector(0,0) = var_R * 100;
    rgbVector(0,1) = var_G * 100;
    rgbVector(0,2) = var_B * 100;

    outSpace = rgbVector * rgb2xyzMatrix;

    qreal X = outSpace(0,0);
    qreal Y = outSpace(0,1);
    qreal Z = outSpace(0,2);

    qreal r = qBound<qreal>(0, X/m_Illuminant.X, 1); //X
    qreal g = qBound<qreal>(0, Y/m_Illuminant.Y, 1); //Y
    qreal b = qBound<qreal>(0, Z/m_Illuminant.Z, 1); //Z
    colOut = QColor::fromRgbF( r,g,b );


    return colOut;
}

QColor WLPBase::XYZtoRGB(QColor p_Col)
{
    QColor colOut;

    qreal var_X = p_Col.redF() * m_Illuminant.X;
    qreal var_Y = p_Col.greenF() * m_Illuminant.Y;
    qreal var_Z = p_Col.blueF() * m_Illuminant.Z;

    //Matrices:
    rgbVector(0,0) = var_X / 100.0;
    rgbVector(0,1) = var_Y / 100.0;
    rgbVector(0,2) = var_Z / 100.0;

    outSpace = rgbVector * xyz2rgbMatrix;

    qreal var_R = outSpace(0,0);
    qreal var_G = outSpace(0,1);
    qreal var_B = outSpace(0,2);

    //Companding
    if(m_GammaFlag == WLP_GAMMA_SRGB)
    {
        if ( var_R > 0.0031308 ) var_R = 1.055 * pow(var_R, ( 1.0 / 2.4 )) - 0.055;
        else                     var_R = 12.92 * var_R;
        if ( var_G > 0.0031308 ) var_G = 1.055 * pow(var_G, ( 1.0 / 2.4 )) - 0.055;
        else                     var_G = 12.92 * var_G;
        if ( var_B > 0.0031308 ) var_B = 1.055 * pow(var_B, ( 1.0 / 2.4 )) - 0.055;
        else                     var_B = 12.92 * var_B;
    }
    else if(m_GammaFlag == WLP_GAMMA_VALUE)
    {
        var_R = qPow(var_R, 1.0/m_Gamma);
        var_G = qPow(var_G, 1.0/m_Gamma);
        var_B = qPow(var_B, 1.0/m_Gamma);
    }
    else if(m_GammaFlag == WLP_GAMMA_L)
    {
        if( var_R <= 0.008856) var_R = (903.3 * var_R) / 100.0;
        else                   var_R = (1.16 * pow(var_R,1.0/3.0)) - 0.16;
        if( var_G <= 0.008856) var_G = (903.3 * var_G) / 100.0;
        else                   var_G = (1.16 * pow(var_G,1.0/3.0)) - 0.16;
        if( var_B <= 0.008856) var_B = (903.3 * var_B) / 100.0;
        else                   var_B = (1.16 * pow(var_B,1.0/3.0)) - 0.16;
    }

    qreal r = qBound<qreal>(0, var_R, 1); //R
    qreal g = qBound<qreal>(0, var_G, 1); //G
    qreal b = qBound<qreal>(0, var_B, 1); //B
    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::XYZtoLab(QColor p_Col)
{
    QColor colOut;

    qreal var_X = p_Col.redF() * m_Illuminant.X;
    qreal var_Y = p_Col.greenF() * m_Illuminant.Y;
    qreal var_Z = p_Col.blueF() * m_Illuminant.Z;

    var_X = var_X / 100.0;
    var_Y = var_Y / 100.0;
    var_Z = var_Z / 100.0;

    if ( var_X > 0.008856 ) var_X = pow(var_X, 1.0/3.0 );
    else                    var_X = ( 7.787 * var_X ) + ( 16.0 / 116.0 );
    if ( var_Y > 0.008856 ) var_Y = pow(var_Y, 1.0/3.0 );
    else                    var_Y = ( 7.787 * var_Y ) + ( 16.0 / 116.0 );
    if ( var_Z > 0.008856 ) var_Z = pow(var_Z, 1.0/3.0 );
    else                    var_Z = ( 7.787 * var_Z ) + ( 16.0 / 116.0 );

    qreal CIE_L = ( 116.0 * var_Y ) - 16.0;
    qreal CIE_a = 500 * ( var_X - var_Y );
    qreal CIE_b = 200 * ( var_Y - var_Z );

    qreal r = qBound<qreal>(0, CIE_L/100.0, 1); //L
    qreal g = qBound<qreal>(0, (CIE_a/256.0)+0.5, 1); //a
    qreal b = qBound<qreal>(0, (CIE_b/256.0)+0.5, 1); //b

    colOut = QColor::fromRgbF( r,g,b );

    return colOut;

}

QColor WLPBase::LabtoLCH(QColor p_Col)
{
    QColor colOut;
    qreal CIE_L = p_Col.redF() * 100;
    qreal CIE_a = (p_Col.greenF() - 0.5) * 256.0;
    qreal CIE_b = (p_Col.blueF() - 0.5) * 256.0;

    qreal var_H = atan2( CIE_b, CIE_a );  //Quadrant by signs

    if ( var_H > 0 ) var_H = ( var_H / M_PI ) * 180;
    else             var_H = 360 - ( std::abs( var_H ) / M_PI ) * 180;

    qreal CIE_C = sqrt( pow(CIE_a, 2.0) + pow(CIE_b, 2.0) );
    qreal CIE_H = var_H;
    if(CIE_H > 359) CIE_H -= 360;
    if(CIE_H < 0) CIE_H += 360;

    qreal r = qBound<qreal>(0, CIE_L/100.0, 1); //L
    qreal g = qBound<qreal>(0, CIE_C/100.0, 1); //C
    qreal b = qBound<qreal>(0, CIE_H/360.0, 1); //H

    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::LCHtoLab(QColor p_Col)
{
    QColor colOut;
    qreal CIE_L = p_Col.redF() * 100;
    qreal CIE_C = p_Col.greenF() * 100;
    qreal CIE_H = p_Col.blueF() * 360;

    qreal CIE_a = cos( CIE_H * (M_PI/180.0) ) * CIE_C;
    qreal CIE_b = sin( CIE_H * (M_PI/180.0) ) * CIE_C;

    qreal r = qBound<qreal>(0, CIE_L/100.0, 1); //L
    qreal g = qBound<qreal>(0, (CIE_a/256.0)+0.5, 1); //a
    qreal b = qBound<qreal>(0, (CIE_b/256.0)+0.5, 1); //b

    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::LabtoXYZ(QColor p_Col)
{
    QColor colOut;
    qreal CIE_L = p_Col.redF() * 100;
    qreal CIE_a = (p_Col.greenF() - 0.5) * 256.0;
    qreal CIE_b = (p_Col.blueF() - 0.5) * 256.0;

    qreal var_Y = ( CIE_L + 16 ) / 116;
    qreal var_X = CIE_a / 500.0 + var_Y;
    qreal var_Z = var_Y - CIE_b / 200.0;

    if ( pow(var_Y, 3.0) > 0.008856 ) var_Y = pow(var_Y, 3.0);
    else                      var_Y = ( var_Y - 16.0 / 116.0 ) / 7.787;
    if ( pow(var_X, 3.0) > 0.008856 ) var_X = pow(var_X, 3.0);
    else                      var_X = ( var_X - 16.0 / 116.0 ) / 7.787;
    if ( pow(var_Z, 3.0) > 0.008856 ) var_Z = pow(var_Z, 3.0);
    else                      var_Z = ( var_Z - 16.0 / 116.0 ) / 7.787;

    qreal X = qBound<qreal>(0, var_X, 1);
    qreal Y = qBound<qreal>(0, var_Y, 1);
    qreal Z = qBound<qreal>(0, var_Z, 1);

    colOut = QColor::fromRgbF(X,Y,Z);

    return colOut;
}

QColor WLPBase::XYZtoLuv(QColor p_Col)
{
    QColor colOut;
    qreal X = p_Col.redF() * m_Illuminant.X;
    qreal Y = p_Col.greenF() * m_Illuminant.Y;
    qreal Z = p_Col.blueF() * m_Illuminant.Z;

    qreal var_U = ( 4 * X ) / ( X + ( 15 * Y ) + ( 3 * Z ) );
    qreal var_V = ( 9 * Y ) / ( X + ( 15 * Y ) + ( 3 * Z ) );

    qreal var_Y = Y / 100;
    if ( var_Y > 0.008856 ) var_Y = pow(var_Y,  1.0/3.0 );
    else                    var_Y = ( 7.787 * var_Y ) + ( 16.0 / 116.0 );

    qreal ref_X = m_Illuminant.X;
    qreal ref_Y = m_Illuminant.Y;
    qreal ref_Z = m_Illuminant.Z;

    qreal ref_U = ( 4 * ref_X ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );
    qreal ref_V = ( 9 * ref_Y ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );

    qreal CIE_L = ( 116 * var_Y ) - 16;
    qreal CIE_u = 13 * CIE_L * ( var_U - ref_U );
    qreal CIE_v = 13 * CIE_L * ( var_V - ref_V );

    qreal r = qBound<qreal>(0, CIE_L/100.0, 1); //L
    qreal g = qBound<qreal>(0, (CIE_u/256.0)+0.5, 1); //a
    qreal b = qBound<qreal>(0, (CIE_v/256.0)+0.5, 1); //b

    colOut = QColor::fromRgbF( r,g,b );
    return colOut;
}



QColor WLPBase::LuvtoXYZ(QColor p_Col)
{
    QColor colOut;
    qreal CIE_L = p_Col.redF() * 100;
    qreal CIE_u = (p_Col.greenF() - 0.5) * 256.0;
    qreal CIE_v = (p_Col.blueF() - 0.5) * 256.0;

    qreal var_Y = ( CIE_L + 16 ) / 116;
    if ( pow(var_Y, 3.0) > 0.008856 ) var_Y = pow(var_Y, 3.0);
    else                      var_Y = ( var_Y - 16.0 / 116.0 ) / 7.787;

    qreal ref_X = m_Illuminant.X;
    qreal ref_Y = m_Illuminant.Y;
    qreal ref_Z = m_Illuminant.Z;

    qreal ref_U = ( 4 * ref_X ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );
    qreal ref_V = ( 9 * ref_Y ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );

    qreal var_U = CIE_u / ( 13 * CIE_L ) + ref_U;
    qreal var_V = CIE_v / ( 13 * CIE_L ) + ref_V;

    qreal Y = var_Y * 100;
    qreal X =  - ( 9 * Y * var_U ) / ( ( var_U - 4 ) * var_V  - var_U * var_V );
    qreal Z = ( 9 * Y - ( 15 * var_V * Y ) - ( var_V * X ) ) / ( 3 * var_V );

    qreal r = qBound<qreal>(0, X/m_Illuminant.X, 1);
    qreal g = qBound<qreal>(0, Y/m_Illuminant.Y, 1);
    qreal b = qBound<qreal>(0, Z/m_Illuminant.Z, 1);

    colOut = QColor::fromRgbF(r,g,b);
    return colOut;
}

QColor WLPBase::XYZtoUVW(QColor p_Col)
{
    QColor colOut;

    qreal X = p_Col.redF() * m_Illuminant.X;
    qreal Y = p_Col.greenF() * m_Illuminant.Y;
    qreal Z = p_Col.blueF() * m_Illuminant.Z;

    qreal var_U = ( 4 * X ) / ( X + ( 15 * Y ) + ( 3 * Z ) );
    qreal var_V = ( 9 * Y ) / ( X + ( 15 * Y ) + ( 3 * Z ) );

    qreal ref_X = m_Illuminant.X;
    qreal ref_Y = m_Illuminant.Y;
    qreal ref_Z = m_Illuminant.Z;

    qreal ref_U = ( 4 * ref_X ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );
    qreal ref_V = ( 9 * ref_Y ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );

    qreal CIE_W = 25.0 * pow(Y, (1.0/3.0)) - 17.0;
    qreal CIE_U = 13 * CIE_W * ( var_U - ref_U );
    qreal CIE_V = 13 * CIE_W * ( var_V - ref_V );

    qreal r = qBound<qreal>(0, (CIE_U/256.0)+0.5, 1); //U
    qreal g = qBound<qreal>(0, (CIE_V/256.0)+0.5, 1); //V
    qreal b = qBound<qreal>(0, CIE_W/100.0, 1); //W

    colOut = QColor::fromRgbF( r,g,b );
    return colOut;
}

QColor WLPBase::UVWtoXYZ(QColor p_Col)
{
    QColor colOut;
    qreal CIE_U = (p_Col.redF() - 0.5) * 256.0;
    qreal CIE_V = (p_Col.greenF() - 0.5) * 256.0;
    qreal CIE_W = p_Col.blueF() * 100.0;

    qreal ref_X = m_Illuminant.X;
    qreal ref_Y = m_Illuminant.Y;
    qreal ref_Z = m_Illuminant.Z;

    qreal ref_U = ( 4 * ref_X ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );
    qreal ref_V = ( 9 * ref_Y ) / ( ref_X + ( 15 * ref_Y ) + ( 3 * ref_Z ) );

    qreal var_U = CIE_U / ( 13 * CIE_W ) + ref_U;
    qreal var_V = CIE_V / ( 13 * CIE_W ) + ref_V;

    qreal Y = 0.000064 * pow(CIE_W+17, 3);
    qreal X =  - ( 9 * Y * var_U ) / ( ( var_U - 4 ) * var_V  - var_U * var_V );
    qreal Z = ( 9 * Y - ( 15 * var_V * Y ) - ( var_V * X ) ) / ( 3 * var_V );

    qreal r = qBound<qreal>(0, X/m_Illuminant.X, 1); //X
    qreal g = qBound<qreal>(0, Y/m_Illuminant.Y, 1); //Y
    qreal b = qBound<qreal>(0, Z/m_Illuminant.Z, 1); //Z
    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}


QColor WLPBase::RGBtoYIQ(QColor p_Col)
{
    QColor colOut;

    rgbVector(0,0) = p_Col.redF();
    rgbVector(0,1) = p_Col.greenF();
    rgbVector(0,2) = p_Col.blueF();

    outSpace = rgbVector * rgb2yiqMatrix;
    rgbOut(0,0) = outSpace(0,0);
    rgbOut(0,1) = outSpace(0,1) + 0.59;
    rgbOut(0,2) = outSpace(0,2) + 0.52;

    int r = qBound(0, (int)(rgbOut(0,0) * 255.0), 255); //Y
    int g = qBound(0, (int)(rgbOut(0,1) * 255.0), 255); //I
    int b = qBound(0, (int)(rgbOut(0,2) * 255.0), 255); //Q
    colOut = QColor::fromRgb( r,g,b );

    return colOut;
}

QColor WLPBase::YIQtoRGB(QColor p_Col)
{
    QColor colOut;

    rgbVector(0,0) = p_Col.redF();
    rgbVector(0,1) = p_Col.greenF() - 0.59;
    rgbVector(0,2) = p_Col.blueF() - 0.52;

    outSpace = rgbVector * yiq2rgbMatrix;
    rgbOut(0,0) = outSpace(0,0);
    rgbOut(0,1) = outSpace(0,1);
    rgbOut(0,2) = outSpace(0,2);

    int r = qBound(0, (int)(rgbOut(0,0) * 255.0), 255); //R
    int g = qBound(0, (int)(rgbOut(0,1) * 255.0), 255); //G
    int b = qBound(0, (int)(rgbOut(0,2) * 255.0), 255); //B
    colOut = QColor::fromRgb( r,g,b ).rgb();

    return colOut;
}

QColor WLPBase::RGBtoYUV(QColor p_Col)
{
    QColor colOut;
    qreal red = p_Col.red();
    qreal green = p_Col.green();
    qreal blue = p_Col.blue();

    qreal Y = red *  .299000 + green *  .587000 + blue *  .114000;
    qreal U = red * -.168736 + green * -.331264 + blue *  .500000 + 128;
    qreal V = red *  .500000 + green * -.418688 + blue * -.081312 + 128;

    int r = qBound<int>(0, Y, 255);
    int g = qBound<int>(0, U, 255);
    int b = qBound<int>(0, V, 255);
    colOut = QColor::fromRgb( r,g,b );

    return colOut;
}

QColor WLPBase::YUVtoRGB(QColor p_Col)
{
    QColor colOut;
    qreal Y = p_Col.red();
    qreal U = p_Col.green();
    qreal V = p_Col.blue();

    int R = Y + 1.4075 * (V - 128);
    int G = Y - 0.3455 * (U - 128) - (0.7169 * (V - 128));
    int B = Y + 1.7790 * (U - 128);

    int r = qBound(0, R, 255);
    int g = qBound(0, G, 255);
    int b = qBound(0, B, 255);
    colOut = QColor::fromRgb( r,g,b );
    return colOut;
}

QColor WLPBase::RGBtoHSL(QColor p_Col)
{
    QColor colOut;

    qreal r = qBound<qreal>(0,p_Col.hslHueF(), 0.9999);
    qreal g = p_Col.hslSaturationF();
    qreal b = p_Col.lightnessF();
    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::XYZtoHLAB(QColor p_Col)
{
    qreal X = p_Col.redF() * 100;
    qreal Y = p_Col.greenF() * 100;
    qreal Z = p_Col.blueF() * 100;

    qreal HUNTER_L = 10 * sqrt(Y);
    qreal HUNTER_A = 17.5 * (((1.02 * X) - Y) / sqrt(Y));
    qreal HUNTER_B = 7 * ((Y - (0.847 * Z)) / sqrt(Y));

    qreal r = qBound<qreal>(0, HUNTER_L/100.0, 1); //L
    qreal g = qBound<qreal>(0, (HUNTER_A/256.0)+0.5, 1); //a
    qreal b = qBound<qreal>(0, (HUNTER_B/256.0)+0.5, 1); //b

    QColor colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::HLABtoXYZ(QColor p_Col)
{
    QColor colOut;
    qreal HUN_L = p_Col.redF() * 100;
    qreal HUN_a = (p_Col.greenF() - 0.5) * 256.0;
    qreal HUN_b = (p_Col.blueF() - 0.5) * 256.0;

    qreal var_Y = HUN_L / 10;
    qreal var_X = HUN_a / 17.5 * HUN_L / 10;
    qreal var_Z = HUN_b / 7 * HUN_L/ 10;

    qreal Y = qPow(var_Y, 2);
    qreal X = ( var_X + Y ) / 1.02;
    qreal Z = -( var_Z - Y ) / 0.847;

    qreal r = qBound<qreal>(0, X/m_Illuminant.X, 1); //X
    qreal g = qBound<qreal>(0, Y/m_Illuminant.Y, 1); //Y
    qreal b = qBound<qreal>(0, Z/m_Illuminant.Z, 1); //Z
    colOut = QColor::fromRgbF( r,g,b );
    return colOut;
}

QColor WLPBase::RGBtoCMYK(QColor p_Col)
{
    QColor colOut = p_Col.toCmyk();
    return colOut;
}

QColor WLPBase::RGBAtoHSI(QColor p_Col)
{
    QColor colOut;

    qreal r = p_Col.cyanF()/3.0;
    qreal g = p_Col.magentaF()/3.0;
    qreal b = p_Col.yellowF()/3.0;
    qreal w = p_Col.blackF();

    QColor getHue = QColor::fromRgbF(r,g,b);

    qreal H = qBound<qreal>(0, getHue.hslHueF(), 1);
    qreal I = qBound<qreal>(0, (r+g+b+w), 1);
    qreal S = qBound<qreal>(0, 1-(w/(I)), 1);

    colOut = QColor::fromRgbF(H,S,I);
    return colOut;

}

// A Colourspace I found here:
// http://reinhard-kietzmann.de/rgbw_color_space.html
//
// Using a function found here:
// http://blog.saikoled.com/post/44677718712/how-to-convert-from-hsi-to-rgb-white
QColor WLPBase::HSItoRGBA(QColor p_Col)
{
    QColor colOut;
    qreal r, g, b, w;
    float cos_h, cos_1047_h;

    //Assuming HSI values were stored in RGB of QColor
    qreal H = p_Col.redF() * 360.0;
    H = 3.14159*H/(float)180; // Convert to radians.

    qreal S = p_Col.greenF();
    qreal I = p_Col.blueF();

    if(H < 2.09439) {
        cos_h = cos(H);
        cos_1047_h = cos(1.047196667-H);
        r = S*255*I/3*(1+cos_h/cos_1047_h);
        g = S*255*I/3*(1+(1-cos_h/cos_1047_h));
        b = 0;
        w = 255*(1-S)*I;
      } else if(H < 4.188787) {
        H = H - 2.09439;
        cos_h = cos(H);
        cos_1047_h = cos(1.047196667-H);
        g = S*255*I/3*(1+cos_h/cos_1047_h);
        b = S*255*I/3*(1+(1-cos_h/cos_1047_h));
        r = 0;
        w = 255*(1-S)*I;
      } else {
        H = H - 4.188787;
        cos_h = cos(H);
        cos_1047_h = cos(1.047196667-H);
        b = S*255*I/3*(1+cos_h/cos_1047_h);
        r = S*255*I/3*(1+(1-cos_h/cos_1047_h));
        g = 0;
        w = 255*(1-S)*I;
      }

    r = qBound<qreal>(0, r*3, 255);
    g = qBound<qreal>(0, g*3, 255);
    b = qBound<qreal>(0, b*3, 255);
    w = qBound<qreal>(0, w, 255);

    //Store them in cmyk of Qcolor
    colOut = QColor::fromCmyk(r,g,b,w);
    return colOut;
}

QColor WLPBase::RGBtoHSI(QColor p_Col)
{
    QColor colOut;

    //We can use QColor's lookup for hue (same for HSL/HSV/HSI)
    //But for some reason it actually goes out of bounds sometimes
    qreal h = qBound<qreal>(0,p_Col.hueF(), 1);

    //A basic average of the RGB values
    qreal i = qBound<qreal>(0, (p_Col.redF() + p_Col.greenF() + p_Col.blueF())/3.0, 1);

    //The minimum value is use alongside the intensity to get the saturation
    qreal m = qMin(p_Col.redF(), qMin(p_Col.greenF(), p_Col.blueF()));
    qreal s = qBound<qreal>(0, 1 - (m/i), 1);

    colOut = QColor::fromRgbF(h,s,i);
    return colOut;
}

QColor WLPBase::HSItoRGB(QColor p_Col)
{
    qreal H = p_Col.redF() * 360;
    qreal S = p_Col.greenF();
    qreal I = p_Col.blueF();

    qreal R,G,B; R=G=B=0;

    if(H == 0)
    {
        R = I + (2*I*S);
        G = I - (I*S);
        B = I - (I*S);
    }
    else if(H < 120)
    {
        R = I + I * S * qCos(DEG_TO_RAD(H))/qCos(DEG_TO_RAD(60-H));
        G = I + I * S * (1 - qCos(DEG_TO_RAD(H))/qCos(DEG_TO_RAD(60-H)));
        B = I - I * S;
    }
    else if(H == 120)
    {
        R = I - I * S;
        G = I + 2 * I * S;
        B = I - I * S;
     }
    else if(H < 240)
    {
        R = I - I * S;
        G = I + I * S * cos(DEG_TO_RAD(H-120))/cos(DEG_TO_RAD(180-H));
        B = I + I * S * (1 - cos(DEG_TO_RAD(H-120))/cos(DEG_TO_RAD(180-H)));
     }
    else if(H == 240)
    {
        R = I - I * S;
        G = I - I * S;
        B = I + 2 * I * S;
    }
    else if(H < 360)
    {
        R = I + I * S * (1 - cos(DEG_TO_RAD(H-240))/cos(DEG_TO_RAD(300-H)));
        G = I - I * S;
        B = I + I * S * cos(DEG_TO_RAD(H-240))/cos(DEG_TO_RAD(300-H));
    }
    else{ R=1; G=B=0; }

    R = qBound<qreal>(0, R, 1);
    G = qBound<qreal>(0, G, 1);
    B = qBound<qreal>(0, B, 1);

    return QColor::fromRgbF(R,G,B);
}

QColor WLPBase::RGBtoCMY(QColor p_Col)
{
    qreal c = 1 - p_Col.redF();
    qreal m = 1 - p_Col.greenF();
    qreal y = 1 - p_Col.blueF();
    return QColor::fromRgbF(c,m,y);
}

QColor WLPBase::RGBtoHSV(QColor p_Col)
{
    QColor colOut;

    qreal r = qBound<qreal>(0,p_Col.hsvHueF(), 0.9999);
    qreal g = p_Col.hsvSaturationF();
    qreal b = p_Col.valueF();
    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::XYZtoLMS(QColor p_Col)
{
    QColor colOut;

    rgbVector(0,0) = p_Col.redF();
    rgbVector(0,1) = p_Col.greenF();
    rgbVector(0,2) = p_Col.blueF();

    outSpace = rgbVector * xyz2lmsMatrix;
    rgbOut(0,0) = outSpace(0,0);// + 0.002;
    rgbOut(0,1) = outSpace(0,1);
    rgbOut(0,2) = outSpace(0,2);

    qreal r = qBound<qreal>(0, rgbOut(0,0), 1); //L
    qreal g = qBound<qreal>(0, rgbOut(0,1), 1); //M
    qreal b = qBound<qreal>(0, rgbOut(0,2), 1); //S
    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::LMStoXYZ(QColor p_Col)
{
    QColor colOut;

    rgbVector(0,0) = p_Col.redF();// - 0.002;
    rgbVector(0,1) = p_Col.greenF();
    rgbVector(0,2) = p_Col.blueF();

    outSpace = rgbVector * lms2xyzMatrix;
    rgbOut(0,0) = outSpace(0,0);
    rgbOut(0,1) = outSpace(0,1);
    rgbOut(0,2) = outSpace(0,2);

    qreal r = qBound<qreal>(0, rgbOut(0,0), 1); //X
    qreal g = qBound<qreal>(0, rgbOut(0,1), 1); //Y
    qreal b = qBound<qreal>(0, rgbOut(0,2), 1); //Z
    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::RGBtoRGC(QColor p_Col)
{
    QColor colOut;
    QColor c = p_Col;
    rgbOut(0,0) = c.redF() / (c.redF() + c.greenF() + c.blueF());
    rgbOut(0,1) = c.greenF()  / (c.redF() + c.greenF() + c.blueF());
    rgbOut(0,2) = c.blueF() / (c.redF() + c.greenF() + c.blueF());

    qreal r = qBound<qreal>(0, rgbOut(0,0), 1); //r'
    qreal g = qBound<qreal>(0, rgbOut(0,1), 1); //g'
    qreal b = qBound<qreal>(0, rgbOut(0,2), 1); //b'

    colOut = QColor::fromRgbF( r,g,b );

    return colOut;
}

QColor WLPBase::RGCtoRGB(QColor p_Col)
{
    QColor c = QColor::fromRgbF(p_Col.redF(), p_Col.greenF(), p_Col.blueF());
    qreal x = c.redF();
    qreal y = c.greenF();
    qreal Y = c.blueF();

    qreal X = qBound<qreal>(0, (x * Y) / y, 1);
    qreal Z = qBound<qreal>(0, ((1 - x - y) * Y)/y, 1);

    return QColor::fromRgbF(X,Y,Z);
}


//The conveniece function
QColor WLPBase::ConvertToRGB(QColor p_Col, WLPMode _mode)
{
    if(_mode == WLP_MODE_RGC) return RGCtoRGB(p_Col);
    else if(_mode == WLP_MODE_RGB) return p_Col; //QColor supported
    else if(_mode == WLP_MODE_RGBW) return HSItoRGB(RGBAtoHSI(p_Col));

    else if(_mode == WLP_MODE_CMY) return RGBtoCMY(p_Col);
    else if(_mode == WLP_MODE_CMYK) return p_Col; //QColor supported

    else if(_mode == WLP_MODE_HSL) return p_Col; //QColor supported
    else if(_mode == WLP_MODE_HSV) return p_Col; //QColor supported
    else if(_mode == WLP_MODE_HSI) return HSItoRGB(p_Col);

    else if(_mode == WLP_MODE_YIQ) return YIQtoRGB(p_Col);
    else if(_mode == WLP_MODE_YUV) return YUVtoRGB(p_Col);

    else if(_mode == WLP_MODE_XYZ) return XYZtoRGB(p_Col);
    else if(_mode == WLP_MODE_LAB) {
        if(lutSetLAB)
            return lutLabtoRGB[p_Col.red()][p_Col.green()][p_Col.blue()];
        else
            return XYZtoRGB(LabtoXYZ(p_Col));
    }
    else if(_mode == WLP_MODE_LUV) return XYZtoRGB(LuvtoXYZ(p_Col));
    else if(_mode == WLP_MODE_UVW) return XYZtoRGB(UVWtoXYZ(p_Col));
    else if(_mode == WLP_MODE_LMS) return XYZtoRGB(LMStoXYZ(p_Col));
    else if(_mode == WLP_MODE_HUNTER) return XYZtoRGB(HLABtoXYZ(p_Col));
    else if(_mode == WLP_MODE_LCH_AB){
        if(lutSetLCH)
            return lutLCHtoRGB[p_Col.red()][p_Col.green()][p_Col.blue()];
        else
            return XYZtoRGB(LabtoXYZ(LCHtoLab(p_Col)));
    }
    else if(_mode == WLP_MODE_LCH_UV) return XYZtoRGB(LuvtoXYZ(LCHtoLab(p_Col)));
    else{ qDebug() << "Why can' things just work, aye?"; }

    return QColor::fromRgbF(1,0,0);
}

QColor WLPBase::ConvertTo(QColor _col, WLPMode _mode, WLPMode _oldMode)
{
    QColor temp = _col;
    if(_oldMode != WLP_MODE_RGB)
        temp = ConvertToRGB(temp, _oldMode);

    if(_mode == WLP_MODE_RGC) return RGBtoRGC(temp);
    else if(_mode == WLP_MODE_RGB) return temp; //QColor supported
    else if(_mode == WLP_MODE_RGBW) return HSItoRGBA(RGBtoHSI(temp));

    else if(_mode == WLP_MODE_CMY) return RGBtoCMY(temp);
    else if(_mode == WLP_MODE_CMYK) return temp; //QColor supported

    else if(_mode == WLP_MODE_HSL) return temp; //QColor supported
    else if(_mode == WLP_MODE_HSV) return temp; //QColor supported
    else if(_mode == WLP_MODE_HSI) return RGBtoHSI(temp);

    else if(_mode == WLP_MODE_YIQ) return RGBtoYIQ(temp);
    else if(_mode == WLP_MODE_YUV) return RGBtoYUV(temp);

    else if(_mode == WLP_MODE_XYZ) return RGBtoXYZ(temp);
    else if(_mode == WLP_MODE_LAB) {
        if(lutSetLAB)
            return lutRGBtoLab[temp.red()][temp.green()][temp.blue()];
        else
            return XYZtoLab(RGBtoXYZ(temp));
    }
    else if(_mode == WLP_MODE_LUV) return RGBtoXYZ(XYZtoLuv(temp));
    else if(_mode == WLP_MODE_UVW) return RGBtoXYZ(XYZtoUVW(temp));
    else if(_mode == WLP_MODE_LMS) return RGBtoXYZ(XYZtoLMS(temp));
    else if(_mode == WLP_MODE_HUNTER) return RGBtoXYZ(XYZtoHLAB(temp));
    else if(_mode == WLP_MODE_LCH_AB){
        if(lutSetLCH)
            return lutRGBtoLCH[temp.red()][temp.green()][temp.blue()];
        else
            return LabtoLCH(XYZtoLab(RGBtoXYZ(temp)));
}
    else if(_mode == WLP_MODE_LCH_UV) return RGBtoXYZ(XYZtoLuv(LabtoLCH(temp)));
    else{ qDebug() << "Failus Maximus"; }


    return _col;
}

